package owl2oo.parser

import org.semanticweb.owlapi.formats.FunctionalSyntaxDocumentFormat
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormat
import spock.lang.Specification
import spock.lang.Subject

class OntologyParserTest extends Specification {

  @Subject
  private OntologyParser ontologyParser = new OntologyParser()

  private static final String OWL_SCHEMA_ADDRESS = 'http://www.w3.org/2002/07/owl'
  private static final String RDF_SCHEMA_ADDRESS = 'http://www.w3.org/1999/02/22-rdf-syntax-ns'
  private static final String ONTOLOGY_ADDRESS = 'http://www.example.com/ontology1'
  private static final List<String> ONTOLOGY_CLASSES = ['Class1', 'Class2', 'Class3']

  def 'should parse ontology in OWL Functional format'() {
    given:
    def ontology = """
      Prefix(:=<$ONTOLOGY_ADDRESS#>)
      Ontology( <$ONTOLOGY_ADDRESS>
          Declaration(Class(<$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[0]}>))
          Declaration(Class(<$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[1]}>))
          Declaration(Class(<$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[2]}>))
      )"""

    when:
    def parsedOntology = ontologyParser.parse(new ByteArrayInputStream(ontology.bytes))

    then:
    with(parsedOntology) {
      format instanceof FunctionalSyntaxDocumentFormat
      ontologyID.ontologyIRI.get().IRIString == ONTOLOGY_ADDRESS
      classesInSignature().collect { it.getIRI().fragment }.sort() == ONTOLOGY_CLASSES
    }
  }

  def 'should parse ontology in OWL/XML format'() {
    given:
    def ontology = """
      <Ontology xmlns="${OWL_SCHEMA_ADDRESS}#" xml:base="$ONTOLOGY_ADDRESS" ontologyIRI="$ONTOLOGY_ADDRESS">
        <Prefix name="" IRI="$ONTOLOGY_ADDRESS"/>
        <Prefix name="owl" IRI="${OWL_SCHEMA_ADDRESS}#"/>
        <Declaration><Class IRI="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[0]}"/></Declaration>
        <Declaration><Class IRI="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[1]}"/></Declaration>
        <Declaration><Class IRI="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[2]}"/></Declaration>
      </Ontology>"""

    when:
    def parsedOntology = ontologyParser.parse(new ByteArrayInputStream(ontology.bytes))

    then:
    with(parsedOntology) {
      format instanceof OWLXMLDocumentFormat
      ontologyID.ontologyIRI.get().IRIString == ONTOLOGY_ADDRESS
      classesInSignature().collect { it.getIRI().fragment }.sort() == ONTOLOGY_CLASSES
    }
  }

  def 'should parse ontology in RDF/XML format'() {
    given:
    def ontology = """
      <rdf:RDF xmlns="$ONTOLOGY_ADDRESS#" xml:base="$ONTOLOGY_ADDRESS" ontologyIRI="$ONTOLOGY_ADDRESS"
               xmlns:general="$ONTOLOGY_ADDRESS" xmlns:rdf="$RDF_SCHEMA_ADDRESS#"
               xmlns:owl="$OWL_SCHEMA_ADDRESS#">
        <owl:Ontology rdf:about="$ONTOLOGY_ADDRESS"/>
        <owl:Class rdf:about="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[0]}"/>
        <owl:Class rdf:about="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[1]}"/>
        <owl:Class rdf:about="$ONTOLOGY_ADDRESS#${ONTOLOGY_CLASSES[2]}"/>
      </rdf:RDF>"""

    when:
    def parsedOntology = ontologyParser.parse(new ByteArrayInputStream(ontology.bytes))

    then:
    with(parsedOntology) {
      format instanceof RDFXMLDocumentFormat
      ontologyID.ontologyIRI.get().IRIString == ONTOLOGY_ADDRESS
      classesInSignature().collect { it.getIRI().fragment }.sort() == ONTOLOGY_CLASSES
    }
  }
}
