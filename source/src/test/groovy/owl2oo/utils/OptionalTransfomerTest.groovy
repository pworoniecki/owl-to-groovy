package owl2oo.utils

import spock.lang.Specification
import spock.lang.Unroll

class OptionalTransfomerTest extends Specification {

  private static final Optional<String> EMPTY_OPTIONAL = Optional.empty()
  private static final Optional<String> FIRST_NON_EMPTY_OPTIONAL = Optional.of('test1')
  private static final Optional<String> SECOND_NON_EMPTY_OPTIONAL = Optional.of('test2')

  def 'should return wrapped optional'() {
    given:
    def optional = Optional.of('test')

    when:
    def transformer = OptionalTransfomer.of(optional)

    then:
    transformer.getResult() == optional
  }

  def 'should throw an exception when trying to be created using null'() {
    when:
    OptionalTransfomer.of(null)

    then:
    thrown NullPointerException
  }

  @Unroll
  def 'should return first non-empty optional'() {
    expect:
    transformer.getResult() == FIRST_NON_EMPTY_OPTIONAL

    where:
    transformer << [
        OptionalTransfomer.of(FIRST_NON_EMPTY_OPTIONAL).or { SECOND_NON_EMPTY_OPTIONAL },
        OptionalTransfomer.of(FIRST_NON_EMPTY_OPTIONAL).or { EMPTY_OPTIONAL }.or { SECOND_NON_EMPTY_OPTIONAL },
        OptionalTransfomer.of(EMPTY_OPTIONAL).or { FIRST_NON_EMPTY_OPTIONAL }.or { SECOND_NON_EMPTY_OPTIONAL },
        OptionalTransfomer.of(EMPTY_OPTIONAL).or { EMPTY_OPTIONAL }.or { FIRST_NON_EMPTY_OPTIONAL }
            .or { SECOND_NON_EMPTY_OPTIONAL }.or { EMPTY_OPTIONAL }
    ]
  }
}
