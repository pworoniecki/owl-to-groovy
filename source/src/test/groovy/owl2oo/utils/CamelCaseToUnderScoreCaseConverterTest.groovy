package owl2oo.utils

import spock.lang.Specification

class CamelCaseToUnderScoreCaseConverterTest extends Specification {

  def 'should convert empty string to empty string'() {
    when:
    def text = ''

    then:
    CamelCaseToUnderScoreCaseConverter.camelCaseToUnderScoreCase(text) == ''
  }

  def 'should convert camelCase written string to under_score_case format'() {
    when:
    def text = 'testCamelCaseString'

    then:
    CamelCaseToUnderScoreCaseConverter.camelCaseToUnderScoreCase(text) == 'TEST_CAMEL_CASE_STRING'
  }

  def 'should throw NullPointerException when null string is given'() {
    given:
    def text = null

    when:
    CamelCaseToUnderScoreCaseConverter.camelCaseToUnderScoreCase(text)

    then:
    thrown NullPointerException
  }

}
