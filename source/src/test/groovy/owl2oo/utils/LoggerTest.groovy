package owl2oo.utils

import spock.lang.Specification
import spock.lang.Subject

class LoggerTest extends Specification {

  private static final PrintStream ORIGINAL_OUTPUT_STREAM = System.out
  private static final PrintStream ORIGINAL_ERROR_STREAM = System.err
  private static final String WARN_LOG_LEVEL = 'WARN'
  private static final String LOGGER_CLASS_NAME = Logger.name

  @Subject
  private Logger logger = new Logger()

  private ByteArrayOutputStream outputStream

  def setup() {
    outputStream = new ByteArrayOutputStream()
    System.setOut(new PrintStream(outputStream))
    System.setErr(new PrintStream(outputStream))
  }

  def cleanupSpec() {
    System.setOut(ORIGINAL_OUTPUT_STREAM)
    System.setErr(ORIGINAL_ERROR_STREAM)
  }

  def 'should log message with warning level'() {
    given:
    def message = 'test message'

    when:
    logger.warn(message)

    then:
    outputStream.toString().matches "^.* $WARN_LOG_LEVEL $LOGGER_CLASS_NAME - $message\n\$"
  }
}
