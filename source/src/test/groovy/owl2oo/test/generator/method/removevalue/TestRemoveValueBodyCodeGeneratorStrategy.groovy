package owl2oo.test.generator.method.removevalue

import owl2oo.generator.method.removevalue.RemoveValueBodyCodeGeneratorStrategy
import owl2oo.generator.model.method.RemoveValueMethod

class TestRemoveValueBodyCodeGeneratorStrategy implements RemoveValueBodyCodeGeneratorStrategy {

  private static final int DEFAULT_PRIORITY = 1
  private static final int DEFAULT_ORDER_WITHIN_PRIORITY_GROUP = 1

  private RemoveValueMethod supportedRemoveValueMethod
  private int priority
  private int orderWithinPriorityGroup

  TestRemoveValueBodyCodeGeneratorStrategy(RemoveValueMethod removeValueMethod, int priority = DEFAULT_PRIORITY,
                                           int orderWithinPriorityGroup = DEFAULT_ORDER_WITHIN_PRIORITY_GROUP) {
    this.supportedRemoveValueMethod = removeValueMethod
    this.priority = priority
    this.orderWithinPriorityGroup = orderWithinPriorityGroup
  }

  @Override
  boolean supports(RemoveValueMethod removeValueMethod) {
    return supportedRemoveValueMethod.is(removeValueMethod)
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod removeValueMethod) {
    return Optional.of(generateRemoveValueMethodValidationCode(removeValueMethod, priority, orderWithinPriorityGroup))
  }

  static String generateRemoveValueMethodValidationCode(RemoveValueMethod removeValueMethod, int priority = DEFAULT_PRIORITY,
                                                        int order = DEFAULT_ORDER_WITHIN_PRIORITY_GROUP) {
    return "Validation code generated by strategy supporting $removeValueMethod.name with priority $priority and order $order"
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod removeValueMethod) {
    return Optional.of(generateRemoveValueCode(supportedRemoveValueMethod, priority, orderWithinPriorityGroup))
  }

  static String generateRemoveValueCode(RemoveValueMethod removeValueMethod, int priority = DEFAULT_PRIORITY,
                                     int order = DEFAULT_ORDER_WITHIN_PRIORITY_GROUP) {
    return "Code generated by strategy supporting $removeValueMethod.name with priority $priority and order $order"
  }

  @Override
  int getPriority() {
    return priority
  }

  int getOrderWithinPriorityGroup() {
    return orderWithinPriorityGroup
  }
}
