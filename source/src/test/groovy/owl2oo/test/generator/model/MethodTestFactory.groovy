package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.Parameter
import owl2oo.generator.model.method.Method
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType

class MethodTestFactory {

  static Method createMethod(String name = 'testMethod', AccessModifier accessModifier = AccessModifier.PROTECTED) {
    return new TestMethod.Builder(name)
        .withAccessModifier(accessModifier)
        .withParameters([new Parameter(new SimpleType(String), 'testParameter')])
        .withReturnType(new SimpleType(Integer))
        .asStatic()
        .buildMethod()
  }

  static Method createStaticMethod(String name = 'testMethod', Type returnType = VoidType.INSTANCE,
                                   List<Parameter> parameters = []) {
    return new TestMethod.Builder(name).asStatic().withReturnType(returnType).withParameters(parameters).buildMethod()
  }

  static Method createNonStaticMethod(String name = 'testMethod', Type returnType = VoidType.INSTANCE,
                                      List<Parameter> parameters = []) {
    return new TestMethod.Builder(name).withReturnType(returnType).withParameters(parameters).buildMethod()
  }

  static Method createMethod(String name, AccessModifier modifier, boolean isStatic, Type returnType,
                             List<Parameter> parameters) {
    def builder = new TestMethod.Builder(name).withAccessModifier(modifier).withReturnType(returnType)
        .withParameters(parameters)
    return isStatic ? builder.asStatic().buildMethod() : builder.buildMethod()
  }
}
