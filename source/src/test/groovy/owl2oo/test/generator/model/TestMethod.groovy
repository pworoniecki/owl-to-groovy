package owl2oo.test.generator.model

import owl2oo.generator.model.method.Method
import groovy.transform.InheritConstructors

@InheritConstructors
class TestMethod extends Method {

  @InheritConstructors
  static class Builder extends Method.Builder<TestMethod, Builder> {
    @Override
    Builder getThis() {
      return this
    }

    @Override
    TestMethod buildMethod() {
      return new TestMethod(this)
    }
  }
}
