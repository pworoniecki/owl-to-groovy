package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.Parameter
import owl2oo.generator.model.type.SimpleType

import static owl2oo.generator.model.AccessModifier.PACKAGE_PROTECTED

class ConstructorTestFactory {

  static Constructor createConstructor(AccessModifier accessModifier = PACKAGE_PROTECTED,
                                       List<Parameter> parameters = [createTestParameter()], boolean isStatic = false) {
    return new Constructor(accessModifier: accessModifier, parameters: parameters, isStatic: isStatic)
  }

  private static Parameter createTestParameter() {
    return new Parameter(new SimpleType(Integer), 'testParameter')
  }
}
