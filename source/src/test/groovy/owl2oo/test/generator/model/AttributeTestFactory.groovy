package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.attribute.AttributeCharacteristic
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type

import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2oo.generator.model.attribute.AttributeCharacteristic.SYMMETRIC

class AttributeTestFactory {

  static Attribute createAttribute(String name = 'testAttribute', Type type = new SimpleType(String),
                                   AccessModifier accessModifier = AccessModifier.PROTECTED) {
    return new Attribute.AttributeBuilder(name, type)
        .withCharacteristics([SYMMETRIC, REFLEXIVE])
        .withAccessModifier(accessModifier)
        .withRelations([])
        .asStatic()
        .withDefaultValue('DefaultAttributeValue')
        .buildAttribute()
  }

  static Attribute createAttribute(List<AttributeCharacteristic> characteristics, String name = 'testAttribute',
                                   Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type)
        .withCharacteristics(characteristics)
        .withAccessModifier(AccessModifier.PROTECTED)
        .withRelations([])
        .asStatic()
        .withDefaultValue('DefaultAttributeValue')
        .buildAttribute()
  }

  static Attribute createEmptyStaticAttribute(String name = 'testAttribute', Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type).asStatic().buildAttribute()
  }

  static Attribute createEmptyNonStaticAttribute(String name = 'testAttribute', Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type).buildAttribute()
  }
}
