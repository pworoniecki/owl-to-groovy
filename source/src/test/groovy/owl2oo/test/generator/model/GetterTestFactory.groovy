package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Getter

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute

class GetterTestFactory {

  static Getter createMethod(Attribute attribute = createAttribute()) {
    return new Getter.Builder(attribute).withAccessModifier(AccessModifier.PROTECTED).asStatic()
        .buildMethod()
  }

  static Getter createMethod(Attribute attribute, AccessModifier modifier, boolean isStatic) {
    def builder = new Getter.Builder(attribute).withAccessModifier(modifier)
    return isStatic ? builder.asStatic().buildMethod() : builder.buildMethod()
  }
}
