package owl2oo.test.generator.model

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.AddValueMethod
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute

class AddValueMethodTestFactory {

  static AddValueMethod createAddValueMethod(Attribute attribute) {
    return new AddValueMethod.Builder(attribute).buildMethod()
  }

  static AddValueMethod createAddValueMethod(String attributeName = 'sample') {
    return new AddValueMethod.Builder(createAttribute(attributeName, Types.STRING_LIST)).buildMethod()
  }
}
