package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.Trait
import owl2oo.generator.model.type.SimpleType

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute
import static owl2oo.test.generator.model.MethodTestFactory.createMethod

class TraitTestFactory {

  static Trait createTrait(String traitName = 'TestTrait', String packageName = 'org.test') {
    return new Trait.Builder(packageName, traitName)
        .withAttributes([createAttribute('test', new SimpleType(String), AccessModifier.PUBLIC)])
        .withInheritedTraits([createEmptyTrait()])
        .withMethods([createMethod('test', AccessModifier.PRIVATE)])
        .buildTrait()
  }

  static Trait createEmptyTrait(String name = 'EmptyTrait') {
    return new Trait.Builder('org.test', name).buildTrait()
  }
}
