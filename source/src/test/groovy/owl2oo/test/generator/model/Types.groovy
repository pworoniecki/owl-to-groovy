package owl2oo.test.generator.model

import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType

class Types {

  static final SimpleType STRING = new SimpleType(String)
  static final ListType STRING_LIST = new ListType(STRING)
}
