package owl2oo.test.generator.model

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.RemoveValueMethod

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute

class RemoveValueMethodTestFactory {

  static RemoveValueMethod createRemoveValueMethod(Attribute attribute) {
    return new RemoveValueMethod.Builder(attribute).buildMethod()
  }

  static RemoveValueMethod createRemoveValueMethod(String attributeName = 'sample') {
    return new RemoveValueMethod.Builder(createAttribute(attributeName, Types.STRING_LIST)).buildMethod()
  }
}
