package owl2oo.test.generator.model

import owl2oo.generator.model.Annotation
import owl2oo.generator.model.Class
import owl2oo.generator.model.type.SimpleType
import owl2oo.resultcodebase.validator.DisjointWith

import static AttributeTestFactory.createAttribute
import static ConstructorTestFactory.createConstructor
import static MethodTestFactory.createMethod

class ClassTestFactory {

  static Class createClass(String className = 'TestClass', String packageName = 'org.test',
                           List<Annotation> annotations = [new Annotation(new SimpleType(DisjointWith))]) {
    return new Class.Builder(packageName, className)
        .withAnnotations(annotations)
        .withConstructors([createConstructor()])
        .withAttributes([createAttribute()])
        .withInheritedClass(createEmptyClass())
        .withMethods([createMethod()])
        .buildClass()
  }

  static Class createEmptyClass(String packageName = 'org.test', String className = 'EmptyClass') {
    return new Class.Builder(packageName, className).buildClass()
  }
}
