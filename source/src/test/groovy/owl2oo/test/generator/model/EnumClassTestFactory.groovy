package owl2oo.test.generator.model

import owl2oo.generator.model.Annotation
import owl2oo.generator.model.EnumClass
import owl2oo.generator.model.type.SimpleType
import owl2oo.resultcodebase.validator.DisjointWith

import static AttributeTestFactory.createAttribute
import static ClassTestFactory.createEmptyClass
import static ConstructorTestFactory.createConstructor
import static MethodTestFactory.createMethod

class EnumClassTestFactory {

  static EnumClass createEnumClass(String className = 'TestEnumClass', String packageName = 'org.test',
                                   List<Annotation> annotations = [new Annotation(new SimpleType(DisjointWith))]) {
    return new EnumClass.Builder(packageName, className)
        .withAnnotations(annotations)
        .withConstructors([createConstructor()])
        .withAttributes([createAttribute()])
        .withInheritedClass(createEmptyClass())
        .withMethods([createMethod()])
        .buildClass()
  }
}
