package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Setter

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute

class SetterTestFactory {

  static Setter createMethod(Attribute attribute = createAttribute()) {
    return new Setter.Builder(attribute).withAccessModifier(AccessModifier.PROTECTED).asStatic()
        .buildMethod()
  }
}
