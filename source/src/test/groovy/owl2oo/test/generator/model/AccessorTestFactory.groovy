package owl2oo.test.generator.model

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Accessor
import owl2oo.generator.model.type.SimpleType
import groovy.transform.InheritConstructors

import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute

class AccessorTestFactory {

  static Accessor createAccessor(String name = 'testAccessor', Attribute attribute = createAttribute()) {
    return new TestAccessor.Builder(name, attribute)
        .withAccessModifier(AccessModifier.PROTECTED)
        .withParameters([new Parameter(new SimpleType(String), 'testParameter')])
        .withReturnType(new SimpleType(Integer))
        .asStatic()
        .buildMethod()
  }

  @InheritConstructors
  static class TestAccessor extends Accessor {

    @InheritConstructors
    static class Builder extends Accessor.Builder<TestAccessor, Builder> {
      @Override
      Builder getThis() {
        return this
      }

      @Override
      TestAccessor buildMethod() {
        return new TestAccessor(this)
      }
    }
  }
}
