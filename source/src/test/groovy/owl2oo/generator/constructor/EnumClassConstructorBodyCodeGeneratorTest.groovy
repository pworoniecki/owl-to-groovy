package owl2oo.generator.constructor

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import owl2oo.resultcodebase.individual.IndividualRepository
import owl2oo.test.generator.model.ConstructorTestFactory
import owl2oo.test.generator.model.EnumClassTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.constructor.EnumClassConstructorBodyCodeGenerator.INDIVIDUAL_NAME_PARAMETER
import static owl2oo.generator.model.AccessModifier.PUBLIC

class EnumClassConstructorBodyCodeGeneratorTest extends Specification {

  private static final SimpleType INTEGER_TYPE = new SimpleType(Integer)
  private static final Parameter PARAMETER_1 = new Parameter(INTEGER_TYPE, 'param1')
  private static final Parameter PARAMETER_2 = new Parameter(new ListType(INTEGER_TYPE), 'param2')

  @Subject
  @Shared
  private EnumClassConstructorBodyCodeGenerator generator = new EnumClassConstructorBodyCodeGenerator()

  def 'should support EnumClass type'() {
    expect:
    generator.supports(EnumClassTestFactory.createEnumClass())
  }

  def 'should generate body code of constructor in enumeration class'() {
    given:
    def constructor = ConstructorTestFactory.createConstructor(PUBLIC, [PARAMETER_1, PARAMETER_2])

    when:
    def constructorBodyCode = generator.generateCode(constructor)

    then:
    constructorBodyCode == """addInstance(this)
                             |${IndividualRepository.simpleName}.addIndividual($INDIVIDUAL_NAME_PARAMETER, this)
                             |setParam1(param1)
                             |setParam2(param2)""".stripMargin()
  }
}
