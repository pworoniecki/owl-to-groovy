package owl2oo.generator

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2oo.generator.model.AccessModifier.PRIVATE
import static owl2oo.generator.model.AccessModifier.PUBLIC

class AttributeCodeGeneratorTest extends Specification {

  private static final SimpleType INTEGER_TYPE = new SimpleType(Integer)

  @Shared
  @Subject
  private AttributeCodeGenerator generator = new AttributeCodeGenerator()

  @Unroll
  def 'should generate code of attribute'() {
    expect:
    generator.generateCode(createAttribute(name, modifier, isStatic, valueType, defaultValue)) == code.toString()

    where:
    name    | modifier | isStatic | valueType                  | defaultValue | code
    'attr1' | PRIVATE  | false    | INTEGER_TYPE               | '5'          | "private Integer attr1 = 5"
    'attr2' | PRIVATE  | true     | INTEGER_TYPE               | '5'          | "private static Integer attr2 = 5"
    'attr3' | PUBLIC   | true     | INTEGER_TYPE               | '5'          | "static Integer attr3 = 5"
    'attr4' | PUBLIC   | false    | INTEGER_TYPE               | '5'          | "Integer attr4 = 5"
    'attr5' | PUBLIC   | false    | INTEGER_TYPE               | null         | "Integer attr5"
    'attr6' | PUBLIC   | false    | new ListType(INTEGER_TYPE) | '[5,6]'      | "List<Integer> attr6 = [5,6]"
  }

  private static Attribute createAttribute(String name, AccessModifier modifier, boolean isStatic,
                                           Type valueType, String defaultValue) {
    def builder = new Attribute.AttributeBuilder(name, valueType).withAccessModifier(modifier)
    if (defaultValue != null) {
      builder = builder.withDefaultValue(defaultValue)
    }
    if (isStatic) {
      builder = builder.asStatic()
    }
    return builder.buildAttribute()
  }
}
