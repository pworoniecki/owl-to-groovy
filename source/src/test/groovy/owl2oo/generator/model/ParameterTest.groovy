package owl2oo.generator.model

import owl2oo.generator.model.type.SimpleType
import spock.lang.Specification

class ParameterTest extends Specification {

  def 'should not allow to create parameter without type or name'() {
    when:
    new Parameter(type, name)

    then:
    thrown NullPointerException

    where:
    type                    | name
    null                    | 'test'
    new SimpleType(Integer) | null
  }

  def 'should return simple declaration consisting of simple name of type and name of parameter'() {
    given:
    def parameterType = new SimpleType(Integer)
    def parameterName = 'test'

    when:
    def parameter = new Parameter(parameterType, parameterName)

    then:
    parameter.simpleDeclaration == "$parameterType.simpleName $parameterName".toString()
  }
}
