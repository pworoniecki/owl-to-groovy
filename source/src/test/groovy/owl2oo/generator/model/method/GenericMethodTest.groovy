package owl2oo.generator.model.method

import spock.lang.Specification

class GenericMethodTest extends Specification {

  def 'should set body of generic method'() {
    given:
    def bodyCode = 'testMethodCode'

    when:
    def method = new GenericMethod.Builder('testMethod', bodyCode).buildMethod()

    then:
    method.bodyCode == bodyCode
  }

  def 'should not allow to create generic method without body code'() {
    given:
    def bodyCode = null

    when:
    new GenericMethod.Builder('testMethod', bodyCode).buildMethod()

    then:
    thrown NullPointerException
  }
}
