package owl2oo.generator.method.addvalue

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import owl2oo.configuration.ApplicationSpringContextConfiguration
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = ApplicationSpringContextConfiguration)
class AddValueBodyCodeGeneratorStrategyOrderTest extends Specification {

  @Autowired
  @Subject
  private List<AddValueBodyCodeGeneratorStrategy> generators

  def 'should assert correct order of add value body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
    generators.sort { it.orderWithinPriorityGroup }*.class == [
        ReflexiveAttributeAddValueBodyCodeGenerator,
        IrreflexiveAttributeAddValueBodyCodeGenerator,
        AsymmetricAttributeAddValueBodyCodeGenerator,
        TransitiveAttributeAddValueBodyCodeGenerator,
        InverseFunctionalAttributeAddValueBodyCodeGenerator,
        SymmetricAttributeAddValueBodyCodeGenerator,
        InverseAttributeAddValueBodyCodeGenerator
    ]
  }
}
