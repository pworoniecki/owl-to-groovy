package owl2oo.generator.method.addvalue

import owl2oo.configuration.ApplicationSpringContextConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = ApplicationSpringContextConfiguration)
class AddValueBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<AddValueBodyCodeGeneratorStrategy> generators

  def 'should assert equal priorities of all add value body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
  }
}
