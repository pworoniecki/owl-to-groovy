package owl2oo.generator.method.addvalue

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.AddValueMethod
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE
import static owl2oo.test.generator.model.AddValueMethodTestFactory.createAddValueMethod

class IrreflexiveAttributeAddValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private IrreflexiveAttributeAddValueBodyCodeGenerator generator = new IrreflexiveAttributeAddValueBodyCodeGenerator()

  def 'should support add value method of irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    generator.supports(addValueMethod)
  }

  def 'should not support add value method of non irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    !generator.supports(addValueMethod)
  }

  def 'should generate validation code of add value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)
    String parameterName = addValueMethod.parameter.name

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        "if ($parameterName == this) {\n" +
        "throw new IrreflexiveAttributeException(\"Cannot add value 'this' - " +
        "it would break irreflexivity of attribute\")\n" +
        '}'
  }

  def 'should generate no specific add value code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificAddValueCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
