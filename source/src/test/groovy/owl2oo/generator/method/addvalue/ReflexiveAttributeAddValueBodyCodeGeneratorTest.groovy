package owl2oo.generator.method.addvalue

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.AddValueMethod
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2oo.test.generator.model.AddValueMethodTestFactory.createAddValueMethod

class ReflexiveAttributeAddValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private ReflexiveAttributeAddValueBodyCodeGenerator generator = new ReflexiveAttributeAddValueBodyCodeGenerator()

  def 'should support add value method of reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    generator.supports(addValueMethod)
  }

  def 'should not support add value method of non reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    !generator.supports(addValueMethod)
  }

  def 'should generate no validation code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate no specific add value code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificAddValueCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
