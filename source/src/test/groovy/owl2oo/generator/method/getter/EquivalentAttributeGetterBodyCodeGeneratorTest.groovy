package owl2oo.generator.method.getter

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.attribute.relation.EquivalentToAttributeRelation
import owl2oo.generator.model.type.SimpleType
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.GetterTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class EquivalentAttributeGetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private EquivalentAttributeGetterBodyCodeGenerator generator = new EquivalentAttributeGetterBodyCodeGenerator()

  def 'should support getter of equivalent attribute'() {
    given:
    def equivalentAttribute = AttributeTestFactory.createAttribute()
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations([new EquivalentToAttributeRelation(equivalentAttribute)]).buildAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    generator.supports(getter)
  }

  def 'should not support getter of non equivalent attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(getter)
  }

  def 'should generate body code of getter'() {
    given:
    def equivalentAttribute = AttributeTestFactory.createAttribute('equivalentAttribute')
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations([new EquivalentToAttributeRelation(equivalentAttribute)])
        .buildAttribute()
    def getter = GetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateCode(getter)

    then:
    generatedCode == 'return getEquivalentAttribute()'
  }
}
