package owl2oo.generator.method.getter

import owl2oo.generator.method.common.TransitiveAttributeHelpersGenerator
import owl2oo.generator.model.attribute.Attribute
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.GetterTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

class TransitiveAttributeGetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private TransitiveAttributeGetterBodyCodeGenerator generator = new TransitiveAttributeGetterBodyCodeGenerator()

  def 'should support getter of transitive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([TRANSITIVE]).buildAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    generator.supports(getter)
  }

  def 'should not support getter of non transitive attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(getter)
  }

  def 'should generate getting value code of list-type getter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([TRANSITIVE]).buildAttribute()
    def getter = GetterTestFactory.createMethod(attribute)
    String attributeName = attribute.name

    when:
    String generatedCode = generator.generateCode(getter)

    then:
    generatedCode ==
        """def result = []
          |${TransitiveAttributeHelpersGenerator.getGetterHelperMethodName(attribute)}(result)
          |return result""".stripMargin()
  }

  def 'should generate getting value code of single-type getter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([TRANSITIVE]).buildAttribute()
    def getter = GetterTestFactory.createMethod(attribute)

    when:
    String generatedCode = generator.generateCode(getter)

    then:
    generatedCode == "return this.${attribute.name}"
  }
}
