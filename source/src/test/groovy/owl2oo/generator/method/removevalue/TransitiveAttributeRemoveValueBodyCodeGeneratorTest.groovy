package owl2oo.generator.method.removevalue

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.resultcodebase.exception.TransitiveAttributeException
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE
import static owl2oo.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class TransitiveAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private TransitiveAttributeRemoveValueBodyCodeGenerator generator = new TransitiveAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of transitive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([TRANSITIVE]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non transitive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate validation code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([TRANSITIVE]).buildAttribute()
    String attributeName = attribute.name
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    Optional<String> generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
    """if (!this.${attributeName}.contains($attributeName)) {
      |throw new ${TransitiveAttributeException.simpleName}("Cannot remove \$$attributeName because it would break transitivity rule")
      |}""".stripMargin()
  }

  def 'should generate no specific remove value code of remove value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([TRANSITIVE]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificRemoveValueCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
