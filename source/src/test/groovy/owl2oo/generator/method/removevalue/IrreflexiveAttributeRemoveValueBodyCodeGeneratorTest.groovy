package owl2oo.generator.method.removevalue

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.generator.model.type.SimpleType
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE
import static owl2oo.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class IrreflexiveAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private IrreflexiveAttributeRemoveValueBodyCodeGenerator generator = new IrreflexiveAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate no validation code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate no specific remove value code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificRemoveValueCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
