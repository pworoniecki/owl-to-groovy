package owl2oo.generator.method.removevalue

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.generator.model.type.SimpleType
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2oo.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class ReflexiveAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private ReflexiveAttributeRemoveValueBodyCodeGenerator generator = new ReflexiveAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate validation code of remove value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)
    String parameterName = removeValueMethod.parameter.name

    when:
    def generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        "if ($parameterName == this) {\n" +
        "throw new ReflexiveAttributeException(\"Cannot remove value 'this' - " +
        "it would break reflexivity of attribute\")\n" +
        '}'
  }

  def 'should generate no specific remove value code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificRemoveValueCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
