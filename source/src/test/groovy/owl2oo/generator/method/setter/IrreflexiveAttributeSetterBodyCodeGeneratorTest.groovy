package owl2oo.generator.method.setter

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.SimpleType
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.SetterTestFactory
import owl2oo.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE
import static java.lang.String.format

class IrreflexiveAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private IrreflexiveAttributeSetterBodyCodeGenerator generator = new IrreflexiveAttributeSetterBodyCodeGenerator()

  def 'should support setter of irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non irreflexive attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate validation code of list-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set \$attribute as it contains 'this' instance - " +
        'it would break irreflexive attribute rule'

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute.contains(this)) {
          |throw new IrreflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate validation code of single-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set 'this' instance - it would break irreflexive attribute rule"

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute == this) {
          |throw new IrreflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificSettingValueCode(setter)

    then:
    !generatedCode.isPresent()
  }
}
