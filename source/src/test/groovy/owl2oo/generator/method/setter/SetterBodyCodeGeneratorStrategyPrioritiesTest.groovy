package owl2oo.generator.method.setter

import owl2oo.configuration.ApplicationSpringContextConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = ApplicationSpringContextConfiguration)
class SetterBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<SetterBodyCodeGeneratorStrategy> generators

  def 'should assert equal priorities of all setter body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
  }
}
