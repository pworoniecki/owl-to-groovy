package owl2oo.generator.method

import owl2oo.generator.model.method.GenericMethod
import spock.lang.Specification
import spock.lang.Subject

class GenericMethodBodyCodeGeneratorTest extends Specification {

  @Subject
  private GenericMethodBodyCodeGenerator generator = new GenericMethodBodyCodeGenerator()

  def 'should generate code of generic method using code stored in it'() {
    given:
    def methodBodyCode = 'test body code'
    def genericMethod = new GenericMethod.Builder('testMethod', methodBodyCode).buildMethod()

    when:
    def generatedCode = generator.generateCode(genericMethod)

    then:
    generatedCode == genericMethod.bodyCode
  }

  def 'should support generic method'() {
    expect:
    generator.supportedClass == GenericMethod
  }
}
