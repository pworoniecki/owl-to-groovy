package owl2oo.converter.attribute

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.converter.attribute.range.OWLDatatypeToTypeConverter
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.type.Type
import owl2oo.resultcodebase.owlSimpleType.Language
import owl2oo.resultcodebase.owlSimpleType.NCName
import owl2oo.resultcodebase.owlSimpleType.NMTOKEN
import owl2oo.resultcodebase.owlSimpleType.Name
import owl2oo.resultcodebase.owlSimpleType.NegativeInteger
import owl2oo.resultcodebase.owlSimpleType.NonNegativeInteger
import owl2oo.resultcodebase.owlSimpleType.NonPositiveInteger
import owl2oo.resultcodebase.owlSimpleType.NormalizedString
import owl2oo.resultcodebase.owlSimpleType.PositiveInteger
import owl2oo.resultcodebase.owlSimpleType.Token
import owl2oo.resultcodebase.owlSimpleType.UnsignedByte
import owl2oo.resultcodebase.owlSimpleType.UnsignedInt
import owl2oo.resultcodebase.owlSimpleType.UnsignedLong
import owl2oo.resultcodebase.owlSimpleType.UnsignedShort
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDatatype
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import javax.xml.datatype.XMLGregorianCalendar

import static owl2oo.utils.SubPackageNameExtractor.getSubPackageName

class OWLDatatypeToTypeConverterTest extends Specification {

  private static final String GENERATED_CODE_BASE_PACKAGE = 'org.test'
  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(basePackage: GENERATED_CODE_BASE_PACKAGE)))

  @Subject
  private OWLDatatypeToTypeConverter datatypeToTypeConverter = new OWLDatatypeToTypeConverter(CONFIGURATION)

  private static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'

  @Unroll
  def 'should convert OWL datatype to Groovy native type'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, name)

    when:
    Type type = datatypeToTypeConverter.convert(datatype)

    then:
    type.fullName == typeClass.name

    where:
    name                 | typeClass
    'string'             | String
    'boolean'            | Boolean
    'decimal'            | BigDecimal
    'float'              | Float
    'double'             | Double
    'integer'            | BigInteger
    'long'               | Long
    'int'                | Integer
    'short'              | Short
    'byte'               | Byte
    'dateTime'           | XMLGregorianCalendar
    'time'               | XMLGregorianCalendar
    'date'               | XMLGregorianCalendar
    'gYearMonth'         | XMLGregorianCalendar
    'gYear'              | XMLGregorianCalendar
    'gMonthDay'          | XMLGregorianCalendar
    'gDay'               | XMLGregorianCalendar
    'gMonth'             | XMLGregorianCalendar
  }

  @Unroll
  def 'should convert OWL datatype to custom type'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, name)
    String expectedTypePackage = GENERATED_CODE_BASE_PACKAGE + '.' + getSubPackageName(typeClass)

    when:
    Type type = datatypeToTypeConverter.convert(datatype)

    then:
    type.fullName == "${expectedTypePackage}.${typeClass.simpleName}"

    where:
    name                 | typeClass
    'normalizedString'   | NormalizedString
    'token'              | Token
    'language'           | Language
    'NMTOKEN'            | NMTOKEN
    'Name'               | Name
    'NCName'             | NCName
    'nonPositiveInteger' | NonPositiveInteger
    'negativeInteger'    | NegativeInteger
    'nonNegativeInteger' | NonNegativeInteger
    'unsignedLong'       | UnsignedLong
    'unsignedInt'        | UnsignedInt
    'unsignedShort'      | UnsignedShort
    'unsignedByte'       | UnsignedByte
    'positiveInteger'    | PositiveInteger
  }

  def 'should throw an exception when trying to convert datatype which is not defined within XML Schema namespace'() {
    given:
    OWLDatatype datatype = createDatatype('invalidNamespace', 'int')

    when:
    datatypeToTypeConverter.convert(datatype)

    then:
    thrown IllegalArgumentException
  }

  def 'should throw an exception when name of datatype is empty'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, '')

    when:
    datatypeToTypeConverter.convert(datatype)

    then:
    thrown MissingIRIRemainderException
  }

  private OWLDatatype createDatatype(String namespace, String name) {
    OWLDatatype datatype = Mock()
    datatype.getIRI() >> IRI.create("$namespace#$name")
    return datatype
  }
}
