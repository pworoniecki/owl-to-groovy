package owl2oo.converter.attribute

import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.converter.attribute.domain.ObjectPropertyDomainExtractor
import owl2oo.converter.attribute.domain.PropertyDirectDomainExtractor
import owl2oo.converter.attribute.range.PropertyDirectRangeExtractor
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type
import owl2oo.test.TestOWLOntologyBuilder
import owl2oo.utils.Logger
import spock.lang.Specification
import spock.lang.Subject

class ObjectPropertyDomainExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(
          basePackage: 'test', modelSubPackage: 'subpackage')))
  private static final String PACKAGE_NAME = CONFIGURATION.generator.generatedClasses.modelFullPackage
  public static final String PROPERTY_1_NAME = 'testProperty1'
  public static final String PROPERTY_1_DOMAIN = 'TestClass1'
  public static final String PROPERTY_1_RANGE = 'TestClass2'
  public static final String PROPERTY_2_NAME = 'testProperty2'
  public static final String PROPERTY_3_NAME = 'testProperty3'
  public static final String PROPERTY_4_NAME = 'testProperty4'
  public static final String PROPERTY_5_NAME = 'testProperty5'

  private Logger logger = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

  @Subject
  private ObjectPropertyDomainExtractor extractor = new ObjectPropertyDomainExtractor(CONFIGURATION, predefinedTypes,
      new PropertyDirectDomainExtractor(logger), new PropertyDirectRangeExtractor(logger))

  def 'should return domain defined directly for object property'() {
    given:
    OWLOntology ontology = createOntologyWithObjectProperties()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return domain defined for equivalent object property when directly one is not available'() {
    given:
    OWLOntology ontology = createOntologyWithObjectProperties()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_2_NAME } as OWLObjectProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return domain defined for super object property when directly one is not available'() {
    given:
    OWLOntology ontology = createOntologyWithObjectProperties()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_3_NAME } as OWLObjectProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return range defined for inverse object property as own domain when directly one is not available'() {
    given:
    OWLOntology ontology = createOntologyWithObjectProperties()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_4_NAME } as OWLObjectProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_RANGE
  }

  def 'should return default Thing type when cannot determine domain of object property'() {
    given:
    OWLOntology ontology = createOntologyWithObjectProperties()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_5_NAME } as OWLObjectProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain == predefinedTypes.thingTraitListType
  }

  def 'should return default Thing type when more than 1 domain is defined for object property and log warning about it'() {
    given:
    def propertyName = 'testProperty'
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations(['Class1', 'Class2'])
        .withObjectPropertyDeclaration(propertyName)
        .withObjectPropertyDomain(propertyName, 'Class1').withObjectPropertyDomain(propertyName, 'Class2')
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain == predefinedTypes.thingTraitListType
    1 * logger.warn(*_)
  }

  private static OWLOntology createOntologyWithObjectProperties() {
    return new TestOWLOntologyBuilder()
        .withClassDeclarations([PROPERTY_1_DOMAIN, PROPERTY_1_RANGE])
        .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME, PROPERTY_3_NAME, PROPERTY_4_NAME, PROPERTY_5_NAME])
        .withObjectPropertyDomain(PROPERTY_1_NAME, PROPERTY_1_DOMAIN)
        .withObjectPropertyRange(PROPERTY_1_NAME, PROPERTY_1_RANGE)
        .withEquivalentObjectProperties(PROPERTY_2_NAME, PROPERTY_1_NAME)
        .withSubObjectPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .withInverseObjectProperties(PROPERTY_4_NAME, PROPERTY_1_NAME)
        .build()
  }
}
