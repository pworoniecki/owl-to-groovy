package owl2oo.converter.attribute

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.converter.trait.TraitsExtractor
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import spock.lang.Specification

class PredefinedTypesTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(generator: new GeneratorConfiguration(
      generatedClasses: new GeneratedClassesConfiguration(basePackage: 'test', modelSubPackage: 'subpackage')
  ))
  private static final String PACKAGE_NAME = CONFIGURATION.generator.generatedClasses.modelFullPackage
  public static final String THING_CLASS_NAME = 'Thing'

  def 'should return Thing class instance'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingClass.name == THING_CLASS_NAME
    predefinedTypes.thingClass.packageName == PACKAGE_NAME
  }

  def 'should return Thing trait instance'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTrait.name == THING_CLASS_NAME + TraitsExtractor.TRAIT_NAME_SUFFIX
    predefinedTypes.thingTrait.packageName == PACKAGE_NAME
  }

  def 'should return single Thing trait type'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTraitType.fullName == new SimpleType(predefinedTypes.thingTrait).fullName
  }

  def 'should return list of Thing trait type'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTraitListType.fullName == new ListType(new SimpleType(predefinedTypes.thingTrait)).fullName
  }
}
