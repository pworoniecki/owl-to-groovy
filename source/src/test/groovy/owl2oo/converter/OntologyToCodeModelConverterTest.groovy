package owl2oo.converter

import owl2oo.converter.attribute.AttributesExtractor
import owl2oo.converter.trait.TraitsExtractor
import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait
import owl2oo.generator.model.attribute.Attribute
import owl2oo.parser.OntologyParser
import owl2oo.test.TestOWLOntologyBuilder
import owl2oo.test.generator.model.AttributeTestFactory
import owl2oo.test.generator.model.ClassTestFactory
import owl2oo.test.generator.model.TraitTestFactory
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import spock.lang.Specification
import spock.lang.Subject

class OntologyToCodeModelConverterTest extends Specification {

  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withObjectPropertyDeclaration('testProperty')
      .withClassDeclaration('TestClass').build()
  private static final Map<OWLProperty, Attribute> ATTRIBUTES = [(getObjectProperty()): AttributeTestFactory.createAttribute()]
  private static final Map<OWLClass, List<Trait>> TRAITS = [(getOwlClass()): [TraitTestFactory.createTrait()]]

  private OntologyParser ontologyParser = Mock()
  private AttributesExtractor attributesExtractor = Mock()
  private ClassesExtractor classesExtractor = Mock()
  private TraitsExtractor traitsExtractor = Mock()

  @Subject
  private OntologyToCodeModelConverter ontologyToCodeConverter = new OntologyToCodeModelConverter(ontologyParser, attributesExtractor,
      classesExtractor, traitsExtractor)

  def 'should convert ontology to object oriented code model'() {
    given:
    List<Class> classes = [ClassTestFactory.createClass()]
    InputStream ontologyInputStream = new ByteArrayInputStream()

    when:
    ObjectOrientedCodeModel code = ontologyToCodeConverter.convert(ontologyInputStream)

    then:
    1 * ontologyParser.parse(ontologyInputStream) >> ONTOLOGY
    1 * attributesExtractor.extract(ONTOLOGY) >> ATTRIBUTES
    1 * classesExtractor.extract(ONTOLOGY, TRAITS) >> classes
    1 * traitsExtractor.extract(ONTOLOGY, ATTRIBUTES) >> TRAITS
    code.classes == classes
    code.traits == TRAITS.values().flatten().toList()
  }

  private static OWLObjectProperty getObjectProperty() {
    return ONTOLOGY.objectPropertiesInSignature().findFirst().get()
  }

  private static OWLClass getOwlClass() {
    return ONTOLOGY.classesInSignature().findFirst().get()
  }
}
