package owl2oo.converter

import owl2oo.generator.model.Constructor
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.AccessModifier.PRIVATE

class ConstructorsExtractorTest extends Specification {

  @Subject
  private ConstructorsExtractor constructorsExtractor = new ConstructorsExtractor()

  def 'should return single private constructor'() {
    when:
    List<Constructor> constructors = constructorsExtractor.extract()

    then:
    constructors.size() == 1
    with(constructors.first()) {
      accessModifier == PRIVATE
      !isStatic()
      parameters.isEmpty()
    }
  }
}
