package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.generator.model.Trait
import owl2oo.generator.model.TraitBuilder
import owl2oo.generator.model.UnionTrait
import owl2oo.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

class TraitBuilderFactoryTest extends Specification {

  private static final String CLASS_NAME = 'TestClass'
  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_NAME).build()
  private static final OWLClass CLASS = ONTOLOGY.classesInSignature().findFirst().get()
  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )

  private UnionClassChecker unionClassChecker = Mock()

  @Subject
  private TraitBuilderFactory factory = new TraitBuilderFactory(CONFIGURATION, unionClassChecker)

  def 'should return default trait builder'() {
    when:
    TraitBuilder builder = factory.createTraitBuilder(CLASS, ONTOLOGY)

    then:
    1 * unionClassChecker.isUnionClass(CLASS, ONTOLOGY) >> false
    builder instanceof Trait.Builder
    Trait traitModel = builder.buildTrait()
    traitModel.packageName == CONFIGURATION.generator.generatedClasses.modelFullPackage
    traitModel.name == CLASS_NAME + TRAIT_NAME_SUFFIX
  }

  def 'should return union trait builder'() {
    when:
    TraitBuilder builder = factory.createTraitBuilder(CLASS, ONTOLOGY)

    then:
    1 * unionClassChecker.isUnionClass(CLASS, ONTOLOGY) >> true
    builder instanceof UnionTrait.Builder
    Trait traitModel = builder.buildTrait()
    traitModel.packageName == CONFIGURATION.generator.generatedClasses.modelFullPackage
    traitModel.name == CLASS_NAME + TRAIT_NAME_SUFFIX
  }
}
