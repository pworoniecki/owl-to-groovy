package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import owl2oo.converter.attribute.AttributeMethodsFactory
import owl2oo.generator.model.Annotation
import owl2oo.generator.model.Trait
import owl2oo.generator.model.TraitBuilder
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import owl2oo.test.TestOWLOntologyBuilder
import owl2oo.test.generator.model.Types
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.generator.model.AccessModifier.PRIVATE
import static owl2oo.generator.model.AccessModifier.PUBLIC
import static owl2oo.test.generator.model.AttributeTestFactory.createAttribute
import static owl2oo.test.generator.model.MethodTestFactory.createMethod

class SimpleBaseTraitExtractorTest extends Specification {

  private static final Type ANNOTATION_TYPE = new SimpleType('org.test', 'SampleAnnotation')
  private static final String TRAIT_PACKAGE = 'org.test'
  private static final String TRAIT_NAME = 'TestTrait'
  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withClassDeclaration('TestClass').build()
  private static final OWLClass CLASS = ONTOLOGY.classesInSignature().findFirst().get()
  private static final Map<OWLProperty, Attribute> ATTRIBUTES = [:]

  private AttributeMethodsFactory attributeMethodsFactory = Mock()
  private ClassPropertyAttributesExtractor classPropertyAttributesExtractor = Mock()
  private TraitBuilderFactory traitBuilderFactory = Mock()
  private AnnotationsExtractor annotationsExtractor = Mock()

  @Subject
  private SimpleBaseTraitExtractor extractor = new SimpleBaseTraitExtractor(attributeMethodsFactory,
      classPropertyAttributesExtractor, traitBuilderFactory, annotationsExtractor)

  def setup() {
    attributeMethodsFactory.createMethods(*_) >> []
    classPropertyAttributesExtractor.extractClassAttributes(*_) >> []
    traitBuilderFactory.createTraitBuilder(*_) >> []
    annotationsExtractor.extract(*_) >> []
  }

  def 'should return trait created by builder returned from factory'() {
    given:
    TraitBuilder traitBuilder = new Trait.Builder(TRAIT_PACKAGE, TRAIT_NAME)

    when:
    Trait extractedTrait = extractor.extract(ONTOLOGY, CLASS, ATTRIBUTES)

    then:
    1 * traitBuilderFactory.createTraitBuilder(CLASS, ONTOLOGY) >> traitBuilder
    extractedTrait.packageName == TRAIT_PACKAGE
    extractedTrait.name == TRAIT_NAME
  }

  def 'should assign annotations to extracted trait'() {
    given:
    TraitBuilder traitBuilder = new Trait.Builder(TRAIT_PACKAGE, TRAIT_NAME)
    List<Annotation> annotations = [new Annotation(ANNOTATION_TYPE, ['parameter1']), new Annotation(ANNOTATION_TYPE, [])]

    when:
    Trait extractedTrait = extractor.extract(ONTOLOGY, CLASS, ATTRIBUTES)

    then:
    1 * traitBuilderFactory.createTraitBuilder(CLASS, ONTOLOGY) >> traitBuilder
    1 * annotationsExtractor.extract(CLASS, ONTOLOGY) >> annotations
    extractedTrait.annotations.is(annotations)
  }

  def 'should assign attributes to extracted trait'() {
    given:
    TraitBuilder traitBuilder = new Trait.Builder(TRAIT_PACKAGE, TRAIT_NAME)
    List<Attribute> attributes = [createAttribute('a1', Types.STRING, PRIVATE), createAttribute('a2', Types.STRING_LIST, PUBLIC)]

    when:
    Trait extractedTrait = extractor.extract(ONTOLOGY, CLASS, ATTRIBUTES)

    then:
    1 * traitBuilderFactory.createTraitBuilder(CLASS, ONTOLOGY) >> traitBuilder
    1 * classPropertyAttributesExtractor.extractClassAttributes(ONTOLOGY, CLASS, ATTRIBUTES) >> attributes
    extractedTrait.attributes.is(attributes)
  }

  def 'should assign methods to extracted trait'() {
    given:
    TraitBuilder traitBuilder = new Trait.Builder(TRAIT_PACKAGE, TRAIT_NAME)
    List<Attribute> attributes = [createAttribute('a1', Types.STRING, PRIVATE), createAttribute('a2', Types.STRING_LIST, PUBLIC)]
    List<Method> methods = [createMethod('method1', PRIVATE), createMethod('method2', PUBLIC)]

    when:
    Trait extractedTrait = extractor.extract(ONTOLOGY, CLASS, ATTRIBUTES)

    then:
    1 * traitBuilderFactory.createTraitBuilder(CLASS, ONTOLOGY) >> traitBuilder
    1 * classPropertyAttributesExtractor.extractClassAttributes(ONTOLOGY, CLASS, ATTRIBUTES) >> attributes
    attributes.each { 1 * attributeMethodsFactory.createMethods(it) >> methods }
    extractedTrait.methods == methods * attributes.size()
  }
}
