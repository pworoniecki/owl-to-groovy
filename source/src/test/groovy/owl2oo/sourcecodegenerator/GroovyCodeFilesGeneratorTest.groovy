package owl2oo.sourcecodegenerator

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.utils.GroovyCodeFormatter
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.test.generator.model.ClassTestFactory.createClass
import static owl2oo.test.generator.model.TraitTestFactory.createTrait

class GroovyCodeFilesGeneratorTest extends Specification {

  private static final String GENERATED_CODE_PACKAGE = 'org'
  private static final String GENERATED_CLASSES_SUB_PACKAGE = 'test'
  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(
          basePackage: GENERATED_CODE_PACKAGE, modelSubPackage: GENERATED_CLASSES_SUB_PACKAGE)))
  private static final String GROOVY_SOURCE_CODE_RELATIVE_PATH = "src/main/groovy/$GENERATED_CODE_PACKAGE"
  private static final String CLASS_1 = 'Class1'
  private static final String CLASS_2 = 'Class2'
  private static final String TRAIT_1 = 'Trait1'
  private static final String TRAIT_2 = 'Trait2'

  @Rule
  private TemporaryFolder temporaryFolder = new TemporaryFolder()

  private GroovyCodeFormatter codeFormatter = Mock()

  @Subject
  private GroovyCodeFilesGenerator generator = new GroovyCodeFilesGenerator(CONFIGURATION, codeFormatter)

  def setup() {
    codeFormatter.format(_ as String) >> { String code -> code }
  }

  def 'should generate base code even when there are no classes nor traits'() {
    given:
    List<ClassSourceCode> classes = []
    List<TraitSourceCode> traits = []
    GroovySourceCode sourceCode = new GroovySourceCode(classes, traits)

    when:
    generator.generate(sourceCode, temporaryFolder.root)

    then:
    temporaryFolder.root.list().toList() == ['build.gradle', 'settings.gradle', 'src']
    !new File(temporaryFolder.root, GROOVY_SOURCE_CODE_RELATIVE_PATH).list().toList().isEmpty()
  }

  def 'should generate no classes nor traits'() {
    given:
    List<ClassSourceCode> classes = []
    List<TraitSourceCode> traits = []
    GroovySourceCode sourceCode = new GroovySourceCode(classes, traits)

    when:
    generator.generate(sourceCode, temporaryFolder.root)

    then:
    listGeneratedClassesAndTraits()*.name.isEmpty()
  }

  def 'should generate classes'() {
    given:
    List<ClassSourceCode> classes = [generateClassCode(CLASS_1), generateClassCode(CLASS_2)]
    List<TraitSourceCode> traits = []
    GroovySourceCode sourceCode = new GroovySourceCode(classes, traits)

    when:
    generator.generate(sourceCode, temporaryFolder.root)

    then:
    File[] sortedGeneratedClasses = listGeneratedClassesAndTraits().sort { it.name }
    sortedGeneratedClasses*.name == [CLASS_1, CLASS_2].collect { it + '.groovy' }
    sortedGeneratedClasses*.text == [CLASS_1, CLASS_2].collect { generateMockClassSourceCode(it) }
  }

  def 'should generate traits'() {
    given:
    List<ClassSourceCode> classes = []
    List<TraitSourceCode> traits = [generateTraitCode(TRAIT_1), generateTraitCode(TRAIT_2)]
    GroovySourceCode sourceCode = new GroovySourceCode(classes, traits)

    when:
    generator.generate(sourceCode, temporaryFolder.root)

    then:
    File[] sortedGeneratedTraits = listGeneratedClassesAndTraits().sort { it.name }
    sortedGeneratedTraits*.name == [TRAIT_1, TRAIT_2].collect { it + '.groovy' }
    sortedGeneratedTraits*.text == [TRAIT_1, TRAIT_2].collect { generateMockTraitSourceCode(it) }
  }

  def 'should generate both classes and traits'() {
    given:
    List<ClassSourceCode> classes = [generateClassCode(CLASS_1), generateClassCode(CLASS_2)]
    List<TraitSourceCode> traits = [generateTraitCode(TRAIT_1), generateTraitCode(TRAIT_2)]
    GroovySourceCode sourceCode = new GroovySourceCode(classes, traits)

    when:
    generator.generate(sourceCode, temporaryFolder.root)

    then:
    File[] sortedGeneratedClassesAndTraits = listGeneratedClassesAndTraits().sort { it.name }
    sortedGeneratedClassesAndTraits*.name == [CLASS_1, CLASS_2, TRAIT_1, TRAIT_2].collect { it + '.groovy' }
    sortedGeneratedClassesAndTraits*.text == [CLASS_1, CLASS_2].collect { generateMockClassSourceCode(it) } +
        [TRAIT_1, TRAIT_2].collect { generateMockTraitSourceCode(it) }
  }

  private File[] listGeneratedClassesAndTraits() {
    return new File(temporaryFolder.root, "$GROOVY_SOURCE_CODE_RELATIVE_PATH/$GENERATED_CLASSES_SUB_PACKAGE").listFiles()
  }

  private static ClassSourceCode generateClassCode(String className) {
    return new ClassSourceCode(createClass(className), generateMockClassSourceCode(className))
  }

  private static String generateMockClassSourceCode(String className) {
    return "Source code for class $className"
  }

  private static TraitSourceCode generateTraitCode(String className) {
    return new TraitSourceCode(createTrait(className), generateMockTraitSourceCode(className))
  }

  private static String generateMockTraitSourceCode(String traitName) {
    return "Source code for trait $traitName"
  }
}
