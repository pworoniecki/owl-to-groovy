package owl2oo.sourcecodegenerator

import owl2oo.converter.ObjectOrientedCodeModel
import owl2oo.converter.OntologyToCodeModelConverter
import owl2oo.generator.ClassCodeGenerator
import owl2oo.generator.TraitCodeGenerator
import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait
import spock.lang.Specification
import spock.lang.Subject

import static owl2oo.test.generator.model.ClassTestFactory.createClass
import static owl2oo.test.generator.model.TraitTestFactory.createTrait

class OwlToGroovyCodeConverterTest extends Specification {

  private static final String DUMMY_FILE_NAME = 'filename'

  private OntologyToCodeModelConverter ontologyToCodeModelConverter = Mock()
  private ClassCodeGenerator classCodeGenerator = Mock()
  private TraitCodeGenerator traitCodeGenerator = Mock()

  private File owlOntologyFile = GroovyMock(File, constructorArgs: [DUMMY_FILE_NAME])
  private BufferedInputStream ontologyInputStream = Mock()

  @Subject
  private OwlToGroovyCodeConverter owlToGroovyCodeConverter =
      new OwlToGroovyCodeConverter(ontologyToCodeModelConverter, classCodeGenerator, traitCodeGenerator)

  def 'should return no source code if there are none class nor traits'() {
    given:
    List<Class> classes = []
    List<Trait> traits = []
    ontologyToCodeModelConverter.convert(ontologyInputStream) >> new ObjectOrientedCodeModel(classes, traits)

    when:
    GroovySourceCode sourceCode = owlToGroovyCodeConverter.convert(owlOntologyFile)

    then:
    1 * owlOntologyFile.newInputStream() >> ontologyInputStream
    sourceCode.classes.isEmpty()
    sourceCode.traits.isEmpty()
  }

  def 'should return source code for all classes present in ontology'() {
    given:
    List<Class> classes = [createClass('Class1'), createClass('Class2')]
    List<Trait> traits = []
    ontologyToCodeModelConverter.convert(ontologyInputStream) >> new ObjectOrientedCodeModel(classes, traits)

    when:
    GroovySourceCode sourceCode = owlToGroovyCodeConverter.convert(owlOntologyFile)

    then:
    1 * owlOntologyFile.newInputStream() >> ontologyInputStream
    classes.each {
      1 * classCodeGenerator.generateCode(it) >> generateMockSourceCode(it)
    }
    assertCorrectSourceCodeForClasses(sourceCode, classes)
    sourceCode.traits.isEmpty()
  }

  def 'should return source code for all traits present in ontology'() {
    given:
    List<Class> classes = []
    List<Trait> traits = [createTrait('Trait1'), createTrait('Trait2')]
    ontologyToCodeModelConverter.convert(ontologyInputStream) >> new ObjectOrientedCodeModel(classes, traits)

    when:
    GroovySourceCode sourceCode = owlToGroovyCodeConverter.convert(owlOntologyFile)

    then:
    1 * owlOntologyFile.newInputStream() >> ontologyInputStream
    traits.each {
      1 * traitCodeGenerator.generateCode(it) >> generateMockSourceCode(it)
    }
    assertCorrectSourceCodeForTraits(sourceCode, traits)
    sourceCode.classes.isEmpty()
  }

  def 'should return source code for all classes and traits present in ontology'() {
    given:
    List<Class> classes = [createClass('Class1', 'Class2')]
    List<Trait> traits = [createTrait('Trait1'), createTrait('Trait2')]
    ontologyToCodeModelConverter.convert(ontologyInputStream) >> new ObjectOrientedCodeModel(classes, traits)

    when:
    GroovySourceCode sourceCode = owlToGroovyCodeConverter.convert(owlOntologyFile)

    then:
    1 * owlOntologyFile.newInputStream() >> ontologyInputStream
    classes.each {
      1 * classCodeGenerator.generateCode(it) >> generateMockSourceCode(it)
    }
    traits.each {
      1 * traitCodeGenerator.generateCode(it) >> generateMockSourceCode(it)
    }
    assertCorrectSourceCodeForClasses(sourceCode, classes)
    assertCorrectSourceCodeForTraits(sourceCode, traits)
  }

  private static String generateMockSourceCode(Class classModel) {
    return classModel.name
  }

  private static void assertCorrectSourceCodeForClasses(GroovySourceCode sourceCode, List<Class> classes) {
    assert sourceCode.classes*.classModel == classes
    sourceCode.classes.each {
      assert it.sourceCode == generateMockSourceCode(it.classModel)
    }
  }

  private static String generateMockSourceCode(Trait traitModel) {
    return traitModel.name
  }

  private static void assertCorrectSourceCodeForTraits(GroovySourceCode sourceCode, List<Trait> traits) {
    assert sourceCode.traits*.traitModel == traits
    sourceCode.traits.each {
      assert it.sourceCode == generateMockSourceCode(it.traitModel)
    }
  }
}
