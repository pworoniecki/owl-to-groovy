package owl2oo.resultcodebase.owlSimpleType

import owl2oo.resultcodebase.exception.OwlTypeIllegalArgumentException

class NonPositiveInteger extends OwlType<BigInteger> {

  private static final String XSD_TYPE = 'nonPositiveInteger'

  NonPositiveInteger(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  void validate(BigInteger value) {
    if (value > 0) {
      throw new OwlTypeIllegalArgumentException(value, this, 'Value is positive')
    }
  }
}
