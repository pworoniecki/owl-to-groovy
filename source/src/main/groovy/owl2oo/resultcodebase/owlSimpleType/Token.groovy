package owl2oo.resultcodebase.owlSimpleType

class Token extends NormalizedString {

  private static final String XSD_TYPE = 'token'

  Token(String value) {
    super(value, XSD_TYPE)
  }
}
