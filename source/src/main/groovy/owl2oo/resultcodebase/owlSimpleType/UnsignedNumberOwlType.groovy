package owl2oo.resultcodebase.owlSimpleType

import owl2oo.resultcodebase.exception.OwlTypeIllegalArgumentException
import groovy.transform.InheritConstructors

@InheritConstructors
abstract class UnsignedNumberOwlType extends OwlType<BigInteger> {

  @Override
  void validate(BigInteger value) {
    BigIntegerRange allowedRange = getAllowedRange()
    if (!(value in allowedRange)) {
      throw new OwlTypeIllegalArgumentException(value, this, "Value is not in allowed range ($allowedRange)")
    }
  }

  protected abstract BigIntegerRange getAllowedRange()

  protected static class BigIntegerRange {
    BigInteger from
    BigInteger to

    boolean isCase(Number number) {
      BigInteger bigIntegerNumber = number.toBigInteger()
      return bigIntegerNumber >= from && bigIntegerNumber <= to
    }
  }
}
