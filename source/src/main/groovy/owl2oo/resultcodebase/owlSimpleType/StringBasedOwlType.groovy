package owl2oo.resultcodebase.owlSimpleType

import owl2oo.resultcodebase.exception.OwlTypeIllegalArgumentException
import groovy.transform.InheritConstructors

@InheritConstructors
abstract class StringBasedOwlType extends OwlType<String> {

  @Override
  void validate(String value) {
    String regexp = getRegexp()
    if (value !=~ regexp) {
      throw new OwlTypeIllegalArgumentException(value, this, "Value doesn't match regular expression '$regexp'")
    }
  }

  protected abstract String getRegexp()
}
