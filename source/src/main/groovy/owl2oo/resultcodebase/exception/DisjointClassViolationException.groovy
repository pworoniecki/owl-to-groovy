package owl2oo.resultcodebase.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class DisjointClassViolationException extends Exception {
}
