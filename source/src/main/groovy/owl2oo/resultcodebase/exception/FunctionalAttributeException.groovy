package owl2oo.resultcodebase.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class FunctionalAttributeException extends Exception {
}
