package owl2oo.resultcodebase.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class OwlTypeIllegalArgumentException extends Exception {

  OwlTypeIllegalArgumentException(Object invalidValue, owl2oo.resultcodebase.owlSimpleType.OwlType owlType, String reason) {
    super("Provided value '$invalidValue' is invalid value of type 'xsd:${owlType.xsdType}'. Reason: $reason")
  }
}
