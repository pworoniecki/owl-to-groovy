package owl2oo.resultcodebase.validator

import owl2oo.resultcodebase.exception.IllegalValuesException
import owl2oo.resultcodebase.exception.InvalidArgumentException

class OneOfValuesValidator {

  static void validateValue(Object value, List<Object> allowedValues) {
    validateValues([value], allowedValues)
  }

  static void validateValues(List<Object> values, List<Object> allowedValues) {
    if (!allowedValues) {
      throw new InvalidArgumentException('Missing allowed values')
    }
    values.each {
      if (!allowedValues.contains(it)) {
        throw new IllegalValuesException("Value '$it' is not allowed")
      }
    }
  }
}
