package owl2oo.resultcodebase.individual

import owl2oo.resultcodebase.exception.IndividualDuplicationException

class IndividualRepository {
  private static Map<String, IndividualWrapper> individuals = [:]

  private IndividualRepository() {}

  static IndividualWrapper getIndividual(String name) {
    return individuals[name]
  }

  static void addIndividual(IndividualWrapper individual) throws IndividualDuplicationException {
    if (individuals[individual.name]) {
      throw new IndividualDuplicationException()
    }
    individuals += [(individual.name): individual]
  }

  static void addIndividual(String name, Object individualClassInstance) throws IndividualDuplicationException {
    if (individuals[name]) {
      throw new IndividualDuplicationException()
    }
    individuals += [(name): new IndividualWrapper(name, individualClassInstance)]
  }
}
