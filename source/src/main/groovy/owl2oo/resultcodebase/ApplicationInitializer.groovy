package owl2oo.resultcodebase

import owl2oo.resultcodebase.individual.IndividualParser
import owl2oo.resultcodebase.individual.IndividualWrapper
import owl2oo.resultcodebase.validator.DisjointClassesValidator

class ApplicationInitializer {

  private static final String GENERATED_INDIVIDUALS_RELATIVE_PATH = 'generated-individuals'

  static void initialize() {
    DisjointClassesValidator.checkDisjointClasses(ApplicationInitializer.package.name)
    initializeIndividuals()
  }

  private static void initializeIndividuals() {
    List<IndividualWrapper> individuals = []
    def individualsDirectory = new File(GENERATED_INDIVIDUALS_RELATIVE_PATH)
    if (!individualsDirectory.exists()) {
      return
    }
    individualsDirectory.eachFile {
      individuals += IndividualParser.parse(it.text)
    }
    individuals.findAll { it.hasClassAssigned() }.each { it.contentInstance }
  }

  static void main(String... args) {
    initialize()
  }
}
