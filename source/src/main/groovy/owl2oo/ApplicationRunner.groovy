package owl2oo

import owl2oo.configuration.ApplicationSpringContextConfiguration
import owl2oo.gui.UserInterfaceWindow
import owl2oo.sourcecodegenerator.GroovyCodeFilesGenerator
import owl2oo.sourcecodegenerator.OwlToGroovyCodeConverter
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class ApplicationRunner {

  static void main(String[] args) {
    ApplicationContext applicationContext = initializeSpringContext()
    OwlToGroovyCodeConverter owlToGroovyCodeConverter = applicationContext.getBean(OwlToGroovyCodeConverter)
    GroovyCodeFilesGenerator groovyCodeFilesGenerator = applicationContext.getBean(GroovyCodeFilesGenerator)
    new UserInterfaceWindow(owlToGroovyCodeConverter, groovyCodeFilesGenerator)
  }

  private static ApplicationContext initializeSpringContext() {
    def applicationContext = new AnnotationConfigApplicationContext()
    applicationContext.register(ApplicationSpringContextConfiguration)
    applicationContext.refresh()
    return applicationContext
  }
}
