package owl2oo.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class IllegalArgumentException extends Exception {
}
