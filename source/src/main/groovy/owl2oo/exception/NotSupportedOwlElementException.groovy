package owl2oo.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class NotSupportedOwlElementException extends Exception {
}
