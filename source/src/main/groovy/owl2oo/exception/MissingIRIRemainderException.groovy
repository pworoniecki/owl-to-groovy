package owl2oo.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class MissingIRIRemainderException extends Exception {
}
