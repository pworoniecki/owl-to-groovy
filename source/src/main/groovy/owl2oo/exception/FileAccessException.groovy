package owl2oo.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class FileAccessException extends Exception {
}
