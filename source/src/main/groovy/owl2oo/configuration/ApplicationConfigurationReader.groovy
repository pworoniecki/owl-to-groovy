package owl2oo.configuration

import owl2oo.configuration.model.ApplicationConfiguration
import org.yaml.snakeyaml.Yaml

class ApplicationConfigurationReader {

  static ApplicationConfiguration readConfiguration(String configurationFilePath) {
    def configurationFileStream = this.classLoader.getResourceAsStream(configurationFilePath)
    return new Yaml().loadAs(configurationFileStream, ApplicationConfiguration)
  }
}
