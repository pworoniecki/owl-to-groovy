package owl2oo.configuration

import owl2oo.ApplicationRunner
import owl2oo.configuration.model.ApplicationConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = ApplicationRunner)
class ApplicationSpringContextConfiguration {

  private static final String APPLICATION_CONFIGURATION_FILE_NAME = 'configuration.yml'

  @Bean
  ApplicationConfiguration applicationConfiguration() {
    return ApplicationConfigurationReader.readConfiguration(APPLICATION_CONFIGURATION_FILE_NAME)
  }
}
