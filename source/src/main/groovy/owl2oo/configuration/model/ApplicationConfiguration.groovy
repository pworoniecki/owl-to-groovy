package owl2oo.configuration.model

class ApplicationConfiguration {

  private GeneratorConfiguration generator

  GeneratorConfiguration getGenerator() {
    return generator
  }

  void setGenerator(GeneratorConfiguration generator) {
    this.generator = generator
  }
}
