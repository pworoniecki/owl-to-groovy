package owl2oo.configuration.model

class GeneratorConfiguration {

  private GeneratedClassesConfiguration generatedClasses

  GeneratedClassesConfiguration getGeneratedClasses() {
    return generatedClasses
  }

  void setGeneratedClasses(GeneratedClassesConfiguration generatedClass) {
    this.generatedClasses = generatedClass
  }
}
