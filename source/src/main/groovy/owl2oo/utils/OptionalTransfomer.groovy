package owl2oo.utils

import java.util.function.Supplier

import static com.google.common.base.Preconditions.checkNotNull

class OptionalTransfomer<T> {

  private Optional<T> wrappedOptional

  private OptionalTransfomer(Optional<T> optional) {
    this.wrappedOptional = optional
  }

  static <T> OptionalTransfomer<T> of(Optional<T> optional) {
    return new OptionalTransfomer<T>(checkNotNull(optional))
  }

  OptionalTransfomer<T> or(Supplier<Optional<T>> alternativeOptionalSupplier) {
    if (!wrappedOptional.isPresent()) {
      this.wrappedOptional = alternativeOptionalSupplier.get()
    }
    return this
  }

  Optional<T> getResult() {
    return wrappedOptional
  }
}
