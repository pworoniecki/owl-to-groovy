package owl2oo.utils

class SubPackageNameExtractor {

  static String getSubPackageName(Class clazz) {
    return org.springframework.util.StringUtils.unqualify(clazz.package.name)
  }
}
