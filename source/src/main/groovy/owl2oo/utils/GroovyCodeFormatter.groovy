package owl2oo.utils

import owl2oo.configuration.model.ApplicationConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class GroovyCodeFormatter {

  private final int indentationStep
  private int indentationLevel

  @Autowired
  GroovyCodeFormatter(ApplicationConfiguration applicationConfiguration) {
    this.indentationStep = applicationConfiguration.generator.generatedClasses.indentationStep
  }

  String format(String sourceCode) {
    this.indentationLevel = 0
    def codeLines = sourceCode.readLines()
    return appendIndentation(codeLines).join('\n')
  }

  private List<String> appendIndentation(List<String> lines) {
    lines.eachWithIndex {
      line, index ->
        if (line.endsWith('}') && !line.contains('{')) {
          this.indentationLevel--
        }
        lines[index] = appendIndentation(line)
        if (line.endsWith('{')) {
          this.indentationLevel++
        }
    }
    return lines
  }

  private String appendIndentation(String line) {
    def indentation = ' ' * this.indentationLevel * this.indentationStep
    return indentation + line
  }
}
