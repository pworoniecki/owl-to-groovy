package owl2oo.utils

class CamelCaseToUnderScoreCaseConverter {

  static String camelCaseToUnderScoreCase(String name) {
    return name.replaceAll(/\B[A-Z]/) { "_$it" }.toUpperCase()
  }
}
