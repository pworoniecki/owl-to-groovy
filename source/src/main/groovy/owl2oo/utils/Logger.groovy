package owl2oo.utils

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class Logger {

  private final org.slf4j.Logger logger = LoggerFactory.getLogger(Logger)

  void warn(String messageFormat, Object... messageArguments) {
    logger.warn(messageFormat, messageArguments)
  }
}
