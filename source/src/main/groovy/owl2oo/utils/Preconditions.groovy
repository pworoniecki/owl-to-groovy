package owl2oo.utils

class Preconditions {

  static void assertNoNullInCollection(Collection<?> collection) {
    if (collection.any { it == null }) {
      throw new IllegalArgumentException("There is forbidden null element in collection: $collection")
    }
  }
}
