package owl2oo.utils

class StringUtils {

  static String emptyOrAddSpace(String str) {
    return str.isEmpty() ? str : "$str "
  }
}
