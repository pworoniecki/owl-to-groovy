package owl2oo.parser

import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.functional.parser.OWLFunctionalSyntaxOWLParserFactory
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLOntologyManager
import org.semanticweb.owlapi.owlxml.parser.OWLXMLParserFactory
import org.semanticweb.owlapi.rdf.rdfxml.parser.RDFXMLParserFactory
import org.springframework.stereotype.Component

@Component
class OntologyParser {

  OWLOntology parse(InputStream ontologyContent) {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager()
    manager.ontologyParsers = [new RDFXMLParserFactory(), new OWLXMLParserFactory(),
                               new OWLFunctionalSyntaxOWLParserFactory()].toSet()
    return manager.loadOntologyFromOntologyDocument(ontologyContent)
  }
}
