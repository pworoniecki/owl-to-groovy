package owl2oo.generator

import owl2oo.generator.constructor.ConstructorCodeGenerator
import owl2oo.generator.method.MethodCodeGenerator
import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.method.Method
import owl2oo.generator.model.attribute.Attribute
import groovy.transform.PackageScope
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ClassBodyCodeGenerator implements ClassElementVisitor {

  private ConstructorCodeGenerator constructorCodeGenerator
  private AttributeCodeGenerator attributeCodeGenerator
  private MethodCodeGenerator methodCodeGenerator

  @Autowired
  ClassBodyCodeGenerator(ConstructorCodeGenerator constructorCodeGenerator,
                         AttributeCodeGenerator attributeCodeGenerator, MethodCodeGenerator methodCodeGenerator) {
    this.constructorCodeGenerator = constructorCodeGenerator
    this.attributeCodeGenerator = attributeCodeGenerator
    this.methodCodeGenerator = methodCodeGenerator
  }

  /* they could be divided into static and non-static constructors, attributes, method to improve performance
   * as static elements are analyzed separately to put them before non-static ones
   * but it would decrease readability of code and performance is still good enough
   */
  private Map<Constructor, String> constructorCodes = [:]
  private Map<Attribute, String> attributeCodes = [:]
  private Map<Method, String> methodCodes = [:]

  @PackageScope Map<Constructor, String> getConstructorCodes() {
    return constructorCodes
  }

  @PackageScope Map<Attribute, String> getAttributeCodes() {
    return attributeCodes
  }

  @PackageScope Map<Method, String> getMethodCodes() {
    return methodCodes
  }

  @Override
  void visit(Constructor constructor, Class constructorOwner) {
    def sourceCode = constructorCodeGenerator.generateCode(constructor, constructorOwner)
    constructorCodes.put(constructor, sourceCode)
  }

  @Override
  void visit(Attribute attribute) {
    def sourceCode = attributeCodeGenerator.generateCode(attribute)
    attributeCodes.put(attribute, sourceCode)
  }

  @Override
  void visit(Method method) {
    def sourceCode = methodCodeGenerator.generateCode(method)
    methodCodes.put(method, sourceCode)
  }
}
