package owl2oo.generator

import owl2oo.generator.method.MethodCodeGenerator
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method
import groovy.transform.PackageScope
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class TraitBodyCodeGenerator implements TraitElementVisitor {

  private AttributeCodeGenerator attributeCodeGenerator
  private MethodCodeGenerator methodCodeGenerator

  @Autowired
  TraitBodyCodeGenerator(AttributeCodeGenerator attributeCodeGenerator, MethodCodeGenerator methodCodeGenerator) {
    this.attributeCodeGenerator = attributeCodeGenerator
    this.methodCodeGenerator = methodCodeGenerator
  }

  /* they could be divided into static and non-static constructors, attributes, method to improve performance
   * as static elements are analyzed separately to put them before non-static ones
   * but it would decrease readability of code and performance is still good enough
   */
  private Map<Attribute, String> attributeCodes = [:]
  private Map<Method, String> methodCodes = [:]

  @PackageScope
  Map<Attribute, String> getAttributeCodes() {
    return attributeCodes
  }

  @PackageScope
  Map<Method, String> getMethodCodes() {
    return methodCodes
  }

  @Override
  void visit(Attribute attribute) {
    def sourceCode = attributeCodeGenerator.generateCode(attribute)
    attributeCodes.put(attribute, sourceCode)
  }

  @Override
  void visit(Method method) {
    def sourceCode = methodCodeGenerator.generateCode(method)
    methodCodes.put(method, sourceCode)
  }
}
