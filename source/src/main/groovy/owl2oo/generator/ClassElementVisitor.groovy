package owl2oo.generator

import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.method.Method
import owl2oo.generator.model.attribute.Attribute

interface ClassElementVisitor {

  void visit(Constructor constructor, Class constructorOwner)

  void visit(Attribute attribute)

  void visit(Method method)
}
