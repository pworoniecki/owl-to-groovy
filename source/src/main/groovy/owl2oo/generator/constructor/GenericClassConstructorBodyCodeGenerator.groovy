package owl2oo.generator.constructor

import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import org.springframework.stereotype.Component

@Component
class GenericClassConstructorBodyCodeGenerator implements ConstructorBodyCodeGenerator {

  private static String EMPTY_CODE = ''

  @Override
  boolean supports(Class constructorOwner) {
    return constructorOwner?.class == Class
  }

  @Override
  String generateCode(Constructor constructor) {
    return EMPTY_CODE
  }
}
