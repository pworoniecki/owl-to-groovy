package owl2oo.generator.constructor

import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor

interface ConstructorBodyCodeGenerator {

  boolean supports(Class ownerClass)

  String generateCode(Constructor constructor)
}
