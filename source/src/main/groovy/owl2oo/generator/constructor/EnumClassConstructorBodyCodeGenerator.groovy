package owl2oo.generator.constructor

import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.EnumClass
import owl2oo.resultcodebase.individual.IndividualRepository
import org.springframework.stereotype.Component

@Component
class EnumClassConstructorBodyCodeGenerator implements ConstructorBodyCodeGenerator {

  static final String INDIVIDUAL_NAME_PARAMETER = 'individualName'

  @Override
  boolean supports(Class constructorOwner) {
    return constructorOwner?.class == EnumClass
  }

  @Override
  String generateCode(Constructor constructor) {
    def parameterSettersInvocations = constructor.parameters.findAll { it.name != INDIVIDUAL_NAME_PARAMETER }
        .collect { "set${it.name.capitalize()}(${it.name})" }.join('\n')

    return """addInstance(this)
             |${IndividualRepository.simpleName}.addIndividual($INDIVIDUAL_NAME_PARAMETER, this)
             |$parameterSettersInvocations""".stripMargin()
  }
}
