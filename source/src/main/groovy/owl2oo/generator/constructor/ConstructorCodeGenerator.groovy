package owl2oo.generator.constructor

import owl2oo.generator.exception.ConstructorBodyCodeGeneratorNotFoundException
import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.utils.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ConstructorCodeGenerator {

  private List<ConstructorBodyCodeGenerator> bodyCodeGenerators

  @Autowired
  ConstructorCodeGenerator(List<ConstructorBodyCodeGenerator> bodyCodeGenerators) {
    this.bodyCodeGenerators = bodyCodeGenerators
  }

  String generateCode(Constructor constructor, Class constructorOwner) {
    def codeBuilder = new StringBuilder()
    codeBuilder.with {
      append(constructorSignature(constructor, constructorOwner) + '\n')
      append(constructorBody(constructor, constructorOwner) + '\n')
      append('}')
    }
    return codeBuilder.toString()
  }

  private static String constructorSignature(Constructor constructor, Class constructorOwner) {
    def accessModifier = StringUtils.emptyOrAddSpace(constructor.accessModifier.toGroovyCode())
    def staticModifier = constructor.isStatic() ? 'static ' : ''
    def parameters = constructor.parameters.collect { it.simpleDeclaration }.join(', ')

    return "$accessModifier$staticModifier$constructorOwner.name($parameters) {"
  }

  private String constructorBody(Constructor constructor, Class constructorOwner) {
    def bodyCodeGenerator = bodyCodeGenerators.find { it.supports(constructorOwner) }
    if (bodyCodeGenerator == null) {
      throw new ConstructorBodyCodeGeneratorNotFoundException(constructorOwner)
    }
    return bodyCodeGenerator.generateCode(constructor)
  }
}
