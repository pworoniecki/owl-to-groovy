package owl2oo.generator

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.generator.model.Class
import owl2oo.generator.model.codebuilder.ClassCodeBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ClassCodeGenerator {

  private ClassBodyCodeGenerator classBodyGenerator
  private ClassSignatureCodeGenerator classSignatureGenerator
  private GeneratorConfiguration generatorConfiguration

  @Autowired
  ClassCodeGenerator(ClassBodyCodeGenerator classBodyGenerator, ClassSignatureCodeGenerator classSignatureGenerator,
                     ApplicationConfiguration applicationConfiguration) {
    this.classBodyGenerator = classBodyGenerator
    this.classSignatureGenerator = classSignatureGenerator
    this.generatorConfiguration = applicationConfiguration.generator
  }

  String generateCode(Class classModel) {
    classModel.accept(classBodyGenerator)

    def codeBuilder = new ClassCodeBuilder(classModel)
    codeBuilder.with {
      appendPackage()
      appendEmptyLine()
      appendImports(generatorConfiguration)
      appendEmptyLine()
      appendAnnotations(classModel.annotations)
      appendCode(classSignatureGenerator.generateClassSignature(classModel))
      appendEmptyLine()
      appendStaticAttributes(classBodyGenerator.attributeCodes)
      appendStaticConstructorsWithEmptyLines(classBodyGenerator.constructorCodes)
      appendNonStaticAttributes(classBodyGenerator.attributeCodes)
      appendNonStaticConstructorsWithEmptyLines(classBodyGenerator.constructorCodes)
      appendStaticMethodsWithEmptyLines(classBodyGenerator.methodCodes)
      appendNonStaticMethodsWithEmptyLines(classBodyGenerator.methodCodes)
      appendClassSignatureEnd()
      appendEmptyLine()
    }

    return codeBuilder.code
  }
}
