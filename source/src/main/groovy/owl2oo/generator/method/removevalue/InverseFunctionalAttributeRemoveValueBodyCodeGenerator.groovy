package owl2oo.generator.method.removevalue

import owl2oo.generator.model.method.RemoveValueMethod
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL

@Component
class InverseFunctionalAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return INVERSE_FUNCTIONAL in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 6
  }
}