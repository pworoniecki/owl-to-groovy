package owl2oo.generator.method.removevalue

import org.springframework.stereotype.Component
import owl2oo.generator.model.method.RemoveValueMethod

import static owl2oo.generator.model.attribute.AttributeCharacteristic.SYMMETRIC

@Component
class SymmetricAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return SYMMETRIC in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method) {
    return Optional.of("${method.parameter.name}.remove${method.attribute.name.capitalize()}(this)")
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 7
  }
}
