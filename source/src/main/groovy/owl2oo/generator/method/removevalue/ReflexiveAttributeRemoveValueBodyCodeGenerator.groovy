package owl2oo.generator.method.removevalue

import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.resultcodebase.exception.ReflexiveAttributeException
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class ReflexiveAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return REFLEXIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    def methodParameterName = method.parameter.name
    def validationCode = generateCode([
        "if ($methodParameterName == this) {",
        "throw new ${ReflexiveAttributeException.simpleName}(\"Cannot remove value 'this' - " +
            "it would break reflexivity of attribute\")",
        '}',
    ])
    return Optional.of(validationCode)
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 1
  }
}
