package owl2oo.generator.method.removevalue

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.generator.method.MethodBodyCodeGenerator
import owl2oo.generator.model.method.Getter
import owl2oo.generator.model.method.RemoveValueMethod

@Component
class RemoveValueMethodBodyCodeGenerator implements MethodBodyCodeGenerator<RemoveValueMethod> {

  private List<RemoveValueBodyCodeGeneratorStrategy> generators

  @Autowired
  RemoveValueMethodBodyCodeGenerator(List<RemoveValueBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  @Override
  String generateCode(RemoveValueMethod method) {
    List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators =
        findGenerators(method).sort { it.getOrderWithinPriorityGroup() }
    List<String> validationCode = [generateGeneralValidationCode(method)] +
        generateSpecificValidationCode(orderedGenerators, method)
    List<String> removeValueCode = [generateGeneralRemoveValueCode(method)] +
        generateSpecificRemoveValueCode(orderedGenerators, method)
    return (validationCode + removeValueCode).join('\n')
  }

  private List<RemoveValueBodyCodeGeneratorStrategy> findGenerators(RemoveValueMethod method) {
    def possibleGenerators = generators.findAll { it.supports(method) }
    def minGeneratorPriority = possibleGenerators*.priority.min()
    return possibleGenerators.findAll { it.priority == minGeneratorPriority }
  }

  private static String generateGeneralValidationCode(RemoveValueMethod method) {
    String attributeGetterName = Getter.getName(method.attribute)
    return "if (${method.parameter.name} == null || !this.${attributeGetterName}().contains(${method.parameter.name})) { return }"
  }

  private static List<String> generateSpecificValidationCode(List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                             RemoveValueMethod method) {
    return orderedGenerators.collect { it.generateValidationCode(method) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static List<String> generateSpecificRemoveValueCode(List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                           RemoveValueMethod method) {
    return orderedGenerators.collect { it.generateSpecificRemoveValueCode(method) }.findAll { it.isPresent() }.collect { it.get() }
  }

  private static String generateGeneralRemoveValueCode(RemoveValueMethod method) {
    return "this.${method.attribute.name}.remove(${method.parameter.name})"
  }

  @Override
  Class<RemoveValueMethod> getSupportedClass() {
    return RemoveValueMethod
  }
}
