package owl2oo.generator.method.removevalue

import owl2oo.generator.model.method.RemoveValueMethod

trait RemoveValueBodyCodeGeneratorStrategy {

  abstract boolean supports(RemoveValueMethod method)

  abstract Optional<String> generateValidationCode(RemoveValueMethod method)

  abstract Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method)

  String generateCode(List<String> codeLines) {
    return codeLines.join('\n')
  }

  // lower value means higher priority
  int getPriority() {
    return Integer.MAX_VALUE
  }

  // lower value means that generator will be used earlier
  int getOrderWithinPriorityGroup() {
    return Integer.MAX_VALUE
  }
}
