package owl2oo.generator.method.removevalue

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.DirectSetter
import owl2oo.generator.model.method.RemoveValueMethod
import org.springframework.stereotype.Component
import owl2oo.generator.model.type.ListType

@Component
class InverseAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return method.attribute.isInverseAttribute()
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method) {
    List<Attribute> inverseAttributes = method.attribute.findInverseAttributes()
    return Optional.of(generateCode(removeSelfFromAttributes(inverseAttributes, method.parameter)))
  }

  private static List<String> removeSelfFromAttributes(List<Attribute> attributes, Parameter methodParameter) {
    return attributes.collect { return removeSelfFromAttribute(it, methodParameter) }
  }

  private static String removeSelfFromAttribute(Attribute attribute, Parameter methodParameter) {
    if (attribute.valueType instanceof ListType) {
      return "${methodParameter.name}.remove${attribute.name.capitalize()}(this)"
    }
    return "${methodParameter.name}.${DirectSetter.getName(attribute)}(null)"
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 8
  }
}
