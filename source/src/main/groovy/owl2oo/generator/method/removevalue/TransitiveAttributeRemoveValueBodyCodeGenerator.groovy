package owl2oo.generator.method.removevalue

import org.springframework.stereotype.Component
import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.resultcodebase.exception.TransitiveAttributeException

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return TRANSITIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    List<String> codeLines = generateListAttributeCodeLines(method)
    return Optional.of(codeLines.join('\n'))
  }

  private static List<String> generateListAttributeCodeLines(RemoveValueMethod method) {
    String parameterName = method.parameter.name
    List<String> codeLines = [
        "if (!this.${method.attribute.name}.contains($parameterName)) {",
        "throw new ${TransitiveAttributeException.simpleName}(\"Cannot remove \$$parameterName because it would break transitivity rule\")",
        '}'
    ]
    return codeLines
  }

  @Override
  Optional<String> generateSpecificRemoveValueCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 4
  }
}
