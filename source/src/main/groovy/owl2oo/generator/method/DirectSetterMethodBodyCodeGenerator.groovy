package owl2oo.generator.method

import owl2oo.generator.model.method.DirectSetter
import org.springframework.stereotype.Component

@Component
class DirectSetterMethodBodyCodeGenerator implements MethodBodyCodeGenerator<DirectSetter> {

  @Override
  String generateCode(DirectSetter setter) {
    return "this.${setter.attribute.name} = ${setter.parameter.name}"
  }

  @Override
  Class<DirectSetter> getSupportedClass() {
    return DirectSetter
  }
}
