package owl2oo.generator.method

import owl2oo.generator.model.method.GenericMethod
import org.springframework.stereotype.Component

@Component
class GenericMethodBodyCodeGenerator implements MethodBodyCodeGenerator<GenericMethod> {

  @Override
  String generateCode(GenericMethod method) {
    return method.bodyCode
  }

  @Override
  Class<GenericMethod> getSupportedClass() {
    return GenericMethod
  }
}
