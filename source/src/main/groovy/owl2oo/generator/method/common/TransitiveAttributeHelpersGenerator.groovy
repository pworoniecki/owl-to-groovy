package owl2oo.generator.method.common

import org.springframework.stereotype.Component
import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.GenericMethod
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.VoidType

import static owl2oo.generator.model.AccessModifier.PUBLIC
import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeHelpersGenerator {

  private static final String GETTER_HELPER_PARAMETER_NAME = 'result'

  GenericMethod generateGetterHelperMethod(Attribute attribute) {
    assertSupportedAttribute(attribute)
    def methodCode = generateGetterHelperMethodCode(attribute)
    return new GenericMethod.Builder(getGetterHelperMethodName(attribute), methodCode)
        .withParameters([new Parameter(attribute.valueType, GETTER_HELPER_PARAMETER_NAME)])
        .withReturnType(VoidType.INSTANCE)
        .withAccessModifier(PUBLIC)
        .buildMethod()
  }

  private static void assertSupportedAttribute(Attribute attribute) {
    if (!attribute.characteristics.contains(TRANSITIVE)) {
      throw new IllegalArgumentException('Attribute must be transitive')
    }
    if (!(attribute.valueType instanceof ListType)) {
      throw new IllegalArgumentException('Attribute must be type of list')
    }
  }

  static String getGetterHelperMethodName(Attribute attribute) {
    return "_fillTransitive${attribute.name.capitalize()}"
  }

  private static String generateGetterHelperMethodCode(Attribute attribute) {
    return [
        "(this.${attribute.name} - $GETTER_HELPER_PARAMETER_NAME).each {",
        "if (!${GETTER_HELPER_PARAMETER_NAME}.contains(it)) {",
        "${GETTER_HELPER_PARAMETER_NAME}.add(it)",
        "it.${getGetterHelperMethodName(attribute)}($GETTER_HELPER_PARAMETER_NAME)",
        '}',
        '}'
    ].join('\n')
  }
}
