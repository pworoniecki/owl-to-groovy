package owl2oo.generator.method.setter

import owl2oo.generator.model.method.Setter
import owl2oo.generator.model.type.ListType
import owl2oo.resultcodebase.exception.IrreflexiveAttributeException
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE

@Component
class IrreflexiveAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return IRREFLEXIVE in setter.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    String setterParameterName = setter.parameter.name
    List<String> codeLines = setter.attribute.valueType instanceof ListType ?
        generateListAttributeValidationCodeLines(setterParameterName) :
        generateNonListAttributeValidationCodeLines(setterParameterName
    )
    return Optional.of(generateCode(codeLines))
  }

  private static List<String> generateListAttributeValidationCodeLines(String setterParameterName) {
    return [
        "if (${setterParameterName}.contains(this)) {",
        "throw new ${IrreflexiveAttributeException.simpleName}(\"Cannot set \$${setterParameterName} " +
            "as it contains 'this' instance - it would break irreflexive attribute rule\")",
        '}',
    ]
  }

  private static List<String> generateNonListAttributeValidationCodeLines(String setterParameterName) {
    return [
        "if (${setterParameterName} == this) {",
        "throw new ${IrreflexiveAttributeException.simpleName}(\"Cannot set 'this' instance - " +
            "it would break irreflexive attribute rule\")",
        '}',
    ]
  }

  @Override
  Optional<String> generateSpecificSettingValueCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 2
  }
}
