package owl2oo.generator.method.setter

import org.springframework.stereotype.Component
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Setter
import owl2oo.generator.model.type.ListType
import owl2oo.resultcodebase.exception.InverseFunctionalAttributeException

import static owl2oo.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2oo.generator.model.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class InverseFunctionalAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return INVERSE_FUNCTIONAL in setter.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    String attributeName = setter.attribute.name
    String setterParameterName = setter.parameter.name
    List<String> codeLines = setter.attribute.valueType instanceof ListType ?
        generateListAttributeValidationCodeLines(setter, setterParameterName) :
        generateNonListAttributeValidationCodeLines(attributeName, setterParameterName
    )
    return Optional.of(generateCode(codeLines))
  }

  private static List<String> generateListAttributeValidationCodeLines(Setter setter, String setterParameterName) {
    Attribute attribute = setter.attribute
    String attributeName = attribute.name
    List<String> codeLines = [
        "def ${attributeName}OfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.${attributeName}.flatten()",
        "def commonValues = ${setterParameterName}.intersect(${attributeName}OfOtherInstances)",
        "if (!commonValues.isEmpty()) {",
        "throw new ${InverseFunctionalAttributeException.simpleName}(\"Cannot set \$commonValues because it would break " +
            "inverse functional attribute rule - these values are already set in other class instances.\")",
        '}',
    ]
    if (TRANSITIVE in attribute.characteristics) {
      codeLines.addAll([
          "if (${setter.parameter.name}.any { !it.${attributeName}.isEmpty() }) {",
          "throw new ${InverseFunctionalAttributeException.simpleName}(\"Cannot set \$commonValues because it would break " +
              "inverse functional attribute rule - some values would belong to more than one class instance due to transitivity.\")",
          '}',
      ])
    }
    return codeLines
  }

  private static List<String> generateNonListAttributeValidationCodeLines(String attributeName, String setterParameterName) {
    return [
        "def ${attributeName}OfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.${attributeName}.flatten()",
        "if ($setterParameterName in ${attributeName}OfOtherInstances) {",
        "throw new ${InverseFunctionalAttributeException.simpleName}(\"Cannot set \$$attributeName because it would break " +
            "inverse functional attribute rule - this value is already set in other class instance.\")",
        '}',
    ]
  }

  @Override
  Optional<String> generateSpecificSettingValueCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 6
  }
}