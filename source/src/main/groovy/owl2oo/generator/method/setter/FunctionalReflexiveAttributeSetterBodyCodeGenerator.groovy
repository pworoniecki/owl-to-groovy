package owl2oo.generator.method.setter

import org.springframework.stereotype.Component
import owl2oo.generator.model.method.Setter
import owl2oo.generator.model.type.ListType
import owl2oo.resultcodebase.exception.ReflexiveAttributeException

import static owl2oo.generator.model.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class FunctionalReflexiveAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return setter.attribute.characteristics.containsAll([FUNCTIONAL, REFLEXIVE])
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificSettingValueCode(Setter setter) {
    String setterParameterName = setter.parameter.name
    if (setter.parameter.type instanceof ListType) {
      throw new IllegalStateException("Setter of functional attribute cannot has list type")
    }
    return Optional.of("throw new ${ReflexiveAttributeException.simpleName}(\"Cannot set \$${setterParameterName} " +
        "as it is not 'this' instance - it would break reflexive attribute rule\")",)
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 10
  }
}
