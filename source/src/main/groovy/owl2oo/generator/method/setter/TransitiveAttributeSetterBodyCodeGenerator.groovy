package owl2oo.generator.method.setter

import org.springframework.stereotype.Component
import owl2oo.generator.model.method.Setter

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return TRANSITIVE in setter.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificSettingValueCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 4
  }
}
