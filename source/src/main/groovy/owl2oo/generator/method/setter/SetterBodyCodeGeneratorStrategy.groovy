package owl2oo.generator.method.setter

import owl2oo.generator.model.method.Setter

trait SetterBodyCodeGeneratorStrategy {

  abstract boolean supports(Setter setter)

  abstract Optional<String> generateValidationCode(Setter setter)

  abstract Optional<String> generateSpecificSettingValueCode(Setter setter)

  String generateCode(List<String> codeLines) {
    return codeLines.join('\n')
  }

  // lower value means higher priority
  int getPriority() {
    return Integer.MAX_VALUE
  }

  // lower value means that generator will be used earlier
  int getOrderWithinPriorityGroup() {
    return Integer.MAX_VALUE
  }
}
