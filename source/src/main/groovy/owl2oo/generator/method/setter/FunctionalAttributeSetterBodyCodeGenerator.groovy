package owl2oo.generator.method.setter

import org.springframework.stereotype.Component
import owl2oo.generator.model.attribute.AttributeCharacteristic
import owl2oo.generator.model.method.Setter

import static owl2oo.generator.model.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class FunctionalAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    List<AttributeCharacteristic> characteristics = setter.attribute.characteristics
    return FUNCTIONAL in characteristics && !(REFLEXIVE in characteristics)
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificSettingValueCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 5
  }
}
