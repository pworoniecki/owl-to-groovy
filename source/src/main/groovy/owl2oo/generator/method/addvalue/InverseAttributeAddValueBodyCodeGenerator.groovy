package owl2oo.generator.method.addvalue

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.AddValueMethod
import org.springframework.stereotype.Component
import owl2oo.generator.model.method.DirectSetter
import owl2oo.generator.model.type.ListType

@Component
class InverseAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return method.attribute.isInverseAttribute()
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    List<Attribute> inverseAttributes = method.attribute.findInverseAttributes()
    return Optional.of(generateCode(addSelfToAttributes(inverseAttributes, method.parameter)))
  }

  private static List<String> addSelfToAttributes(List<Attribute> attributes, Parameter methodParameter) {
    return attributes.collect { addSelfToAttribute(it, methodParameter) }
  }

  private static String addSelfToAttribute(Attribute attribute, Parameter methodParameter) {
    if (attribute.valueType instanceof ListType) {
      return "${methodParameter.name}.add${attribute.name.capitalize()}(this)"
    }
    return "${methodParameter.name}.${DirectSetter.getName(attribute)}(this)"
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 8
  }
}
