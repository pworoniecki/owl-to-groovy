package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class ReflexiveAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return REFLEXIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 1
  }
}
