package owl2oo.generator.method.addvalue

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.generator.method.MethodBodyCodeGenerator
import owl2oo.generator.model.method.AddValueMethod

@Component
class AddValueMethodBodyCodeGenerator implements MethodBodyCodeGenerator<AddValueMethod> {

  private List<AddValueBodyCodeGeneratorStrategy> generators

  @Autowired
  AddValueMethodBodyCodeGenerator(List<AddValueBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  @Override
  String generateCode(AddValueMethod method) {
    List<AddValueBodyCodeGeneratorStrategy> orderedGenerators =
        findGenerators(method).sort { it.getOrderWithinPriorityGroup() }
    List<String> validationCode = [generateGeneralValidationCode(method)] +
        generateSpecificValidationCode(orderedGenerators, method)
    List<String> addValueCode = [generateGeneralAddValueCode(method)] +
        generateSpecificAddValueCode(orderedGenerators, method)
    return (validationCode + addValueCode).join('\n')
  }

  private List<AddValueBodyCodeGeneratorStrategy> findGenerators(AddValueMethod method) {
    def possibleGenerators = generators.findAll { it.supports(method) }
    def minGeneratorPriority = possibleGenerators*.priority.min()
    return possibleGenerators.findAll { it.priority == minGeneratorPriority }
  }

  private static String generateGeneralValidationCode(AddValueMethod method) {
    return "if (${method.parameter.name} == null || this.${method.attribute.name}.contains(${method.parameter.name})) { return }"
  }

  private static List<String> generateSpecificValidationCode(List<AddValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                             AddValueMethod method) {
    return orderedGenerators.collect { it.generateValidationCode(method) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static List<String> generateSpecificAddValueCode(List<AddValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                           AddValueMethod method) {
    return orderedGenerators.collect { it.generateSpecificAddValueCode(method) }.findAll { it.isPresent() }.collect { it.get() }
  }

  private static String generateGeneralAddValueCode(AddValueMethod method) {
    return "this.${method.attribute.name}.add(${method.parameter.name})"
  }

  @Override
  Class<AddValueMethod> getSupportedClass() {
    return AddValueMethod
  }
}
