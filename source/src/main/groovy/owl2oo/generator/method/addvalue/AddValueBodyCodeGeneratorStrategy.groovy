package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod

trait AddValueBodyCodeGeneratorStrategy {

  abstract boolean supports(AddValueMethod method)

  abstract Optional<String> generateValidationCode(AddValueMethod method)

  abstract Optional<String> generateSpecificAddValueCode(AddValueMethod method)

  String generateCode(List<String> codeLines) {
    return codeLines.join('\n')
  }

  // lower value means higher priority
  int getPriority() {
    return Integer.MAX_VALUE
  }

  // lower value means that generator will be used earlier
  int getOrderWithinPriorityGroup() {
    return Integer.MAX_VALUE
  }
}
