package owl2oo.generator.method.addvalue

import org.springframework.stereotype.Component
import owl2oo.generator.model.method.AddValueMethod

import static owl2oo.generator.model.attribute.AttributeCharacteristic.SYMMETRIC

@Component
class SymmetricAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return SYMMETRIC in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.of("${method.parameter.name}.add${method.attribute.name.capitalize()}(this)")
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 7
  }
}
