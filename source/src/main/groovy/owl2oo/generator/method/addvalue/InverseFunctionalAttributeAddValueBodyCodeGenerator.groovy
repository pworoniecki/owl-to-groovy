package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod
import owl2oo.resultcodebase.exception.InverseFunctionalAttributeException
import org.springframework.stereotype.Component

import static owl2oo.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2oo.generator.model.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class InverseFunctionalAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return INVERSE_FUNCTIONAL in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def attributeName = method.attribute.name
    def methodParameterName = method.parameter.name
    def codeLines = [
        "def ${attributeName}OfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.${attributeName}.flatten()",
        "if (${methodParameterName} in ${attributeName}OfOtherInstances) {",
        "throw new ${InverseFunctionalAttributeException.simpleName}(\"Cannot add value '\$${methodParameterName}' - " +
            "it would break inverse functional attribute rule\")",
        '}',
    ]
    if (TRANSITIVE in method.attribute.characteristics) {
      codeLines.addAll([
          "if (!${methodParameterName}.${attributeName}.isEmpty()) {",
          "throw new ${InverseFunctionalAttributeException.simpleName}(\"Cannot add value \$${methodParameterName} because it would break " +
              "inverse functional attribute rule - some values would belong to more than one class instance due to transitivity.\")",
          '}',
      ])
    }
    return Optional.of(generateCode(codeLines))
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 6
  }
}