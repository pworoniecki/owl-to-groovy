package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod
import org.springframework.stereotype.Component
import owl2oo.resultcodebase.exception.TransitiveAttributeException

import static owl2oo.generator.model.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return TRANSITIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 4
  }
}
