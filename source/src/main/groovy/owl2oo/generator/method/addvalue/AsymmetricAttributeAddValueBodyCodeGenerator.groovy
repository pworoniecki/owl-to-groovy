package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod
import owl2oo.resultcodebase.exception.AsymmetricAttributeException
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.ASYMMETRIC

@Component
class AsymmetricAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return ASYMMETRIC in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def parameterName = method.parameter.name
    def validationCode = generateCode([
        "if (${parameterName}.${parameterName}.contains(this)) {",
        "throw new $AsymmetricAttributeException.simpleName(\"Cannot add value '\$$parameterName' - " +
            "it would break asymmetric attribute rule\")",
        '}'
    ])
    return Optional.of(validationCode)
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 3
  }
}
