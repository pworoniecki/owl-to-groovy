package owl2oo.generator.method.addvalue

import owl2oo.generator.model.method.AddValueMethod
import owl2oo.resultcodebase.exception.IrreflexiveAttributeException
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE

@Component
class IrreflexiveAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(AddValueMethod method) {
    return IRREFLEXIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def methodParameterName = method.parameter.name
    def validationCode = generateCode([
        "if ($methodParameterName == this) {",
        "throw new ${IrreflexiveAttributeException.simpleName}(\"Cannot add value 'this' - " +
            "it would break irreflexivity of attribute\")",
        '}',
    ])
    return Optional.of(validationCode)
  }

  @Override
  Optional<String> generateSpecificAddValueCode(AddValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 2
  }
}
