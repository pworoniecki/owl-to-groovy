package owl2oo.generator.method.getter

import org.springframework.stereotype.Component
import owl2oo.generator.method.common.TransitiveAttributeHelpersGenerator
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Getter
import owl2oo.generator.model.type.ListType

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeGetterBodyCodeGenerator implements GetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Getter getter) {
    return TRANSITIVE in getter.attribute.characteristics
  }

  @Override
  String generateCode(Getter getter) {
    if (getter.attribute.valueType instanceof ListType) {
      return generateCodeForListAttribute(getter)
    }
    return generateCodeForSingleAttribute(getter)
  }

  private static String generateCodeForListAttribute(Getter getter) {
    Attribute attribute = getter.attribute
    return [
        'def result = []',
        "${TransitiveAttributeHelpersGenerator.getGetterHelperMethodName(attribute)}(result)",
        'return result'
    ].join('\n')
  }

  private static String generateCodeForSingleAttribute(Getter getter) {
    return "return this.${getter.attribute.name}"
  }

  @Override
  int getPriority() {
    return 1
  }
}
