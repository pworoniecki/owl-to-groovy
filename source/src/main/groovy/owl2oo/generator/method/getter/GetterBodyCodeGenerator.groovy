package owl2oo.generator.method.getter

import owl2oo.generator.method.MethodBodyCodeGenerator
import owl2oo.generator.model.method.Getter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class GetterBodyCodeGenerator implements MethodBodyCodeGenerator<Getter> {

  private List<GetterBodyCodeGeneratorStrategy> generators

  @Autowired
  GetterBodyCodeGenerator(List<GetterBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  @Override
  String generateCode(Getter method) {
    GetterBodyCodeGeneratorStrategy generator = generators.findAll { it.supports(method) }.max { it.priority }
    if (!generator) {
      throw new IllegalStateException("Missing body code generator that supports getter $method.name")
    }
    return generator.generateCode(method)
  }

  @Override
  Class<Getter> getSupportedClass() {
    return Getter
  }
}
