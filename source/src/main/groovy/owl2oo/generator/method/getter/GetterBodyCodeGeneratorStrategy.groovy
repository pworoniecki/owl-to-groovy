package owl2oo.generator.method.getter

import owl2oo.generator.model.method.Getter

interface GetterBodyCodeGeneratorStrategy {

  boolean supports(Getter getter)

  String generateCode(Getter getter)

  // higher value means higher priority
  int getPriority()
}
