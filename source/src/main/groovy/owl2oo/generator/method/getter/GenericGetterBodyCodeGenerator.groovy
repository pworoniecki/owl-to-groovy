package owl2oo.generator.method.getter

import owl2oo.generator.model.method.Getter
import org.springframework.stereotype.Component

@Component
final class GenericGetterBodyCodeGenerator implements GetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Getter getter) {
    return true
  }

  @Override
  String generateCode(Getter getter) {
    return "return this.${getter.attribute.name}"
  }

  @Override
  int getPriority() {
    return Integer.MIN_VALUE
  }
}
