package owl2oo.generator.method

import owl2oo.generator.model.method.Method

trait MethodBodyCodeGenerator<T extends Method> {

  boolean supports(Method method) {
    return method?.class == supportedClass
  }

  abstract String generateCode(T method)

  abstract Class<T> getSupportedClass()
}
