package owl2oo.generator.method

import owl2oo.generator.exception.MethodBodyCodeGeneratorNotFoundException
import owl2oo.generator.model.method.Method
import owl2oo.utils.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MethodCodeGenerator {

  private List<MethodBodyCodeGenerator> bodyCodeGenerators

  @Autowired
  MethodCodeGenerator(List<MethodBodyCodeGenerator> bodyCodeGenerators) {
    this.bodyCodeGenerators = bodyCodeGenerators
  }

  String generateCode(Method method) {
    def codeBuilder = new StringBuilder()
    codeBuilder.with {
      append(methodSignature(method) + '\n')
      append(methodBody(method) + '\n')
      append('}')
    }
    return codeBuilder.toString()
  }

  private static String methodSignature(Method method) {
    def accessModifier = StringUtils.emptyOrAddSpace(method.accessModifier.toGroovyCode())
    def staticModifier = method.isStatic() ? 'static ' : ''
    def returnType = "${method.returnType.simpleName} "
    def parameters = method.parameters.collect { it.simpleDeclaration }.join(', ')

    return "$accessModifier$staticModifier$returnType$method.name($parameters) {"
  }

  private String methodBody(Method method) {
    def bodyCodeGenerator = bodyCodeGenerators.find { it.supports(method) }
    if (!bodyCodeGenerator) {
      throw new MethodBodyCodeGeneratorNotFoundException(method)
    }
    return bodyCodeGenerator.generateCode(method)
  }
}
