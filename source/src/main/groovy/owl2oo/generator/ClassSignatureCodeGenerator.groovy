package owl2oo.generator

import owl2oo.generator.model.Class
import org.springframework.stereotype.Component

@Component
class ClassSignatureCodeGenerator {

  String generateClassSignature(Class classModel) {
    def inheritedClassSignaturePart = classModel.inheritedClass.isPresent() ?
        " extends ${classModel.inheritedClass.get().name}" : ''
    def implementedTraitsSignaturePart = classModel.implementedTraits.any() ?
        " implements ${classModel.implementedTraits*.name.join(', ')}" : ''
    return "class ${classModel.name}" + inheritedClassSignaturePart + implementedTraitsSignaturePart + ' {'
  }
}
