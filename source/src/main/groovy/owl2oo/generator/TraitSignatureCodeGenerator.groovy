package owl2oo.generator

import owl2oo.generator.model.Trait
import org.springframework.stereotype.Component

@Component
class TraitSignatureCodeGenerator {

  String generateTraitSignature(Trait traitModel) {
    def implementedTraitsSignaturePart = traitModel.inheritedTraits.any() ?
        " implements ${traitModel.inheritedTraits*.name.join(', ')}" : ''
    return "trait ${traitModel.name}" + implementedTraitsSignaturePart + ' {'
  }
}
