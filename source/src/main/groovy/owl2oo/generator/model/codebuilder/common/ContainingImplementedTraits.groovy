package owl2oo.generator.model.codebuilder.common

import owl2oo.generator.model.Trait
import owl2oo.generator.model.type.SimpleType

trait ContainingImplementedTraits implements ContainingImports {

  void appendImplementedTraitImports(List<Trait> implementedTraits, String ownerPackageName) {
    implementedTraits.findAll { it.packageName != ownerPackageName && !new SimpleType(it).isImportedByDefault() }
        .each { appendClassImport(it.fullName) }
  }
}
