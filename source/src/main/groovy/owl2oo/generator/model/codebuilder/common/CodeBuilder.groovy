package owl2oo.generator.model.codebuilder.common

import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType

abstract class CodeBuilder implements ContainingMethods, ContainingAttributes, ContainingImplementedTraits,
    ContainingAnnotations {

  private StringBuilder codeBuilder = new StringBuilder()
  private List<String> imports = []

  void appendCode(String code) {
    codeBuilder.append("$code\n")
  }

  void appendEmptyLine() {
    appendCode('')
  }

  String getCode() {
    return codeBuilder.toString()
  }

  void appendImports(GeneratorConfiguration generatorConfiguration) {
    appendDefaultImports(generatorConfiguration)
    appendSpecificImports()
  }

  void appendDefaultImports(GeneratorConfiguration generatorConfiguration) {
    appendPackageImport(generatorConfiguration.generatedClasses.exceptionFullPackage)
  }

  @Override
  void appendPackageImport(String packageName) {
    def importCandidate = "import $packageName.*"
    if (!imports.contains(importCandidate)) {
      appendCode(importCandidate)
      imports.add(importCandidate)
    }
  }

  @Override
  void appendClassImport(String classFullName) {
    def importCandidate = "import $classFullName"
    if (!imports.contains(importCandidate)) {
      appendCode(importCandidate)
      imports.add(importCandidate)
    }
  }

  abstract void appendSpecificImports()

  @Override
  void appendImports(List<Type> types, String ownerPackage) {
    List<Type> nonVoidTypes = types.findAll { !(it instanceof VoidType) }
    List<Type> flattenTypes = nonVoidTypes.collect { extractSimpleType(it) }
    List<Type> typesToBeImported = flattenTypes.unique().findAll { !it.isImportedByDefault() && it.packageName != ownerPackage }
    typesToBeImported*.fullName.unique().each { appendClassImport(it) }
  }

  static Type extractSimpleType(Type type) {
    return type instanceof ListType ? type.extractSimpleParameterizedType() : type
  }
}
