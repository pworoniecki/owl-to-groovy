package owl2oo.generator.model.codebuilder.common

import owl2oo.generator.model.Annotation
import owl2oo.generator.model.type.Type

trait ContainingAnnotations implements ContainingCode, ContainingImports {

  void appendAnnotationTypeImports(List<Annotation> annotations) {
    List<Type> uniqueParameterTypes = annotations*.type.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  void appendAnnotations(List<Annotation> annotations) {
    annotations.each { appendAnnotation(it) }
  }

  void appendAnnotation(Annotation annotation) {
    def parametersCodePart = annotation.parameters ? "([${annotation.parameters*.toString().join(', ')}])" : ''
    appendCode("@${annotation.type.simpleName}$parametersCodePart")
  }
}
