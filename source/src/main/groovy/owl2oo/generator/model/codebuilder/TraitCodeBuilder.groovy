package owl2oo.generator.model.codebuilder

import owl2oo.generator.model.Trait
import owl2oo.generator.model.codebuilder.common.CodeBuilder

class TraitCodeBuilder extends CodeBuilder {

  private Trait traitModel

  TraitCodeBuilder(Trait traitModel) {
    this.traitModel = traitModel
  }

  void appendPackage() {
    appendCode("package ${traitModel.packageName}")
  }

  void appendTraitSignatureEnd() {
    appendCode('}')
  }

  @Override
  void appendSpecificImports() {
    appendImplementedTraitImports(traitModel.inheritedTraits, traitModel.packageName)
    appendMethodParameterImports(traitModel.methods)
    appendMethodReturnTypeImports(traitModel.methods)
    appendAttributeTypeImports(traitModel.attributes)
    appendAnnotationTypeImports(traitModel.annotations)
  }

  @Override
  String getOwnerPackageName() {
    return traitModel.packageName
  }
}
