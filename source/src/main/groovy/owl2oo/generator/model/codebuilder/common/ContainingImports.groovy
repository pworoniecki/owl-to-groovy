package owl2oo.generator.model.codebuilder.common

import owl2oo.generator.model.type.Type

interface ContainingImports {

  abstract void appendImports(List<Type> types, String ownerPackage)

  abstract void appendClassImport(String classFullName)

  abstract void appendPackageImport(String packageName)

  abstract String getOwnerPackageName()
}
