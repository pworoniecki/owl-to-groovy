package owl2oo.generator.model.codebuilder

import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.codebuilder.common.CodeBuilder
import owl2oo.generator.model.type.Type

class ClassCodeBuilder extends CodeBuilder {

  private Class classModel

  ClassCodeBuilder(Class classModel) {
    this.classModel = classModel
  }

  void appendPackage() {
    appendCode("package ${classModel.packageName}")
  }

  void appendClassSignatureEnd() {
    appendCode('}')
  }

  @Override
  void appendSpecificImports() {
    appendInheritedClassImportIfNecessary()
    appendImplementedTraitImports(classModel.implementedTraits, classModel.packageName)
    appendConstructorParameterImports(classModel.constructors)
    appendMethodParameterImports(classModel.methods)
    appendMethodReturnTypeImports(classModel.methods)
    appendAttributeTypeImports(classModel.attributes)
    appendAnnotationTypeImports(classModel.annotations)
  }

  private void appendInheritedClassImportIfNecessary() {
    classModel.inheritedClass.ifPresent({ inheritedClass ->
      if (inheritedClass.packageName != classModel.packageName) {
        appendClassImport(inheritedClass.fullName)
      }
    })
  }

  private void appendConstructorParameterImports(List<Constructor> constructors) {
    List<Type> uniqueParameterTypes = constructors*.parameters*.type.flatten().unique() as List<Type>
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  void appendStaticConstructorsWithEmptyLines(Map<Constructor, String> constructorsToCode) {
    constructorsToCode.each { constructor, code ->
      if (constructor.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  void appendNonStaticConstructorsWithEmptyLines(Map<Constructor, String> constructorsToCode) {
    constructorsToCode.each { constructor, code ->
      if (!constructor.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  @Override
  String getOwnerPackageName() {
    return classModel.packageName
  }
}
