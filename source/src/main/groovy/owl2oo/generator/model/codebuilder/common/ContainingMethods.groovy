package owl2oo.generator.model.codebuilder.common

import owl2oo.generator.model.method.Method
import owl2oo.generator.model.type.Type

trait ContainingMethods implements ContainingCode, ContainingImports {

  void appendMethodParameterImports(List<Method> methods) {
    List<Type> uniqueParameterTypes = methods*.parameters*.type.flatten().unique() as List<Type>
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  void appendMethodReturnTypeImports(List<Method> methods) {
    List<Type> uniqueParameterTypes = methods*.returnType.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  void appendStaticMethodsWithEmptyLines(Map<Method, String> methodsToCode) {
    methodsToCode.each { method, code ->
      if (method.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  void appendNonStaticMethodsWithEmptyLines(Map<Method, String> methodsToCode) {
    methodsToCode.each { method, code ->
      if (!method.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }
}
