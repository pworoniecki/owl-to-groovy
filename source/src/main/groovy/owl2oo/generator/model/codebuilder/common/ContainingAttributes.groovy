package owl2oo.generator.model.codebuilder.common

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.Type

trait ContainingAttributes implements ContainingCode, ContainingImports {

  void appendAttributeTypeImports(List<Attribute> attributes) {
    List<Type> uniqueParameterTypes = attributes*.valueType.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  void appendStaticAttributes(Map<Attribute, String> attributesToCode) {
    boolean isAnyStaticAttributePresent = false
    attributesToCode.each { attribute, code ->
      if (attribute.isStatic()) {
        appendCode(code)
        isAnyStaticAttributePresent = true
      }
    }
    if (isAnyStaticAttributePresent) {
      appendEmptyLine()
    }
  }

  void appendNonStaticAttributes(Map<Attribute, String> attributesToCode) {
    boolean isAnyNonStaticAttributePresent = false
    attributesToCode.each { attribute, code ->
      if (!attribute.isStatic()) {
        appendCode(code)
        isAnyNonStaticAttributePresent = true
      }
    }
    if (isAnyNonStaticAttributePresent) {
      appendEmptyLine()
    }
  }
}
