package owl2oo.generator.model.codebuilder.common

interface ContainingCode {

  abstract void appendCode(String code)

  abstract void appendEmptyLine()
}
