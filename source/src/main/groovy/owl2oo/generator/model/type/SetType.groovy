package owl2oo.generator.model.type

import groovy.transform.InheritConstructors

import static com.google.common.base.Preconditions.checkNotNull

@InheritConstructors
class SetType extends Type {

  private static final Class TYPE_REFERENCE_CLASS = Set

  private Type parameterizedType

  SetType(Type parameterizedType) {
    this.parameterizedType = checkNotNull(parameterizedType)
  }

  Type getParameterizedType() {
    return parameterizedType
  }

  @Override
  String getFullName() {
    return "${TYPE_REFERENCE_CLASS.name}<${parameterizedType.fullName}>"
  }

  @Override
  String getSimpleName() {
    return "${TYPE_REFERENCE_CLASS.simpleName}<${parameterizedType.simpleName}>"
  }

  @Override
  String getPackageName() {
    return TYPE_REFERENCE_CLASS.package.name
  }

  Type extractSimpleParameterizedType() {
    Type lastParametrizedType = parameterizedType
    while (lastParametrizedType instanceof SetType) {
      lastParametrizedType = (lastParametrizedType as SetType).parameterizedType
    }
    return lastParametrizedType
  }
}
