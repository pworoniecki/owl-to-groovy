package owl2oo.generator.model.type

import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait

import static com.google.common.base.Preconditions.checkNotNull

class SimpleType extends Type {

  protected String packageName
  protected String className

  SimpleType(Class basicType) {
    this(basicType?.packageName, basicType?.name)
  }

  SimpleType(Trait basicType) {
    this(basicType?.packageName, basicType?.name)
  }

  SimpleType(java.lang.Class<?> basicType) {
    this(basicType?.package?.name, basicType?.simpleName)
  }

  SimpleType(String packageName, String className) {
    this.packageName = checkNotNull(packageName)
    this.className = checkNotNull(className)
  }

  @Override
  String getFullName() {
    return "$packageName.$className"
  }

  @Override
  String getPackageName() {
    return packageName
  }

  @Override
  String getSimpleName() {
    return className
  }
}
