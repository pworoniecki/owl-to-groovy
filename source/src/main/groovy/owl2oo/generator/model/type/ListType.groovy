package owl2oo.generator.model.type

import groovy.transform.InheritConstructors

import static com.google.common.base.Preconditions.checkNotNull

@InheritConstructors
class ListType extends Type {

  private static final Class TYPE_REFERENCE_CLASS = List

  private Type parameterizedType

  ListType(Type parameterizedType) {
    this.parameterizedType = checkNotNull(parameterizedType)
  }

  Type getParameterizedType() {
    return parameterizedType
  }

  @Override
  String getFullName() {
    return "${TYPE_REFERENCE_CLASS.name}<${parameterizedType.fullName}>"
  }

  @Override
  String getSimpleName() {
    return "${TYPE_REFERENCE_CLASS.simpleName}<${parameterizedType.simpleName}>"
  }

  @Override
  String getPackageName() {
    return TYPE_REFERENCE_CLASS.package.name
  }

  Type extractSimpleParameterizedType() {
    Type lastParametrizedType = parameterizedType
    while (lastParametrizedType instanceof ListType) {
      lastParametrizedType = (lastParametrizedType as ListType).parameterizedType
    }
    return lastParametrizedType
  }
}
