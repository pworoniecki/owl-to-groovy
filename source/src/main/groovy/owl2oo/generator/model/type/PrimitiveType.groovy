package owl2oo.generator.model.type

class PrimitiveType extends Type {

  static final PrimitiveType BOOLEAN = new PrimitiveType('boolean')

  private String name

  PrimitiveType(String name) {
    this.name = name
  }

  @Override
  String getFullName() {
    return name
  }

  @Override
  String getSimpleName() {
    return name
  }

  @Override
  String getPackageName() {
    return null
  }
}
