package owl2oo.generator.model.type

class VoidType extends Type {

  static final VoidType INSTANCE = new VoidType()

  private static final String TYPE_NAME = 'void'

  private VoidType() {}

  @Override
  String getFullName() {
    return TYPE_NAME
  }

  @Override
  String getSimpleName() {
    return TYPE_NAME
  }

  @Override
  String getPackageName() {
    return null
  }
}
