package owl2oo.generator.model.type

abstract class Type {

  private static final List<String> DEFAULT_IMPORTED_PACKAGES = [
      'java.lang', 'java.util', 'java.io', 'java.net', 'groovy.lang', 'groovy.util'
  ]
  private static final List<String> DEFAULT_IMPORTED_CLASSES = ['java.math.BigInteger', 'java.math.BigDecimal']
  private static final List<String> PRIMITIVE_TYPES = ['byte', 'short', 'int', 'long', 'float', 'double', 'boolean', 'char']

  abstract String getFullName()

  abstract String getSimpleName()

  abstract String getPackageName()

  @Override
  boolean equals(Object obj) {
    return obj != null && (obj as Type).fullName == fullName
  }

  @Override
  int hashCode() {
    return fullName.hashCode()
  }

  boolean isImportedByDefault() {
    return DEFAULT_IMPORTED_PACKAGES.any { packageName == it } || DEFAULT_IMPORTED_CLASSES.any { fullName == it } ||
        PRIMITIVE_TYPES.any { fullName == it }
  }
}
