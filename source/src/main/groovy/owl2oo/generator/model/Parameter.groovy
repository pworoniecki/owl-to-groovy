package owl2oo.generator.model

import owl2oo.generator.model.type.Type

import static com.google.common.base.Preconditions.checkNotNull

class Parameter<T extends Type> {

  private T type
  private String name

  Parameter(T type, String name) {
    this.type = checkNotNull(type)
    this.name = checkNotNull(name)
  }

  T getType() {
    return type
  }

  String getName() {
    return name
  }

  String getSimpleDeclaration() {
    return "$type.simpleName $name"
  }
}
