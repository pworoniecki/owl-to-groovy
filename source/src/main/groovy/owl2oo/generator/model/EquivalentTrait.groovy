package owl2oo.generator.model

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

class EquivalentTrait extends Trait {

  private EquivalentTrait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                          List<Attribute> attributes, List<Method> methods) {
    super(packageName, name, annotations, inheritedTraits, attributes, methods)
  }

  static class Builder extends TraitBuilder<Builder, EquivalentTrait> {

    Builder(String packageName, String name) {
      super(packageName, name)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    EquivalentTrait buildTrait() {
      return new EquivalentTrait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
