package owl2oo.generator.model

import groovy.transform.PackageScope
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

abstract class ClassBuilder<S extends ClassBuilder, T extends Class> {

  @PackageScope String packageName
  @PackageScope String className
  @PackageScope List<Annotation> annotations = []
  @PackageScope Optional<Class> inheritedClass = Optional.empty()
  @PackageScope List<Constructor> constructors = []
  @PackageScope List<Attribute> attributes = []
  @PackageScope List<Method> methods = []
  @PackageScope List<Trait> implementedTraits = []

  ClassBuilder(String packageName, String className) {
    this.packageName = checkNotNull(packageName)
    this.className = checkNotNull(className)
  }

  S withAnnotations(List<Annotation> annotations) {
    this.annotations = checkNotNull(annotations)
    assertNoNullInCollection(annotations)
    return getThis()
  }

  S withInheritedClass(Class inheritedClass) {
    this.inheritedClass = Optional.of(checkNotNull(inheritedClass))
    return getThis()
  }

  S withConstructors(List<Constructor> constructors) {
    this.constructors = checkNotNull(constructors)
    assertNoNullInCollection(constructors)
    return getThis()
  }

  S withAttributes(List<Attribute> attributes) {
    this.attributes = checkNotNull(attributes)
    assertNoNullInCollection(attributes)
    return getThis()
  }

  S withMethods(List<Method> methods) {
    this.methods = checkNotNull(methods)
    assertNoNullInCollection(methods)
    return getThis()
  }

  S withImplementedTraits(List<Trait> implementedTraits) {
    this.implementedTraits = checkNotNull(implementedTraits)
    assertNoNullInCollection(implementedTraits)
    return getThis()
  }

  abstract S getThis()

  abstract T buildClass()
}
