package owl2oo.generator.model

import groovy.transform.InheritConstructors
import owl2oo.generator.ClassElementVisitor
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

class Class {

  private String packageName
  private String name
  private List<Annotation> annotations
  private Optional<Class> inheritedClass
  private List<Constructor> constructors
  private List<Attribute> attributes
  private List<Method> methods
  private List<Trait> implementedTraits

  protected Class(String packageName, String className, List<Annotation> annotations, Optional<Class> inheritedClass,
                  List<Constructor> constructors, List<Attribute> attributes, List<Method> methods,
                  List<Trait> implementedTraits) {
    this.packageName = packageName
    this.name = className
    this.annotations = annotations
    this.inheritedClass = inheritedClass
    this.constructors = constructors
    this.attributes = attributes
    this.methods = methods
    this.implementedTraits = implementedTraits
  }

  String getPackageName() {
    return packageName
  }

  String getName() {
    return name
  }

  List<Annotation> getAnnotations() {
    return annotations
  }

  Optional<Class> getInheritedClass() {
    return inheritedClass
  }

  List<Constructor> getConstructors() {
    return constructors
  }

  List<Attribute> getAttributes() {
    return attributes
  }

  List<Method> getMethods() {
    return methods
  }

  List<Trait> getImplementedTraits() {
    return implementedTraits
  }

  String getFullName() {
    return "$packageName.$name"
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Class that = (Class) o
    return this.name == that.name && this.packageName == that.packageName
  }

  int hashCode() {
    return name.hashCode()
  }

  void accept(ClassElementVisitor visitor) {
    constructors.each { visitor.visit(it, this) }
    attributes.each { visitor.visit(it) }
    methods.each { visitor.visit(it) }
  }

  @InheritConstructors
  static class Builder extends ClassBuilder<Builder, Class> {

    @Override
    Builder getThis() {
      return this
    }

    @Override
    Class buildClass() {
      return new Class(packageName, className, annotations, inheritedClass, constructors, attributes, methods, implementedTraits)
    }
  }
}
