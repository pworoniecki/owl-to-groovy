package owl2oo.generator.model

import owl2oo.generator.model.type.Type

import static com.google.common.base.Preconditions.checkNotNull

class Annotation {

  private Type type
  private List<String> parameters

  Annotation(Type type, List<String> parameters = []) {
    this.type = checkNotNull(type)
    this.parameters = checkNotNull(parameters)
    parameters.each { checkNotNull(it) }
  }

  Type getType() {
    return type
  }

  List<String> getParameters() {
    return parameters
  }
}
