package owl2oo.generator.model.attribute

enum AttributeCharacteristic {
  SYMMETRIC, ASYMMETRIC, TRANSITIVE, FUNCTIONAL, INVERSE_FUNCTIONAL, REFLEXIVE, IRREFLEXIVE
}
