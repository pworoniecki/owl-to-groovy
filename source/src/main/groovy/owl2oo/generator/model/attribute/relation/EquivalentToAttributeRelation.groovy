package owl2oo.generator.model.attribute.relation

import groovy.transform.InheritConstructors

// this relation must be set only on "owned"/"child" side of attribute
// and must not be set on "owner"/"parent" side to avoid circular dependency
@InheritConstructors
class EquivalentToAttributeRelation extends AttributeRelation {
}
