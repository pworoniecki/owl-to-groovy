package owl2oo.generator.model.attribute

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.relation.AttributeRelation
import owl2oo.generator.model.attribute.relation.EquivalentToAttributeRelation
import owl2oo.generator.model.attribute.relation.InverseAttributeRelation
import owl2oo.generator.model.attribute.relation.SuperAttributeOfRelation
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

class Attribute {

  private String name
  private AccessModifier accessModifier
  private boolean staticAttribute
  private Type valueType
  private String defaultValue
  private List<AttributeCharacteristic> characteristics
  private List<AttributeRelation> relations

  private Attribute(String name, AccessModifier accessModifier, boolean staticAttribute, Type valueType, String defaultValue,
                    List<AttributeCharacteristic> characteristics, List<AttributeRelation> relations) {
    this.name = name
    this.accessModifier = accessModifier
    this.staticAttribute = staticAttribute
    this.valueType = valueType
    this.defaultValue = defaultValue
    this.characteristics = characteristics
    this.relations = relations
  }

  String getName() {
    return name
  }

  AccessModifier getAccessModifier() {
    return accessModifier
  }

  boolean isStatic() {
    return staticAttribute
  }

  Type getValueType() {
    return valueType
  }

  Type extractSimpleValueType() {
    return valueType instanceof ListType ? valueType.extractSimpleParameterizedType() : valueType
  }

  String getDefaultValue() {
    return defaultValue
  }

  List<AttributeCharacteristic> getCharacteristics() {
    return characteristics
  }

  List<AttributeRelation> getRelations() {
    return relations
  }

  boolean isEquivalentOwnedAttribute() {
    return findAnyEquivalentAttribute().isPresent()
  }

  Optional<Attribute> findAnyEquivalentAttribute() {
    return Optional.ofNullable(relations.find { it instanceof EquivalentToAttributeRelation }?.relatedAttribute)
  }

  boolean isSuperAttribute() {
    return !findSubAttributes().isEmpty()
  }

  List<Attribute> findSubAttributes() {
    return relations.findAll { it instanceof SuperAttributeOfRelation }*.relatedAttribute
  }

  boolean isInverseAttribute() {
    return !findInverseAttributes().isEmpty()
  }

  List<Attribute> findInverseAttributes() {
    return relations.findAll { it instanceof InverseAttributeRelation }*.relatedAttribute
  }

  boolean equals(Object o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Attribute that = (Attribute) o
    return that.accessModifier == this.accessModifier && that.name == this.name
  }

  int hashCode() {
    return name.hashCode()
  }

  static class AttributeBuilder {

    private String name
    private AccessModifier accessModifier = AccessModifier.PUBLIC
    private boolean staticAttribute = false
    private Type valueType
    private String defaultValue
    private List<AttributeCharacteristic> characteristics = []
    private List<AttributeRelation> relations = []

    AttributeBuilder(String name, Type valueType) {
      this.name = checkNotNull(name)
      this.valueType = checkNotNull(valueType)
      assertCorrectType(valueType)
    }

    private static void assertCorrectType(Type type) {
      if (type instanceof VoidType) {
        throw new IllegalArgumentException('Cannot set void type as type of an attribute')
      }
    }

    AttributeBuilder withAccessModifier(AccessModifier modifier) {
      this.accessModifier = checkNotNull(modifier)
      return this
    }

    AttributeBuilder asStatic() {
      this.staticAttribute = true
      return this
    }

    AttributeBuilder withDefaultValue(String value) {
      this.defaultValue = checkNotNull(value)
      return this
    }

    AttributeBuilder withCharacteristics(List<AttributeCharacteristic> characteristics) {
      this.characteristics = checkNotNull(characteristics)
      assertNoNullInCollection(characteristics)
      return this
    }

    AttributeBuilder withRelations(List<AttributeRelation> relations) {
      this.relations = checkNotNull(relations)
      assertNoNullInCollection(relations)
      return this
    }

    Attribute buildAttribute() {
      return new Attribute(name, accessModifier, staticAttribute, valueType, defaultValue, characteristics, relations)
    }
  }
}
