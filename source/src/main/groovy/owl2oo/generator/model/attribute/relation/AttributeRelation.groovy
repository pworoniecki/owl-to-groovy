package owl2oo.generator.model.attribute.relation

import owl2oo.generator.model.attribute.Attribute

import static com.google.common.base.Preconditions.checkNotNull

abstract class AttributeRelation {

  private Attribute relatedAttribute

  AttributeRelation(Attribute relatedAttribute) {
    this.relatedAttribute = checkNotNull(relatedAttribute)
  }

  Attribute getRelatedAttribute() {
    return relatedAttribute
  }

  boolean equals(Object o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false
    return relatedAttribute == ((AttributeRelation) o).relatedAttribute
  }

  int hashCode() {
    return relatedAttribute.hashCode()
  }
}
