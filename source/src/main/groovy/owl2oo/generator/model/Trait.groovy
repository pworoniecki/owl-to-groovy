package owl2oo.generator.model

import owl2oo.generator.TraitElementVisitor
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method
import groovy.transform.PackageScope

class Trait {

  private String packageName
  private String name
  private List<Annotation> annotations
  private List<Trait> inheritedTraits
  private List<Attribute> attributes
  private List<Method> methods

  @PackageScope Trait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                      List<Attribute> attributes, List<Method> methods) {
    this.packageName = packageName
    this.name = name
    this.annotations = annotations
    this.inheritedTraits = inheritedTraits
    this.attributes = attributes
    this.methods = methods
  }

  String getPackageName() {
    return packageName
  }

  String getName() {
    return name
  }

  List<Annotation> getAnnotations() {
    return annotations
  }

  List<Trait> getInheritedTraits() {
    return inheritedTraits
  }

  List<Attribute> getAttributes() {
    return attributes
  }

  List<Method> getMethods() {
    return methods
  }

  String getFullName() {
    return "$packageName.$name"
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Trait that = (Trait) o
    return this.name == that.name && this.packageName == that.packageName
  }

  int hashCode() {
    return name.hashCode()
  }

  void accept(TraitElementVisitor visitor) {
    attributes.each { visitor.visit(it) }
    methods.each { visitor.visit(it) }
  }

  static class Builder extends TraitBuilder<Builder, Trait> {

    Builder(String packageName, String name) {
      super(packageName, name)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    Trait buildTrait() {
      return new Trait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
