package owl2oo.generator.model.method

import com.google.common.base.Preconditions
import groovy.transform.InheritConstructors

@InheritConstructors
class GenericMethod extends Method {

  private String bodyCode

  GenericMethod(Builder builder) {
    super(builder)
    this.bodyCode = builder.bodyCode
  }

  String getBodyCode() {
    return bodyCode
  }

  static class Builder extends Method.Builder<GenericMethod, Builder> {

    protected String bodyCode

    Builder(String name, String bodyCode) {
      super(name)
      this.bodyCode = Preconditions.checkNotNull(bodyCode)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    GenericMethod buildMethod() {
      return new GenericMethod(this)
    }
  }
}
