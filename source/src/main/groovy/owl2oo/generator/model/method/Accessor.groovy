package owl2oo.generator.model.method

import com.google.common.base.Preconditions
import owl2oo.generator.model.attribute.Attribute

abstract class Accessor extends Method {

  private Attribute attribute

  protected Accessor(Builder builder) {
    super(builder)
    this.attribute = builder.attribute
  }

  Attribute getAttribute() {
    return attribute
  }

  protected static abstract class Builder<S, T extends Builder<S, T>> extends Method.Builder<S, T> {

    protected Attribute attribute

    Builder(String name, Attribute attribute) {
      super(name)
      this.attribute = Preconditions.checkNotNull(attribute)
    }
  }
}
