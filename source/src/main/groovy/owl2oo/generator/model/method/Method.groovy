package owl2oo.generator.model.method

import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.Parameter
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

abstract class Method {

  private String name
  private AccessModifier accessModifier
  private boolean staticMethod = false
  private Type returnType
  private List<Parameter> parameters

  protected Method(Builder builder) {
    this.name = builder.name
    this.accessModifier = builder.accessModifier
    this.staticMethod = builder.staticMethod
    this.returnType = builder.returnType
    this.parameters = builder.parameters
  }

  String getName() {
    return name
  }

  AccessModifier getAccessModifier() {
    return accessModifier
  }

  boolean isStatic() {
    return staticMethod
  }

  Type getReturnType() {
    return returnType
  }

  List<Parameter> getParameters() {
    return parameters
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Method method = (Method) o
    return method.name == this.name && method.parameters == this.parameters
  }

  int hashCode() {
    return name.hashCode()
  }

  @Override
  public String toString() {
    return "Method{" +
        "name='" + name + '\'' +
        ", accessModifier=" + accessModifier +
        ", staticMethod=" + staticMethod +
        ", returnType=" + returnType.fullName +
        ", parameters=[" + parameters*.simpleDeclaration.join(', ') + ']' +
        '}';
  }

  protected static abstract class Builder<S, T extends Builder<S, T>> {

    protected String name
    protected AccessModifier accessModifier = AccessModifier.PUBLIC
    protected boolean staticMethod = false
    protected Type returnType = VoidType.INSTANCE
    protected List<Parameter> parameters = []

    Builder(String name) {
      this.name = checkNotNull(name)
    }

    T withAccessModifier(AccessModifier accessModifier) {
      this.accessModifier = checkNotNull(accessModifier)
      return getThis()
    }

    T asStatic() {
      this.staticMethod = true
      return getThis()
    }

    T withReturnType(Type type) {
      this.returnType = checkNotNull(type)
      return getThis()
    }

    T withParameters(List<Parameter> parameters) {
      this.parameters = checkNotNull(parameters)
      assertNoNullInCollection(parameters)
      return getThis()
    }

    abstract T getThis()

    abstract S buildMethod()
  }
}
