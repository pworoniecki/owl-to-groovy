package owl2oo.generator.model.method

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType
import groovy.transform.InheritConstructors

@InheritConstructors
class AddValueMethod extends Accessor {

  Parameter getParameter() {
    return parameters.first()
  }

  static class Builder extends Accessor.Builder<AddValueMethod, Builder> {

    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      assertListTypeOfAttribute(attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static void assertListTypeOfAttribute(Attribute attribute) {
      if (!(attribute.valueType instanceof ListType)) {
        throw new IllegalArgumentException('Cannot create add value method for attribute which is not a list')
      }
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter((attribute.valueType as ListType).extractSimpleParameterizedType(), attribute.name)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    AddValueMethod buildMethod() {
      return new AddValueMethod(this)
    }

    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for add value method.')
    }

    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for add value method.')
    }
  }

  static String getName(Attribute attribute) {
    return "add${attribute.name.capitalize()}"
  }
}
