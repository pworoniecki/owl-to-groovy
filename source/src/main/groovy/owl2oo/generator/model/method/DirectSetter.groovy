package owl2oo.generator.model.method

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.Type
import owl2oo.generator.model.type.VoidType
import groovy.transform.InheritConstructors

@InheritConstructors
class DirectSetter extends Accessor {

  Parameter getParameter() {
    return parameters.first()
  }

  static class Builder extends Accessor.Builder<DirectSetter, Builder> {

    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter(attribute.valueType, attribute.name)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    DirectSetter buildMethod() {
      return new DirectSetter(this)
    }

    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for setter.')
    }

    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for setter.')
    }
  }

  static String getName(Attribute attribute) {
    return "set${attribute.name.capitalize()}Directly"
  }
}
