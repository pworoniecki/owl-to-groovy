package owl2oo.generator.model.method

import owl2oo.generator.model.Parameter
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.Type
import groovy.transform.InheritConstructors

@InheritConstructors
class Getter extends Accessor {

  static class Builder extends Accessor.Builder<Getter, Builder> {

    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      this.parameters = []
      this.returnType = attribute.valueType
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    Getter buildMethod() {
      return new Getter(this)
    }

    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for getter.')
    }

    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for getter.')
    }
  }

  static String getName(Attribute attribute) {
    return "get${attribute.name.capitalize()}"
  }
}
