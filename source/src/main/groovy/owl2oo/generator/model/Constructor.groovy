package owl2oo.generator.model

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

class Constructor {

  private AccessModifier accessModifier = AccessModifier.PUBLIC
  private List<Parameter> parameters = []
  private boolean isStatic = false

  AccessModifier getAccessModifier() {
    return accessModifier
  }

  void setAccessModifier(AccessModifier accessModifier) {
    this.accessModifier = checkNotNull(accessModifier)
  }

  List<Parameter> getParameters() {
    return parameters
  }

  void setParameters(List<Parameter> parameters) {
    assertNoNullInCollection(parameters)
    this.parameters = checkNotNull(parameters)
  }

  boolean isStatic() {
    return isStatic
  }

  void setIsStatic(boolean isStatic) {
    this.isStatic = isStatic
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Constructor that = (Constructor) o
    return accessModifier == that.accessModifier && parameters == that.parameters && isStatic == that.isStatic
  }

  int hashCode() {
    return 31 * accessModifier.hashCode() + parameters.hashCode() + (isStatic ? 1 : 0)
  }
}
