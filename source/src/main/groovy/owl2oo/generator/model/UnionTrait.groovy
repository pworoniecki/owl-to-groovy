package owl2oo.generator.model

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

class UnionTrait extends Trait {

  private List<Trait> unionOf = []

  private UnionTrait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                     List<Attribute> attributes, List<Method> methods) {
    super(packageName, name, annotations, inheritedTraits, attributes, methods)
  }

  void addUnionOf(List<Trait> traits) {
    assertNoNullInCollection(checkNotNull(traits))
    unionOf.addAll(traits)
  }

  boolean isUnionOf(Trait traitModel) {
    return traitModel in unionOf
  }

  List<Trait> getUnionOf() {
    return unionOf
  }

  static class Builder extends TraitBuilder<Builder, UnionTrait> {

    Builder(String packageName, String name) {
      super(packageName, name)
    }

    @Override
    Builder getThis() {
      return this
    }

    @Override
    UnionTrait buildTrait() {
      return new UnionTrait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
