package owl2oo.generator.model

import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method
import groovy.transform.PackageScope

import static com.google.common.base.Preconditions.checkNotNull
import static owl2oo.utils.Preconditions.assertNoNullInCollection

abstract class TraitBuilder<S extends TraitBuilder, T extends Trait> {

  @PackageScope String packageName
  @PackageScope String name
  @PackageScope List<Annotation> annotations = []
  @PackageScope List<Trait> inheritedTraits = []
  @PackageScope List<Attribute> attributes = []
  @PackageScope List<Method> methods = []

  TraitBuilder(String packageName, String name) {
    this.packageName = checkNotNull(packageName)
    this.name = checkNotNull(name)
  }

  S withAnnotations(List<Annotation> annotations) {
    this.annotations = checkNotNull(annotations)
    assertNoNullInCollection(annotations)
    return getThis()
  }

  S withInheritedTraits(List<Trait> traits) {
    this.inheritedTraits = checkNotNull(traits)
    assertNoNullInCollection(traits)
    return getThis()
  }

  S withAttributes(List<Attribute> attributes) {
    this.attributes = checkNotNull(attributes)
    assertNoNullInCollection(attributes)
    assertPublicOrPrivateAttributes(attributes)
    return getThis()
  }

  private static void assertPublicOrPrivateAttributes(List<Attribute> attributes) {
    List<Attribute> illegalAttributes = attributes.findAll {
      it.accessModifier != AccessModifier.PRIVATE && it.accessModifier != AccessModifier.PUBLIC
    }
    if (illegalAttributes) {
      throw new IllegalArgumentException('Trait may contain only private and public attributes. ' +
          "Illegal ones are: ${attributes.collect {"$it.valueType.simpleName $it.name"}}")
    }
  }

  S withMethods(List<Method> methods) {
    this.methods = checkNotNull(methods)
    assertNoNullInCollection(methods)
    assertPublicOrPrivateMethods(methods)
    return getThis()
  }

  private static void assertPublicOrPrivateMethods(List<Method> methods) {
    List<Method> illegalMethods = methods.findAll {
      it.accessModifier != AccessModifier.PRIVATE && it.accessModifier != AccessModifier.PUBLIC
    }
    if (illegalMethods) {
      throw new IllegalArgumentException('Trait may contain only private and public methods. ' +
          "Illegal ones are: ${methods.collect {"$it.returnType.simpleName $it.name"}}")
    }
  }

  abstract S getThis()

  abstract T buildTrait()
}
