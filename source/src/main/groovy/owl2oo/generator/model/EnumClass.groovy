package owl2oo.generator.model

import groovy.transform.InheritConstructors

@InheritConstructors
class EnumClass extends Class {

  @InheritConstructors
  static class Builder extends ClassBuilder<Builder, EnumClass> {

    @Override
    Builder getThis() {
      return this
    }

    @Override
    EnumClass buildClass() {
      return new EnumClass(packageName, className, annotations, inheritedClass, constructors, attributes, methods,
          implementedTraits)
    }
  }
}
