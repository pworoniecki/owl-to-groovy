package owl2oo.generator

import owl2oo.generator.model.attribute.Attribute
import owl2oo.utils.StringUtils
import org.springframework.stereotype.Component

@Component
class AttributeCodeGenerator {

  String generateCode(Attribute attribute) {
    def accessModifier = StringUtils.emptyOrAddSpace(attribute.accessModifier.toGroovyCode())
    def staticModifier = attribute.isStatic() ? 'static ' : ''
    def defaultValue = attribute.defaultValue ? " = ${attribute.defaultValue}" : ''

    return "$accessModifier$staticModifier${attribute.valueType.simpleName} ${attribute.name}$defaultValue"
  }
}
