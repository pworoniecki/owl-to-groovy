package owl2oo.generator

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratorConfiguration
import owl2oo.generator.model.Trait
import owl2oo.generator.model.codebuilder.TraitCodeBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
class TraitCodeGenerator {

  private ApplicationContext applicationContext
  private TraitSignatureCodeGenerator traitSignatureCodeGenerator
  private GeneratorConfiguration generatorConfiguration

  @Autowired
  TraitCodeGenerator(ApplicationContext applicationContext, TraitSignatureCodeGenerator traitSignatureCodeGenerator,
                     ApplicationConfiguration applicationConfiguration) {
    this.applicationContext = applicationContext
    this.traitSignatureCodeGenerator = traitSignatureCodeGenerator
    this.generatorConfiguration = applicationConfiguration.generator
  }

  String generateCode(Trait traitModel) {
    TraitBodyCodeGenerator traitBodyGenerator = applicationContext.getBean(TraitBodyCodeGenerator)
    traitModel.accept(traitBodyGenerator)

    def codeBuilder = new TraitCodeBuilder(traitModel)
    codeBuilder.with {
      appendPackage()
      appendEmptyLine()
      appendImports(generatorConfiguration)
      appendEmptyLine()
      appendAnnotations(traitModel.annotations)
      appendCode(traitSignatureCodeGenerator.generateTraitSignature(traitModel))
      appendEmptyLine()
      appendStaticAttributes(traitBodyGenerator.attributeCodes)
      appendNonStaticAttributes(traitBodyGenerator.attributeCodes)
      appendStaticMethodsWithEmptyLines(traitBodyGenerator.methodCodes)
      appendNonStaticMethodsWithEmptyLines(traitBodyGenerator.methodCodes)
      appendTraitSignatureEnd()
      appendEmptyLine()
    }

    return codeBuilder.code
  }
}
