package owl2oo.generator

import owl2oo.generator.model.method.Method
import owl2oo.generator.model.attribute.Attribute

interface TraitElementVisitor {

  void visit(Method method)

  void visit(Attribute attribute)
}
