package owl2oo.converter

import owl2oo.converter.attribute.AttributesExtractor
import owl2oo.converter.trait.TraitsExtractor
import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait
import owl2oo.generator.model.attribute.Attribute
import owl2oo.parser.OntologyParser
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class OntologyToCodeModelConverter {

  private OntologyParser ontologyParser
  private AttributesExtractor attributesExtractor
  private ClassesExtractor classesExtractor
  private TraitsExtractor traitsExtractor

  @Autowired
  OntologyToCodeModelConverter(OntologyParser ontologyParser, AttributesExtractor attributesExtractor,
                               ClassesExtractor classesExtractor, TraitsExtractor traitsExtractor) {
    this.ontologyParser = ontologyParser
    this.attributesExtractor = attributesExtractor
    this.classesExtractor = classesExtractor
    this.traitsExtractor = traitsExtractor
  }

  ObjectOrientedCodeModel convert(InputStream ontologyContentStream) {
    OWLOntology ontology = ontologyParser.parse(ontologyContentStream)
    Map<OWLProperty, Attribute> attributes = attributesExtractor.extract(ontology)
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)
    List<Class> classes = classesExtractor.extract(ontology, traits)
    return new ObjectOrientedCodeModel(classes, traits.values().flatten().toList())
  }
}
