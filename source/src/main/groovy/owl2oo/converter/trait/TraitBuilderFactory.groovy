package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.Trait
import owl2oo.generator.model.TraitBuilder
import owl2oo.generator.model.UnionTrait

import static owl2oo.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

@Component
class TraitBuilderFactory {

  private String traitPackage
  private UnionClassChecker unionClassChecker

  TraitBuilderFactory(ApplicationConfiguration configuration, UnionClassChecker unionClassChecker) {
    traitPackage = configuration.generator.generatedClasses.modelFullPackage
    this.unionClassChecker = unionClassChecker
  }

  TraitBuilder<? extends TraitBuilder, ? extends Trait> createTraitBuilder(OWLClass owlClass, OWLOntology ontology) {
    String traitName = extractName(owlClass) + TRAIT_NAME_SUFFIX
    return unionClassChecker.isUnionClass(owlClass, ontology) ? createUnionTrait(traitName) : createTrait(traitName)
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }

  private Trait.Builder createTrait(String traitName) {
    return new Trait.Builder(traitPackage, traitName)
  }

  private UnionTrait.Builder createUnionTrait(String traitName) {
    return new UnionTrait.Builder(traitPackage, traitName)
  }
}
