package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2oo.converter.attribute.domain.PropertyDomainExtractor
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type

@Component
class ClassPropertyAttributesExtractor {

  private PropertyDomainExtractor propertyDomainExtractor

  ClassPropertyAttributesExtractor(PropertyDomainExtractor propertyDomainExtractor) {
    this.propertyDomainExtractor = propertyDomainExtractor
  }

  List<Attribute> extractClassAttributes(OWLOntology ontology, OWLClass owlClass, Map<OWLProperty, Attribute> attributes) {
    return attributes.findAll { OWLProperty property, Attribute attribute ->
      belongsPropertyToClass(property, owlClass, ontology)
    }.values().toList()
  }

  private boolean belongsPropertyToClass(OWLProperty property, OWLClass owlClass, OWLOntology ontology) {
    Type propertyDomain = propertyDomainExtractor.extractDomain(property, ontology)
    return isTypeCreatedFromOwlClass(propertyDomain, owlClass)
  }

  private static boolean isTypeCreatedFromOwlClass(Type type, OWLClass owlClass) {
    Type extractedType = type instanceof ListType ? type.extractSimpleParameterizedType() : type
    return extractedType.simpleName == extractName(owlClass)
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }
}
