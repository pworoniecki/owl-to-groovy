package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2oo.converter.TraitInstanceMethodsFactory
import owl2oo.generator.model.EquivalentTrait
import owl2oo.generator.model.Trait
import owl2oo.generator.model.UnionTrait
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.SetType
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type

import static owl2oo.generator.model.AccessModifier.PRIVATE

@Component
class TraitsExtractor {

  static final String TRAIT_NAME_SUFFIX = 'Trait'
  static final String ALL_INSTANCES_ATTRIBUTE_NAME = 'allInstances'

  private BaseTraitsExtractor baseTraitsExtractor
  private TraitInstanceMethodsFactory traitInstanceMethodsFactory
  private EquivalentTraitsExtractor equivalentTraitsExtractor

  TraitsExtractor(BaseTraitsExtractor baseTraitsExtractor, TraitInstanceMethodsFactory traitInstanceMethodsFactory,
                  EquivalentTraitsExtractor equivalentTraitsExtractor) {
    this.baseTraitsExtractor = baseTraitsExtractor
    this.traitInstanceMethodsFactory = traitInstanceMethodsFactory
    this.equivalentTraitsExtractor = equivalentTraitsExtractor
  }

  Map<OWLClass, List<Trait>> extract(OWLOntology ontology, Map<OWLProperty, Attribute> attributes) {
    Map<OWLClass, Trait> baseTraits = baseTraitsExtractor.extract(ontology, attributes)
    Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits = equivalentTraitsExtractor.extract(baseTraits, ontology)
    addAllInstancesAttributeToTraits(baseTraits, equivalentTraits)
    addInstanceMethodsToBaseTraits(baseTraits.values(), equivalentTraits.values().findAll { it.isPresent() }*.get())
    return baseTraits.collectEntries {
      [(it.key): extractTraitsForOwlClass(it.key, baseTraits, equivalentTraits)]
    } as Map<OWLClass, List<Trait>>
  }

  private static Map<OWLClass, Trait> addAllInstancesAttributeToTraits(
      Map<OWLClass, Trait> baseTraits, Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits) {
    return baseTraits.findAll { owlClass, traitModel -> !(traitModel instanceof UnionTrait) }.each { owlClass, traitModel ->
      traitModel.attributes.add(0, createAllClassInstancesAttribute(traitModel, equivalentTraits[owlClass]))
    }
  }

  private static Attribute createAllClassInstancesAttribute(Trait traitModel, Optional<EquivalentTrait> equivalentTrait) {
    Trait attributeTypeTrait = equivalentTrait.map { it as Trait }.orElse(traitModel)
    Type type = new SetType(new SimpleType(attributeTypeTrait))
    return new Attribute.AttributeBuilder(ALL_INSTANCES_ATTRIBUTE_NAME, type).asStatic().withAccessModifier(PRIVATE)
        .withDefaultValue('[].toSet()').buildAttribute()
  }

  private void addInstanceMethodsToBaseTraits(Collection<Trait> baseTraits, List<EquivalentTrait> equivalentTraits) {
    baseTraits.each { it.methods.addAll(traitInstanceMethodsFactory.create(it, equivalentTraits)) }
  }

  private static List<Trait> extractTraitsForOwlClass(OWLClass owlClass, Map<OWLClass, Trait> baseTraits,
                                                      Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits) {
    Trait classTrait = baseTraits[owlClass]
    Optional<Trait> equivalentTrait = equivalentTraits[owlClass]
    return equivalentTrait.isPresent() ? [equivalentTrait.get(), classTrait] : [classTrait]
  }
}
