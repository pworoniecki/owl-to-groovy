package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.Annotation
import owl2oo.generator.model.type.SimpleType
import owl2oo.resultcodebase.validator.DisjointWith
import owl2oo.utils.SubPackageNameExtractor

import static org.semanticweb.owlapi.model.AxiomType.DISJOINT_UNION
import static owl2oo.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

@Component
class AnnotationsExtractor {

  private String traitBasePackage

  AnnotationsExtractor(ApplicationConfiguration configuration) {
    traitBasePackage = configuration.generator.generatedClasses.basePackage
  }

  List<Annotation> extract(OWLClass owlClass, OWLOntology ontology) {
    List<Annotation> annotations = []
    createDisjointWithAnnotation(ontology, owlClass).ifPresent { annotations.add(it) }
    return annotations
  }

  private Optional<Annotation> createDisjointWithAnnotation(OWLOntology ontology, OWLClass owlClass) {
    List<OWLClass> disjointClasses = findDisjointClassesByDisjointUnionAxiom(ontology, owlClass) +
        findDisjointClassesByDisjointClassesAnxiom(ontology, owlClass)
    List<String> disjointClassTraits = disjointClasses.collect { extractName(it) + TRAIT_NAME_SUFFIX }
    return disjointClassTraits.isEmpty() ? Optional.empty() : Optional.of(createDisjointWithAnnotation(disjointClassTraits))
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }

  private Annotation createDisjointWithAnnotation(List<String> disjointWithTraits) {
    String typePackage = traitBasePackage + '.' + SubPackageNameExtractor.getSubPackageName(DisjointWith)
    String typeClassName = DisjointWith.simpleName
    return new Annotation(new SimpleType(typePackage, typeClassName), disjointWithTraits)
  }

  private static List<OWLClass> findDisjointClassesByDisjointUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointUnionAxiom> disjointUnionAxioms = ontology.axioms().findAll { it.axiomType == DISJOINT_UNION }
    List<OWLDisjointUnionAxiom> axiomsRelatedToClass =
        disjointUnionAxioms.findAll { it.classesInSignature().any { it == owlClass } }
    return axiomsRelatedToClass.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }

  private static List<OWLClass> findDisjointClassesByDisjointClassesAnxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointClassesAxiom> disjointClassesAxioms = ontology.disjointClassesAxioms(owlClass).findAll()
    return disjointClassesAxioms.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }
}
