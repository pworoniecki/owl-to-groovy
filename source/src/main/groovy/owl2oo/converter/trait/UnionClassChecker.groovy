package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2oo.exception.NotSupportedOwlElementException

import static org.semanticweb.owlapi.model.ClassExpressionType.OBJECT_UNION_OF

@Component
class UnionClassChecker {

  boolean isUnionClass(OWLClass owlClass, OWLOntology ontology) {
    return isEquivalentToClassesUnion(ontology, owlClass) || isDisjointUnion(ontology, owlClass)
  }

  private static boolean isEquivalentToClassesUnion(OWLOntology ontology, OWLClass owlClass) {
    List<OWLEquivalentClassesAxiom> unionEquivalentClassesAxiom = ontology.equivalentClassesAxioms(owlClass)
        .findAll { isEquivalentOfClassesUnion(it) }
    if (unionEquivalentClassesAxiom.isEmpty()) {
      return false
    }
    if (unionEquivalentClassesAxiom.size() == 1) {
      return true
    }
    throw new NotSupportedOwlElementException('OWL class cannot be equivalent of more than one unions!')
  }

  private static boolean isEquivalentOfClassesUnion(OWLEquivalentClassesAxiom equivalentClassesAxiom) {
    return equivalentClassesAxiom.classExpressions().any { it.classExpressionType == OBJECT_UNION_OF }
  }

  private static boolean isDisjointUnion(OWLOntology ontology, OWLClass owlClass) {
    return ontology.disjointUnionAxioms(owlClass).count() > 0
  }
}
