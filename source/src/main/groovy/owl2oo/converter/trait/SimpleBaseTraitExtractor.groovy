package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2oo.converter.attribute.AttributeMethodsFactory
import owl2oo.generator.model.Trait
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

@Component
class SimpleBaseTraitExtractor {

  private AttributeMethodsFactory attributeMethodsFactory
  private ClassPropertyAttributesExtractor classPropertyAttributesExtractor
  private TraitBuilderFactory traitBuilderFactory
  private AnnotationsExtractor annotationsExtractor

  SimpleBaseTraitExtractor(AttributeMethodsFactory attributeMethodsFactory,
                           ClassPropertyAttributesExtractor classPropertyAttributesExtractor,
                           TraitBuilderFactory traitBuilderFactory, AnnotationsExtractor annotationsExtractor) {
    this.attributeMethodsFactory = attributeMethodsFactory
    this.traitBuilderFactory = traitBuilderFactory
    this.classPropertyAttributesExtractor = classPropertyAttributesExtractor
    this.annotationsExtractor = annotationsExtractor
  }

  Trait extract(OWLOntology ontology, OWLClass owlClass, Map<OWLProperty, Attribute> attributes) {
    List<Attribute> propertyAttributes = classPropertyAttributesExtractor.extractClassAttributes(ontology, owlClass, attributes)
    return traitBuilderFactory.createTraitBuilder(owlClass, ontology)
        .withAnnotations(annotationsExtractor.extract(owlClass, ontology))
        .withAttributes(propertyAttributes)
        .withMethods(extractPropertyMethods(propertyAttributes)).buildTrait()
  }

  private List<Method> extractPropertyMethods(List<Attribute> attributes) {
    return attributes.collect { attributeMethodsFactory.createMethods(it) }.flatten() as List<Method>
  }
}
