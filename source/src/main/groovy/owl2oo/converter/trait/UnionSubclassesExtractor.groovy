package owl2oo.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLObjectUnionOf
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component

import static org.semanticweb.owlapi.model.ClassExpressionType.OBJECT_UNION_OF

@Component
class UnionSubclassesExtractor {

  List<OWLClass> extract(OWLOntology ontology, OWLClass owlClass) {
    if (ontology.equivalentClassesAxioms(owlClass).count() > 0) {
      return findUnionSubclassesInEquivalentUnionAxiom(ontology, owlClass)
    }
    return findUnionSubclassesInDisjointUnionAxiom(ontology, owlClass)
  }

  private static List<OWLClass> findUnionSubclassesInEquivalentUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    OWLEquivalentClassesAxiom equivalentClassesAxiom = ontology.equivalentClassesAxioms(owlClass)
        .find { it.classExpressions().any { it.classExpressionType == OBJECT_UNION_OF } } as OWLEquivalentClassesAxiom
    OWLObjectUnionOf objectUnionOf = equivalentClassesAxiom.classExpressions()
        .find { it.classExpressionType == OBJECT_UNION_OF } as OWLObjectUnionOf
    return objectUnionOf.classesInSignature().findAll { it != owlClass }
  }

  private static List<OWLClass> findUnionSubclassesInDisjointUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointUnionAxiom> disjointUnionAxioms = ontology.disjointUnionAxioms(owlClass).findAll()
    return disjointUnionAxioms.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }
}
