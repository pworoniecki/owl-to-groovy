package owl2oo.converter

import owl2oo.generator.model.Constructor
import org.springframework.stereotype.Component

import static owl2oo.generator.model.AccessModifier.PRIVATE

@Component
class ConstructorsExtractor {

  List<Constructor> extract() {
    return createConstructorsForNonEnumClass()
  }

  private static List<Constructor> createConstructorsForNonEnumClass() {
    return [new Constructor(accessModifier: PRIVATE, isStatic: false, parameters: [])]
  }
}
