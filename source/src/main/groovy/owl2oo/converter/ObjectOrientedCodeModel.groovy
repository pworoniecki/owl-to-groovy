package owl2oo.converter

import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait

class ObjectOrientedCodeModel {

  private List<Class> classes
  private List<Trait> traits

  ObjectOrientedCodeModel(List<Class> classes, List<Trait> traits) {
    this.classes = classes
    this.traits = traits
  }

  List<Class> getClasses() {
    return classes
  }

  List<Trait> getTraits() {
    return traits
  }
}
