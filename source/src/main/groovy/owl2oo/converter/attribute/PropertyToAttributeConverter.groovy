package owl2oo.converter.attribute

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.converter.attribute.range.PropertyRangeExtractor
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.AccessModifier
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.type.Type

@Component
class PropertyToAttributeConverter {

  private PropertyRangeExtractor propertyRangeExtractor
  private AttributeCharacteristicsExtractor characteristicsExtractor
  private AttributeDefaultValueExtractor defaultValueExtractor

  @Autowired
  PropertyToAttributeConverter(PropertyRangeExtractor propertyRangeExtractor,
                               AttributeCharacteristicsExtractor characteristicsExtractor,
                               AttributeDefaultValueExtractor defaultValueExtractor) {
    this.propertyRangeExtractor = propertyRangeExtractor
    this.characteristicsExtractor = characteristicsExtractor
    this.defaultValueExtractor = defaultValueExtractor
  }

  Attribute convertWithoutRelations(OWLObjectProperty property, OWLOntology ontology) {
    Type range = propertyRangeExtractor.extractRange(property, ontology)
    Optional<String> defaultValue = defaultValueExtractor.extract(property, ontology)
    def builder = new Attribute.AttributeBuilder(extractName(property), range)
        .withCharacteristics(characteristicsExtractor.extract(property, ontology))
        .withAccessModifier(AccessModifier.PRIVATE)
    return defaultValue.isPresent() ? builder.withDefaultValue(defaultValue.get()).buildAttribute() : builder.buildAttribute()
  }

  private static String extractName(OWLProperty property) {
    return property.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Invalid property's IRI: ${property.getIRI()}") }
  }

  Attribute convertWithoutRelations(OWLDataProperty property, OWLOntology ontology) {
    Type range = propertyRangeExtractor.extractRange(property, ontology)
    Optional<String> defaultValue = defaultValueExtractor.extract(property, ontology)
    def builder = new Attribute.AttributeBuilder(extractName(property), range)
        .withCharacteristics(characteristicsExtractor.extract(property, ontology))
        .withAccessModifier(AccessModifier.PRIVATE)
    return defaultValue.isPresent() ? builder.withDefaultValue(defaultValue.get()).buildAttribute() : builder.buildAttribute()
  }
}
