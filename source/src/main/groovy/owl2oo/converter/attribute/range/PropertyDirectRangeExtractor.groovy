package owl2oo.converter.attribute.range

import owl2oo.utils.Logger
import org.semanticweb.owlapi.model.AxiomType
import org.semanticweb.owlapi.model.DataRangeType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLDataRange
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PropertyDirectRangeExtractor {

  private Logger logger

  @Autowired
  PropertyDirectRangeExtractor(Logger logger) {
    this.logger = logger
  }

  Optional<OWLClass> extractSingleDirectRange(OWLObjectProperty property, OWLOntology ontology) {
    List<OWLClassExpression> rangeDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.OBJECT_PROPERTY_RANGE }
        .collect { (it as OWLObjectPropertyRangeAxiom).range } as List<OWLClassExpression>
    if (rangeDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (rangeDeclarations.size() > 1) {
      logger.warn("More than 1 range declarations found for property $property but only 1 is supported at the time. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declarations: $rangeDeclarations")
      return Optional.empty()
    }
    if (rangeDeclarations.first() && !(rangeDeclarations.first() instanceof OWLClass)) {
      logger.warn("Unsupported type of range declaration found for property $property. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declaration: ${rangeDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(rangeDeclarations.first() as OWLClass)
  }

  Optional<OWLDatatype> extractSingleDirectRange(OWLDataProperty property, OWLOntology ontology) {
    List<OWLDataRange> rangeDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.DATA_PROPERTY_RANGE }
        .collect { (it as OWLDataPropertyRangeAxiom).range } as List<OWLDataRange>
    if (rangeDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (rangeDeclarations.size() > 1) {
      logger.warn("More than 1 range declarations found for property $property but only 1 is supported at the time. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declarations: $rangeDeclarations")
      return Optional.empty()
    }
    if (rangeDeclarations.first() && rangeDeclarations.first().dataRangeType != DataRangeType.DATATYPE) {
      logger.warn("Unsupported type of range declaration found for property $property. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declaration: ${rangeDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(rangeDeclarations.first().asOWLDatatype())
  }
}
