package owl2oo.converter.attribute.range

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.converter.attribute.PredefinedTypes
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.Type
import owl2oo.utils.OptionalTransfomer

@Component
class DataPropertyRangeExtractor {

  private String generatedClassesPackage
  private PredefinedTypes predefinedTypes
  private PropertyDirectRangeExtractor directRangeExtractor
  private OWLDatatypeToTypeConverter datatypeToTypeConverter

  @Autowired
  DataPropertyRangeExtractor(ApplicationConfiguration applicationConfiguration, PredefinedTypes predefinedTypes,
                             PropertyDirectRangeExtractor directRangeExtractor,
                             OWLDatatypeToTypeConverter datatypeToTypeConverter) {
    this.generatedClassesPackage = applicationConfiguration.generator.generatedClasses.modelFullPackage
    this.predefinedTypes = predefinedTypes
    this.directRangeExtractor = directRangeExtractor
    this.datatypeToTypeConverter = datatypeToTypeConverter
  }

  Type extractRange(OWLDataProperty property, OWLOntology ontology) {
    Optional<Type> simpleType = OptionalTransfomer.of(directRangeExtractor.extractSingleDirectRange(property, ontology))
        .or { extractRangeOfEquivalentProperty(property, ontology) }
        .or { extractRangeOfSuperProperty(property, ontology) }
        .getResult()
        .map { datatypeToTypeConverter.convert(it) }
    if (hasSingleType(property, ontology)) {
      return simpleType.orElse(predefinedTypes.thingTraitType)
    }
    return simpleType.isPresent() ? new ListType(simpleType.get()) : predefinedTypes.thingTraitListType
  }

  private static boolean hasSingleType(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0
  }

  private Optional<OWLDatatype> extractRangeOfEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLDataProperty> findEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    OWLEquivalentObjectPropertiesAxiom equivalentPropertyAxiom =
        ontology.equivalentDataPropertiesAxioms(property).find() as OWLEquivalentObjectPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLDataProperty)
  }

  private Optional<OWLDatatype> extractRangeOfSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLDataProperty> findSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    OWLSubObjectPropertyOfAxiom subPropertyAxiom = ontology.dataSubPropertyAxiomsForSubProperty(property).find() as OWLSubObjectPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLDataProperty)
  }
}
