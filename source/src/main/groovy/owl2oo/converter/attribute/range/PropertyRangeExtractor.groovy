package owl2oo.converter.attribute.range

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.generator.model.type.Type

@Component
class PropertyRangeExtractor {

  private ObjectPropertyRangeExtractor objectPropertyRangeExtractor
  private DataPropertyRangeExtractor dataPropertyRangeExtractor

  @Autowired
  PropertyRangeExtractor(ObjectPropertyRangeExtractor objectPropertyRangeExtractor,
                         DataPropertyRangeExtractor dataPropertyRangeExtractor) {
    this.objectPropertyRangeExtractor = objectPropertyRangeExtractor
    this.dataPropertyRangeExtractor = dataPropertyRangeExtractor
  }

  Type extractRange(OWLObjectProperty property, OWLOntology ontology) {
    return objectPropertyRangeExtractor.extractRange(property, ontology)
  }

  Type extractRange(OWLDataProperty property, OWLOntology ontology) {
    return dataPropertyRangeExtractor.extractRange(property, ontology)
  }
}
