package owl2oo.converter.attribute.range

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import owl2oo.resultcodebase.owlSimpleType.Language
import owl2oo.resultcodebase.owlSimpleType.NCName
import owl2oo.resultcodebase.owlSimpleType.NMTOKEN
import owl2oo.resultcodebase.owlSimpleType.Name
import owl2oo.resultcodebase.owlSimpleType.NegativeInteger
import owl2oo.resultcodebase.owlSimpleType.NonNegativeInteger
import owl2oo.resultcodebase.owlSimpleType.NonPositiveInteger
import owl2oo.resultcodebase.owlSimpleType.NormalizedString
import owl2oo.resultcodebase.owlSimpleType.OwlType
import owl2oo.resultcodebase.owlSimpleType.PositiveInteger
import owl2oo.resultcodebase.owlSimpleType.Token
import owl2oo.resultcodebase.owlSimpleType.UnsignedByte
import owl2oo.resultcodebase.owlSimpleType.UnsignedInt
import owl2oo.resultcodebase.owlSimpleType.UnsignedLong
import owl2oo.resultcodebase.owlSimpleType.UnsignedShort
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDatatype
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.xml.datatype.XMLGregorianCalendar

import static owl2oo.utils.SubPackageNameExtractor.getSubPackageName

@Component
class OWLDatatypeToTypeConverter {

  private static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'
  private static final Map<String, Class> TYPE_MAPPING = [
      string            : String,
      normalizedString  : NormalizedString,
      token             : Token,
      language          : Language,
      NMTOKEN           : NMTOKEN,
      Name              : Name,
      NCName            : NCName,
      boolean           : Boolean,
      decimal           : BigDecimal,
      float             : Float,
      double            : Double,
      integer           : BigInteger,
      nonPositiveInteger: NonPositiveInteger,
      negativeInteger   : NegativeInteger,
      long              : Long,
      int               : Integer,
      short             : Short,
      byte              : Byte,
      nonNegativeInteger: NonNegativeInteger,
      unsignedLong      : UnsignedLong,
      unsignedInt       : UnsignedInt,
      unsignedShort     : UnsignedShort,
      unsignedByte      : UnsignedByte,
      positiveInteger   : PositiveInteger,
      dateTime          : XMLGregorianCalendar,
      time              : XMLGregorianCalendar,
      date              : XMLGregorianCalendar,
      gYearMonth        : XMLGregorianCalendar,
      gYear             : XMLGregorianCalendar,
      gMonthDay         : XMLGregorianCalendar,
      gDay              : XMLGregorianCalendar,
      gMonth            : XMLGregorianCalendar
  ]

  private String generatedCodePackage

  @Autowired
  OWLDatatypeToTypeConverter(ApplicationConfiguration applicationConfiguration) {
    this.generatedCodePackage = applicationConfiguration.generator.generatedClasses.basePackage
  }

  Type convert(OWLDatatype datatype) {
    IRI typeIdentifier = datatype.getIRI()
    assertCorrectTypeNamespace(typeIdentifier)
    String name = typeIdentifier.remainder.orElseThrow {
      new MissingIRIRemainderException("Missing remainder in identifier of datatype: $datatype")
    }
    return createType(TYPE_MAPPING[name])
  }

  private SimpleType createType(Class typeClass) {
    return isCustomOwlType(typeClass) ?
        new SimpleType(generatedCodePackage + '.' + getSubPackageName(typeClass), typeClass.simpleName) :
        new SimpleType(typeClass)
  }

  private static boolean isCustomOwlType(Class typeClass) {
    return typeClass.package.name.startsWith(OwlType.package.name)
  }

  private static void assertCorrectTypeNamespace(IRI typeIdentifier) {
    if (typeIdentifier.namespace != "$XML_SCHEMA_NAMESPACE#") {
      throw new IllegalArgumentException("Datatype must be value from XML Schema namespace " +
          "(http://www.w3.org/2001/XMLSchema). Provided value ($typeIdentifier) is from another namespace: " +
          "(${typeIdentifier.namespace}).")
    }
  }
}
