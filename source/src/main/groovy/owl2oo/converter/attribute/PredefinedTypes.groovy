package owl2oo.converter.attribute

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.converter.trait.TraitsExtractor
import owl2oo.generator.model.Class
import owl2oo.generator.model.Trait
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type

@Component
class PredefinedTypes {

  private static final String THING_CLASS_NAME = 'Thing'

  private String packageName
  private Class thingClass
  private Trait thingTrait
  private Type thingTraitType
  private Type thingTraitListType

  @Autowired
  PredefinedTypes(ApplicationConfiguration applicationConfiguration) {
    packageName = applicationConfiguration.generator.generatedClasses.modelFullPackage
    thingTrait = new Trait.Builder(packageName, THING_CLASS_NAME + TraitsExtractor.TRAIT_NAME_SUFFIX).buildTrait()
    thingClass = new Class.Builder(packageName, THING_CLASS_NAME).withImplementedTraits([thingTrait]).buildClass()
    thingTraitType = new SimpleType(thingTrait)
    thingTraitListType = new ListType(thingTraitType)
  }

  Class getThingClass() {
    return thingClass
  }

  Trait getThingTrait() {
    return thingTrait
  }

  Type getThingTraitType() {
    return thingTraitType
  }

  Type getThingTraitListType() {
    return thingTraitListType
  }
}
