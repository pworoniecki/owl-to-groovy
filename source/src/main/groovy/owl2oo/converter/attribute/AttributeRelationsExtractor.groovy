package owl2oo.converter.attribute

import owl2oo.exception.MissingIRIRemainderException
import owl2oo.exception.NotSupportedOwlElementException
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.attribute.relation.AttributeRelation
import owl2oo.generator.model.attribute.relation.EquivalentToAttributeRelation
import owl2oo.generator.model.attribute.relation.InverseAttributeRelation
import owl2oo.generator.model.attribute.relation.SuperAttributeOfRelation
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.stereotype.Component

@Component
class AttributeRelationsExtractor {

  List<AttributeRelation> extract(OWLObjectProperty property, OWLOntology ontology, Map<OWLObjectProperty, Attribute> attributes) {
    List<AttributeRelation> relations = []
    relations.addAll(extractInverseAttributeRelations(ontology, property, attributes))
    relations.addAll(extractSuperAttributeOfRelations(ontology, property, attributes))
    extractEquivalentAttributeRelation(ontology, property, attributes).ifPresent { relations.add(it) }
    return relations
  }

  private static List<InverseAttributeRelation> extractInverseAttributeRelations(OWLOntology ontology, OWLObjectProperty property,
                                                                                 Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLInverseObjectPropertiesAxiom> inversePropertyAxioms = ontology.inverseObjectPropertyAxioms(property).findAll()
    List<OWLObjectProperty> inverseProperties = inversePropertyAxioms*.properties.flatten()
        .findAll { !it.is(property) } as List<OWLObjectProperty>
    List<Attribute> inverseAttributes = inverseProperties.collect { mapPropertyToAttribute(it, attributes) }
    return inverseAttributes.collect { new InverseAttributeRelation(it) }
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(OWLOntology ontology, OWLObjectProperty property,
                                                                                 Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLSubObjectPropertyOfAxiom> superPropertyAxioms = ontology.objectSubPropertyAxiomsForSuperProperty(property).findAll()
    return extractSuperAttributeOfRelations(superPropertyAxioms*.subProperty as List<OWLObjectProperty>, attributes)
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(List<OWLProperty> superProperties,
                                                                                 Map<OWLProperty, Attribute> attributes) {
    List<Attribute> superAttributes = superProperties.collect { mapPropertyToAttribute(it, attributes) }
    return superAttributes.collect { new SuperAttributeOfRelation(it) }
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      OWLOntology ontology, OWLObjectProperty property, Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLEquivalentObjectPropertiesAxiom> equivalentPropertyAxioms =
        ontology.equivalentObjectPropertiesAxioms(property).findAll()
    List<OWLObjectProperty> equivalentProperties = equivalentPropertyAxioms*.properties()*.findAll { !it.is(property) }
        .flatten() as List<OWLObjectProperty>
    return extractEquivalentAttributeRelation(equivalentProperties, attributes, property)
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      List<OWLProperty> equivalentProperties, Map<OWLProperty, Attribute> attributes, OWLProperty property) {
    List<Attribute> equivalentAttributes = equivalentProperties.collect { mapPropertyToAttribute(it, attributes) }
    if (equivalentAttributes.isEmpty()) {
      return Optional.empty()
    }
    assertSingleEquivalentAttribute(equivalentAttributes, property)
    return Optional.of(new EquivalentToAttributeRelation(equivalentAttributes.first()))
  }

  private static void assertSingleEquivalentAttribute(List<Attribute> equivalentAttributes, OWLProperty property) {
    if (equivalentAttributes.size() > 1) {
      throw new NotSupportedOwlElementException("More than 1 property are equivalent to ${property.getIRI()}. " +
          "Only 1 equivalent property is supported. Please adjust ontology to this limitation.")
    }
  }

  private static Attribute mapPropertyToAttribute(OWLProperty property, Map<OWLProperty, Attribute> attributes) {
    String propertyName = extractName(property)
    Attribute attribute = attributes[property]
    if (!attribute) {
      throw new IllegalArgumentException("Missing attribute for property $propertyName")
    }
    return attribute
  }

  private static String extractName(OWLProperty property) {
    return property.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Invalid property's IRI: ${property.getIRI()}") }
  }

  List<AttributeRelation> extract(OWLDataProperty property, OWLOntology ontology, Map<OWLDataProperty, Attribute> attributes) {
    List<AttributeRelation> relations = []
    relations.addAll(extractSuperAttributeOfRelations(ontology, property, attributes))
    extractEquivalentAttributeRelation(ontology, property, attributes).ifPresent { relations.add(it) }
    return relations
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(OWLOntology ontology, OWLDataProperty property,
                                                                                 Map<OWLDataProperty, Attribute> attributes) {
    List<OWLDataProperty> superProperties = ontology.dataSubPropertyAxiomsForSuperProperty(property)*.subProperty
        .findAll { it instanceof OWLDataProperty && !it.is(property) } as List<OWLDataProperty>
    return extractSuperAttributeOfRelations(superProperties, attributes)
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      OWLOntology ontology, OWLDataProperty property, Map<OWLDataProperty, Attribute> attributes) {
    List<OWLEquivalentDataPropertiesAxiom> equivalentPropertyAxioms = ontology.equivalentDataPropertiesAxioms(property).findAll()
    List<OWLDataProperty> equivalentProperties = equivalentPropertyAxioms*.properties()*.findAll { !it.is(property) }
        .flatten() as List<OWLDataProperty>
    return extractEquivalentAttributeRelation(equivalentProperties, attributes, property)
  }
}
