package owl2oo.converter.attribute

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component

@Component
class AttributeDefaultValueExtractor {

  Optional<String> extract(OWLObjectProperty property, OWLOntology ontology) {
    def isReflexiveProperty = isReflexive(property, ontology)
    if (isSingleTypeProperty(property, ontology)) {
      return isReflexiveProperty ? Optional.of('this') : Optional.empty()
    }
    return Optional.of(isReflexiveProperty ? '[this]' : '[]')
  }

  private static boolean isSingleTypeProperty(OWLObjectProperty property, OWLOntology ontology) {
    return ontology.functionalObjectPropertyAxioms(property).count() > 0
  }

  Optional<String> extract(OWLDataProperty property, OWLOntology ontology) {
    return isSingleTypeProperty(property, ontology) ? Optional.empty() : Optional.of('[]')
  }

  private static boolean isSingleTypeProperty(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0
  }

  private static boolean isReflexive(OWLObjectProperty property, OWLOntology ontology) {
    return ontology.reflexiveObjectPropertyAxioms(property).count() > 0
  }
}
