package owl2oo.converter.attribute

import owl2oo.generator.method.common.TransitiveAttributeHelpersGenerator
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.AddValueMethod
import owl2oo.generator.model.method.DirectSetter
import owl2oo.generator.model.method.Getter
import owl2oo.generator.model.method.Method
import owl2oo.generator.model.method.RemoveValueMethod
import owl2oo.generator.model.method.Setter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.generator.model.type.ListType

import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class AttributeMethodsFactory {

  private TransitiveAttributeHelpersGenerator transitiveAttributeHelpersGenerator

  @Autowired
  AttributeMethodsFactory(TransitiveAttributeHelpersGenerator transitiveAttributeHelpersGenerator) {
    this.transitiveAttributeHelpersGenerator = transitiveAttributeHelpersGenerator
  }

  List<Method> createMethods(Attribute attribute) {
    List<Method> methods = []
    methods.with {
      add(createGetter(attribute))
      add(createSetter(attribute))
      if (attribute.valueType instanceof ListType) {
        add(createAddValue(attribute))
        add(createRemoveValue(attribute))
      }
    }
    generateSpecificMethods(attribute, methods)
    return methods
  }

  private void generateSpecificMethods(Attribute attribute, ArrayList<Method> methods) {
    if (TRANSITIVE in attribute.characteristics && attribute.valueType instanceof ListType) {
      methods.add(transitiveAttributeHelpersGenerator.generateGetterHelperMethod(attribute))
    }
    if (attribute.isInverseAttribute()) {
      methods.add(new DirectSetter.Builder(attribute).buildMethod())
    }
  }

  private static Getter createGetter(Attribute attribute) {
    return new Getter.Builder(attribute).buildMethod()
  }

  private static Setter createSetter(Attribute attribute) {
    return new Setter.Builder(attribute).buildMethod()
  }

  private static AddValueMethod createAddValue(Attribute attribute) {
    return new AddValueMethod.Builder(attribute).buildMethod()
  }

  private static RemoveValueMethod createRemoveValue(Attribute attribute) {
    return new RemoveValueMethod.Builder(attribute).buildMethod()
  }
}
