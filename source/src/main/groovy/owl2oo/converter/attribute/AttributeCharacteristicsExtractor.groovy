package owl2oo.converter.attribute

import owl2oo.generator.model.attribute.AttributeCharacteristic
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom
import org.springframework.stereotype.Component

import static owl2oo.generator.model.attribute.AttributeCharacteristic.ASYMMETRIC
import static owl2oo.generator.model.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2oo.generator.model.attribute.AttributeCharacteristic.IRREFLEXIVE
import static owl2oo.generator.model.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2oo.generator.model.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2oo.generator.model.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class AttributeCharacteristicsExtractor {

  private static Map<AttributeCharacteristic, AxiomsExtractor> CHARACTERISTIC_TO_OBJECT_PROPERTY_AXIOMS_EXTRACTOR = [
      (SYMMETRIC)         : AttributeCharacteristicsExtractor.&extractSymmetricPropertyAxioms,
      (ASYMMETRIC)        : AttributeCharacteristicsExtractor.&extractAsymmetricPropertyAxioms,
      (TRANSITIVE)        : AttributeCharacteristicsExtractor.&extractTransitivePropertyAxioms,
      (FUNCTIONAL)        : AttributeCharacteristicsExtractor.&extractFunctionalPropertyAxioms,
      (INVERSE_FUNCTIONAL): AttributeCharacteristicsExtractor.&extractInverseFunctionalPropertyAxioms,
      (REFLEXIVE)         : AttributeCharacteristicsExtractor.&extractReflexivePropertyAxioms,
      (IRREFLEXIVE)       : AttributeCharacteristicsExtractor.&extractIrreflexivePropertyAxioms
  ].collectEntries { key, value -> [(key): value as AxiomsExtractor] } as Map<AttributeCharacteristic, AxiomsExtractor>

  List<AttributeCharacteristic> extract(OWLObjectProperty property, OWLOntology ontology) {
    return CHARACTERISTIC_TO_OBJECT_PROPERTY_AXIOMS_EXTRACTOR
        .findAll { characteristic, extractor -> extractor.hasAnyAxioms(property, ontology) }
        .collect { characteristic, extractor -> characteristic }
  }

  List<AttributeCharacteristic> extract(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0 ? [FUNCTIONAL] : []
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractSymmetricPropertyAxioms(OWLObjectProperty property,
                                                                                      OWLOntology ontology) {
    return ontology.symmetricObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractAsymmetricPropertyAxioms(OWLObjectProperty property,
                                                                                       OWLOntology ontology) {
    return ontology.asymmetricObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractTransitivePropertyAxioms(OWLObjectProperty property,
                                                                                       OWLOntology ontology) {
    return ontology.transitiveObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractFunctionalPropertyAxioms(OWLObjectProperty property,
                                                                                       OWLOntology ontology) {
    return ontology.functionalObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractInverseFunctionalPropertyAxioms(OWLObjectProperty property,
                                                                                              OWLOntology ontology) {
    return ontology.inverseFunctionalObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractReflexivePropertyAxioms(OWLObjectProperty property,
                                                                                      OWLOntology ontology) {
    return ontology.reflexiveObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractIrreflexivePropertyAxioms(OWLObjectProperty property,
                                                                                        OWLOntology ontology) {
    return ontology.irreflexiveObjectPropertyAxioms(property).findAll()
  }

  private trait AxiomsExtractor {
    abstract List<OWLObjectPropertyAxiom> extractAxioms(OWLObjectProperty property, OWLOntology ontology)

    boolean hasAnyAxioms(OWLObjectProperty property, OWLOntology ontology) {
      return !extractAxioms(property, ontology).isEmpty()
    }
  }
}
