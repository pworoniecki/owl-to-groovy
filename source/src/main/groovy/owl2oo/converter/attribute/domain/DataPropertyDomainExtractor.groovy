package owl2oo.converter.attribute.domain

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.converter.attribute.PredefinedTypes
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import owl2oo.utils.OptionalTransfomer

@Component
class DataPropertyDomainExtractor {

  private String generatedClassesPackage
  private PredefinedTypes predefinedTypes
  private PropertyDirectDomainExtractor directDomainExtractor

  @Autowired
  DataPropertyDomainExtractor(ApplicationConfiguration applicationConfiguration, PredefinedTypes predefinedTypes,
                              PropertyDirectDomainExtractor directDomainExtractor) {
    this.generatedClassesPackage = applicationConfiguration.generator.generatedClasses.modelFullPackage
    this.predefinedTypes = predefinedTypes
    this.directDomainExtractor = directDomainExtractor
  }

  Type extract(OWLDataProperty property, OWLOntology ontology) {
    return OptionalTransfomer.of(directDomainExtractor.extractSingleDirectDomain(property, ontology))
        .or { extractDomainOfEquivalentProperty(property, ontology) }
        .or { extractDomainOfSuperProperty(property, ontology) }
        .getResult()
        .map { convertToType(it) }
        .orElse(predefinedTypes.thingTraitListType)
  }

  private Optional<OWLClassExpression> extractDomainOfEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLDataProperty> findEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    def equivalentPropertyAxiom =
        ontology.equivalentDataPropertiesAxioms(property).find() as OWLEquivalentDataPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLDataProperty)
  }

  private Optional<OWLClassExpression> extractDomainOfSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLDataProperty> findSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    def subPropertyAxiom = ontology.dataSubPropertyAxiomsForSubProperty(property).find() as OWLSubDataPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLDataProperty)
  }

  private Type convertToType(OWLClass owlClass) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { throw new MissingIRIRemainderException("Missing class name in ontology for: $owlClass") }
    return new ListType(new SimpleType(generatedClassesPackage, className))
  }
}
