package owl2oo.converter.attribute.domain

import owl2oo.utils.Logger
import org.semanticweb.owlapi.model.AxiomType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PropertyDirectDomainExtractor {

  private Logger logger

  @Autowired
  PropertyDirectDomainExtractor(Logger logger) {
    this.logger = logger
  }

  Optional<OWLClass> extractSingleDirectDomain(OWLObjectProperty property, OWLOntology ontology) {
    def domainDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.OBJECT_PROPERTY_DOMAIN }
        .collect { (it as OWLObjectPropertyDomainAxiom).domain } as List<OWLClassExpression>
    return extractDomain(domainDeclarations, property)
  }

  private Optional<OWLClass> extractDomain(List<OWLClassExpression> domainDeclarations, OWLProperty property) {
    if (domainDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (domainDeclarations.size() > 1) {
      logger.warn("More than 1 domain declarations found for property $property but only 1 is supported at the time. " +
          "Domain will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found domain declarations: $domainDeclarations")
      return Optional.empty()
    }
    if (domainDeclarations.first() && !(domainDeclarations.first() instanceof OWLClass)) {
      logger.warn("Unsupported type of domain declaration found for property $property. " +
          "Domain will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found domain declaration: ${domainDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(domainDeclarations.first() as OWLClass)
  }

  Optional<OWLClass> extractSingleDirectDomain(OWLDataProperty property, OWLOntology ontology) {
    def domainDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.DATA_PROPERTY_DOMAIN }
        .collect { (it as OWLDataPropertyDomainAxiom).domain } as List<OWLClassExpression>
    return extractDomain(domainDeclarations, property)
  }
}
