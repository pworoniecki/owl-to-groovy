package owl2oo.converter.attribute.domain

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.generator.model.type.Type

@Component
class PropertyDomainExtractor {

  private ObjectPropertyDomainExtractor objectPropertyDomainExtractor
  private DataPropertyDomainExtractor dataPropertyDomainExtractor

  @Autowired
  PropertyDomainExtractor(ObjectPropertyDomainExtractor objectPropertyDomainExtractor, DataPropertyDomainExtractor dataPropertyDomainExtractor) {
    this.objectPropertyDomainExtractor = objectPropertyDomainExtractor
    this.dataPropertyDomainExtractor = dataPropertyDomainExtractor
  }

  Type extractDomain(OWLProperty property, OWLOntology ontology) {
    if (property instanceof OWLObjectProperty) {
      return objectPropertyDomainExtractor.extract(property as OWLObjectProperty, ontology)
    }
    if (property instanceof OWLDataProperty) {
      return dataPropertyDomainExtractor.extract(property as OWLDataProperty, ontology)
    }
    throw new IllegalArgumentException("Cannot extract domain of property '$property' because it has unsupported type")
  }
}
