package owl2oo.converter.attribute.domain

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.converter.attribute.PredefinedTypes
import owl2oo.converter.attribute.range.PropertyDirectRangeExtractor
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.type.ListType
import owl2oo.generator.model.type.SimpleType
import owl2oo.generator.model.type.Type
import owl2oo.utils.OptionalTransfomer

@Component
class ObjectPropertyDomainExtractor {

  private String generatedClassesPackage
  private PredefinedTypes predefinedTypes
  private PropertyDirectDomainExtractor directDomainExtractor
  private PropertyDirectRangeExtractor directRangeExtractor

  @Autowired
  ObjectPropertyDomainExtractor(ApplicationConfiguration applicationConfiguration, PredefinedTypes predefinedTypes,
                                PropertyDirectDomainExtractor directDomainExtractor,
                                PropertyDirectRangeExtractor directRangeExtractor) {
    this.generatedClassesPackage = applicationConfiguration.generator.generatedClasses.modelFullPackage
    this.predefinedTypes = predefinedTypes
    this.directDomainExtractor = directDomainExtractor
    this.directRangeExtractor = directRangeExtractor
  }

  Type extract(OWLObjectProperty property, OWLOntology ontology) {
    return OptionalTransfomer.of(directDomainExtractor.extractSingleDirectDomain(property, ontology))
        .or { extractDomainOfEquivalentProperty(property, ontology) }
        .or { extractDomainOfSuperProperty(property, ontology) }
        .or { extractRangeOfInverseProperty(property, ontology) }
        .getResult()
        .map { convertToType(it) }
        .orElse(predefinedTypes.thingTraitListType)
  }

  private Optional<OWLClassExpression> extractDomainOfEquivalentProperty(OWLObjectProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findEquivalentProperty(OWLObjectProperty property, OWLOntology ontology) {
    def equivalentPropertyAxiom =
        ontology.equivalentObjectPropertiesAxioms(property).find() as OWLEquivalentObjectPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLObjectProperty)
  }

  private Optional<OWLClassExpression> extractDomainOfSuperProperty(OWLObjectProperty property, OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findSuperProperty(OWLObjectProperty property, OWLOntology ontology) {
    def subPropertyAxiom = ontology.objectSubPropertyAxiomsForSubProperty(property).find() as OWLSubObjectPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLObjectProperty)
  }

  private Optional<OWLClassExpression> extractRangeOfInverseProperty(OWLObjectProperty property,
                                                                     OWLOntology ontology) {
    return findInverseProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findInverseProperty(OWLObjectProperty property, OWLOntology ontology) {
    def inversePropertyAxiom = ontology.inverseObjectPropertyAxioms(property).find() as OWLInverseObjectPropertiesAxiom
    return Optional.ofNullable(inversePropertyAxiom?.properties()?.find { !it.is(property) } as OWLObjectProperty)
  }

  private Type convertToType(OWLClass owlClass) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { throw new MissingIRIRemainderException("Missing class name in ontology for: $owlClass") }
    return new ListType(new SimpleType(generatedClassesPackage, className))
  }
}
