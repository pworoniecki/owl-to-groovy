package owl2oo.converter

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.exception.MissingIRIRemainderException
import owl2oo.generator.model.Class
import owl2oo.generator.model.Constructor
import owl2oo.generator.model.Trait
import owl2oo.generator.model.attribute.Attribute
import owl2oo.generator.model.method.Method

@Component
class ClassesExtractor {

  private String classPackage
  private ConstructorsExtractor constructorsExtractor

  @Autowired
  ClassesExtractor(ApplicationConfiguration configuration, ConstructorsExtractor constructorsExtractor) {
    classPackage = configuration.generator.generatedClasses.modelFullPackage
    this.constructorsExtractor = constructorsExtractor
  }

  List<Class> extract(OWLOntology ontology, Map<OWLClass, List<Trait>> traits) {
    return ontology.classesInSignature().findAll().collect { extractClass(it as OWLClass, traits) }
  }

  private Class extractClass(OWLClass owlClass, Map<OWLClass, List<Trait>> traits) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Missing OWL class name: $owlClass") }
    Class.Builder classBuilder = new Class.Builder(classPackage, className)
        .withAnnotations([])
        .withAttributes(extractAttributes())
        .withConstructors(extractConstructors())
        .withImplementedTraits(traits[owlClass])
        .withMethods(extractMethods())
    Optional<Class> inheritedClass = extractInheritedClass()
    return (inheritedClass.isPresent() ? classBuilder.withInheritedClass(inheritedClass.get()) : classBuilder).buildClass()
  }

  private static List<Attribute> extractAttributes() {
    return []
  }

  List<Constructor> extractConstructors() {
    return constructorsExtractor.extract()
  }

  private static Optional<Class> extractInheritedClass() {
    return Optional.empty()
  }

  private static List<Method> extractMethods() {
    return []
  }
}
