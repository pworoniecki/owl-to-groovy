package owl2oo.sourcecodegenerator

interface SourceCode {

  String getName()
  String getSourceCode()
}