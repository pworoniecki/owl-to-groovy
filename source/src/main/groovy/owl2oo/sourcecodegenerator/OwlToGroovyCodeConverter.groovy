package owl2oo.sourcecodegenerator

import owl2oo.converter.ObjectOrientedCodeModel
import owl2oo.converter.OntologyToCodeModelConverter
import owl2oo.generator.ClassCodeGenerator
import owl2oo.generator.TraitCodeGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class OwlToGroovyCodeConverter {

  private OntologyToCodeModelConverter ontologyToCodeModelConverter
  private ClassCodeGenerator classCodeGenerator
  private TraitCodeGenerator traitCodeGenerator

  @Autowired
  OwlToGroovyCodeConverter(OntologyToCodeModelConverter ontologyToCodeModelConverter,
                           ClassCodeGenerator classCodeGenerator, TraitCodeGenerator traitCodeGenerator) {
    this.traitCodeGenerator = traitCodeGenerator
    this.classCodeGenerator = classCodeGenerator
    this.ontologyToCodeModelConverter = ontologyToCodeModelConverter
  }

  GroovySourceCode convert(File owlOntologyFile) {
    ObjectOrientedCodeModel codeModel = ontologyToCodeModelConverter.convert(owlOntologyFile.newInputStream())
    List<ClassSourceCode> classSourceCodes = generateClassesSourceCode(codeModel)
    List<TraitSourceCode> traitSourceCodes = generateTraitsSourceCode(codeModel)
    return new GroovySourceCode(classSourceCodes, traitSourceCodes)
  }

  private List<ClassSourceCode> generateClassesSourceCode(ObjectOrientedCodeModel codeModel) {
    return codeModel.classes.collect {
      new ClassSourceCode(it, classCodeGenerator.generateCode(it))
    }
  }

  private List<TraitSourceCode> generateTraitsSourceCode(ObjectOrientedCodeModel codeModel) {
    return codeModel.traits.collect {
      new TraitSourceCode(it, traitCodeGenerator.generateCode(it))
    }
  }
}
