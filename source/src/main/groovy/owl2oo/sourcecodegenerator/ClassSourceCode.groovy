package owl2oo.sourcecodegenerator

import owl2oo.generator.model.Class

class ClassSourceCode implements SourceCode {

  private Class classModel
  private String sourceCode

  ClassSourceCode(Class classModel, String sourceCode) {
    this.classModel = classModel
    this.sourceCode = sourceCode
  }

  Class getClassModel() {
    return classModel
  }

  @Override
  String getName() {
    return classModel.name
  }

  @Override
  String getSourceCode() {
    return sourceCode
  }
}
