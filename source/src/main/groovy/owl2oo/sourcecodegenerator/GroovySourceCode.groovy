package owl2oo.sourcecodegenerator

class GroovySourceCode {

  private List<ClassSourceCode> classes
  private List<TraitSourceCode> traits

  GroovySourceCode(List<ClassSourceCode> classes, List<TraitSourceCode> traits) {
    this.classes = classes
    this.traits = traits
  }

  List<ClassSourceCode> getClasses() {
    return classes
  }

  List<TraitSourceCode> getTraits() {
    return traits
  }
}
