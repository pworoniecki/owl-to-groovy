package owl2oo.sourcecodegenerator

import owl2oo.generator.model.Trait

class TraitSourceCode implements SourceCode {

  private Trait traitModel
  private String sourceCode

  TraitSourceCode(Trait traitModel, String sourceCode) {
    this.traitModel = traitModel
    this.sourceCode = sourceCode
  }

  Trait getTraitModel() {
    return traitModel
  }

  @Override
  String getName() {
    return traitModel.name
  }

  @Override
  String getSourceCode() {
    return sourceCode
  }
}
