package owl2oo.sourcecodegenerator

import owl2oo.configuration.model.ApplicationConfiguration
import owl2oo.configuration.model.GeneratedClassesConfiguration
import owl2oo.exception.FileAccessException
import owl2oo.resultcodebase.ApplicationInitializer
import owl2oo.utils.GroovyCodeFormatter
import net.lingala.zip4j.core.ZipFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class GroovyCodeFilesGenerator {

  private static final String CODE_BASE_ZIP_FILE_NAME = 'generatedApplicationBaseCode.zip'
  private static final String GROOVY_SOURCE_CODE_RELATIVE_PATH = 'src/main/groovy'
  private static final String GROOVY_SOURCE_FILE_EXTENSION = 'groovy'
  private static final String BUILD_GRADLE_SOURCE_FILE_NAME = 'generatedApplicationBuild.gradle'
  private static final String BUILD_GRADLE_DESTINATION_FILE_NAME = 'build.gradle'
  private static final String SETTINGS_GRADLE_SOURCE_FILE_NAME = 'generatedApplicationSettings.gradle'
  private static final String SETTINGS_GRADLE_DESTINATION_FILE_NAME = 'settings.gradle'

  private String generatedCodeBasePackage
  private String generatedCodeRelativePath
  private String generatedClassesRelativePath
  private GroovyCodeFormatter codeFormatter

  @Autowired
  GroovyCodeFilesGenerator(ApplicationConfiguration applicationConfiguration, GroovyCodeFormatter codeFormatter) {
    GeneratedClassesConfiguration generatedClassConfiguration = applicationConfiguration.generator.generatedClasses
    this.generatedCodeBasePackage = generatedClassConfiguration.basePackage
    this.generatedCodeRelativePath = generatedClassConfiguration.basePackage.replace('.', File.separator)
    this.generatedClassesRelativePath = generatedClassConfiguration.modelFullPackage.replace('.', File.separator)
    this.codeFormatter = codeFormatter
  }

  void generate(GroovySourceCode sourceCode, File destinationDirectory) throws FileAccessException {
    String destinationDirectoryPath = destinationDirectory.absolutePath
    createDirectory(destinationDirectoryPath)
    generateBaseCodeFiles(destinationDirectory)
    generateSourceCode(sourceCode, destinationDirectoryPath)
  }

  private void generateSourceCode(GroovySourceCode sourceCode, String destinationDirectoryPath) {
    File generatedClassesDestinationDirectory = createGeneratedClassesDestinationDirectory(destinationDirectoryPath)
    generateSourceCodeFiles(sourceCode.classes, generatedClassesDestinationDirectory)
    generateSourceCodeFiles(sourceCode.traits, generatedClassesDestinationDirectory)
  }

  private void generateBaseCodeFiles(File destinationDirectory) {
    extractCodeBaseFilesFromZip(destinationDirectory)
    copyGradleFiles(destinationDirectory)
  }

  private static File createDirectory(String destinationDirectoryPath) {
    def destinationDirectory = new File(destinationDirectoryPath)
    if (!destinationDirectory.exists() && !destinationDirectory.mkdirs()) {
      throw new FileAccessException("Cannot create directory: $destinationDirectoryPath")
    }
    return destinationDirectory
  }

  private void extractCodeBaseFilesFromZip(File destinationRootDirectory) {
    File generatedCodeDestination = createGeneratedCodeDestinationDirectory(destinationRootDirectory)
    copyCodeBaseZipFile(generatedCodeDestination)
    def zipFile = new ZipFile("$generatedCodeDestination/$CODE_BASE_ZIP_FILE_NAME")
    zipFile.extractAll(generatedCodeDestination.absolutePath)
    zipFile.file.delete()
    adjustPackages(generatedCodeDestination)
  }

  private File createGeneratedCodeDestinationDirectory(File destinationRootDirectory) {
    return createDirectory("$destinationRootDirectory/$GROOVY_SOURCE_CODE_RELATIVE_PATH/$generatedCodeRelativePath")
  }

  private void copyCodeBaseZipFile(File destinationDirectory) {
    def destinationFile = new File(destinationDirectory, CODE_BASE_ZIP_FILE_NAME)
    copyFile(CODE_BASE_ZIP_FILE_NAME, destinationFile)
  }

  private void copyFile(String resourceFileName, File destinationFile) {
    def fileStream = getClass().getResourceAsStream("/$resourceFileName")
    if (!destinationFile.exists() && !destinationFile.createNewFile()) {
      throw new FileAccessException("Cannot create file: $destinationFile")
    }
    destinationFile.bytes = fileStream.bytes
  }

  private void adjustPackages(File codeFilesDirectory) {
    codeFilesDirectory.eachFileRecurse {
      file ->
        if (file.isFile()) {
          file.text = file.text.replace(ApplicationInitializer.package.name, generatedCodeBasePackage)
        }
    }
  }

  private void copyGradleFiles(File destinationDirectory) {
    copyFile(BUILD_GRADLE_SOURCE_FILE_NAME, new File(destinationDirectory, BUILD_GRADLE_DESTINATION_FILE_NAME))
    copyFile(SETTINGS_GRADLE_SOURCE_FILE_NAME,
        new File(destinationDirectory, SETTINGS_GRADLE_DESTINATION_FILE_NAME))
  }

  private File createGeneratedClassesDestinationDirectory(String destinationDirectory) {
    return createDirectory("$destinationDirectory/$GROOVY_SOURCE_CODE_RELATIVE_PATH/$generatedClassesRelativePath")
  }

  private void generateSourceCodeFiles(List<? extends SourceCode> classes, File generatedCodeDestinationDirectory) {
    classes.each {
      def fileName = "$it.name.$GROOVY_SOURCE_FILE_EXTENSION"
      String formattedSourceCode = codeFormatter.format(it.sourceCode)
      createFileWithContent(generatedCodeDestinationDirectory, fileName, formattedSourceCode)
    }
  }

  private static void createFileWithContent(File fileDirectory, String fileName, String sourceCode) {
    def createdFile = new File(fileDirectory, fileName)
    if (!createdFile.exists() && !createdFile.createNewFile()) {
      throw new FileAccessException("Cannot create file: $createdFile")
    }
    createdFile.text = sourceCode
  }
}
