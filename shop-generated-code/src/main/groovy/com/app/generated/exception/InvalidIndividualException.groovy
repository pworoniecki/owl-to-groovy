package com.app.generated.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class InvalidIndividualException extends Exception {
}
