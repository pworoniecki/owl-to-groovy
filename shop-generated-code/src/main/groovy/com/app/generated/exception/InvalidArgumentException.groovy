package com.app.generated.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class InvalidArgumentException extends Exception {
}
