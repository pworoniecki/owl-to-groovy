package com.app.generated.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class IndividualDuplicationException extends Exception {
}
