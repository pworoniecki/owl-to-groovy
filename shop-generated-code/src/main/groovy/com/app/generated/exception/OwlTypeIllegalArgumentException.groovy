package com.app.generated.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class OwlTypeIllegalArgumentException extends Exception {

  OwlTypeIllegalArgumentException(Object invalidValue, com.app.generated.owlSimpleType.OwlType owlType, String reason) {
    super("Provided value '$invalidValue' is invalid value of type 'xsd:${owlType.xsdType}'. Reason: $reason")
  }
}
