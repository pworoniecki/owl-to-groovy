package com.app.generated.validator

import com.app.generated.exception.IllegalValuesException
import com.app.generated.exception.InvalidArgumentException

class OneOfValuesValidator {

  static void validateValue(Object value, List<Object> allowedValues) {
    validateValues([value], allowedValues)
  }

  static void validateValues(List<Object> values, List<Object> allowedValues) {
    if (!allowedValues) {
      throw new InvalidArgumentException('Missing allowed values')
    }
    values.each {
      if (!allowedValues.contains(it)) {
        throw new IllegalValuesException("Value '$it' is not allowed")
      }
    }
  }
}
