package com.app.generated.individual

import org.codehaus.groovy.control.CompilerConfiguration

class IndividualParser {

  private IndividualParser() {}

  static IndividualWrapper parse(String individualDsl) {
    CompilerConfiguration configuration = new CompilerConfiguration(scriptBaseClass: DelegatingScript.name)
    DelegatingScript script = new GroovyShell(configuration).parse(individualDsl)

    IndividualWrapper individual = new IndividualWrapper()
    script.setDelegate(individual)
    script.run()
    IndividualRepository.addIndividual(individual)
    return individual
  }
}
