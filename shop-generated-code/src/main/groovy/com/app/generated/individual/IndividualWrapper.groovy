package com.app.generated.individual

import com.app.generated.exception.InvalidIndividualException

class IndividualWrapper {
  private String name
  private Optional<Class> clazz = Optional.empty()
  private Closure content
  private Object contentInstance

  IndividualWrapper() {}

  IndividualWrapper(String name, Object individualClassInstance) {
    this.name = name
    this.clazz = Optional.of(individualClassInstance.class)
    this.content = {}
    this.contentInstance = individualClassInstance
  }

  void name(String name) {
    this.name = name
  }

  void individualClass(Class clazz) {
    this.clazz = Optional.of(clazz)
  }

  void content(Closure content) {
    this.content = content
  }

  String getName() {
    return this.name
  }

  Object getContentInstance() {
    if (!this.contentInstance) {
      createContentInstance()
    }
    return this.contentInstance
  }

  private void createContentInstance() throws InvalidIndividualException {
    assertNonEmptyClass()
    Object individualInstance = clazz.get().newInstance()
    content.delegate = individualInstance
    content.resolveStrategy = Closure.DELEGATE_ONLY
    try {
      content()
    } catch (MissingMethodException) {
      throw new InvalidIndividualException("Individual is not instance of '${clazz.get()}' class.")
    }
    this.contentInstance = individualInstance
  }

  private void assertNonEmptyClass() throws InvalidIndividualException {
    if (!clazz.isPresent()) {
      throw new InvalidIndividualException("Missing individual’s class name.")
    }
  }

  boolean hasClassAssigned() {
    return this.clazz.isPresent()
  }
}
