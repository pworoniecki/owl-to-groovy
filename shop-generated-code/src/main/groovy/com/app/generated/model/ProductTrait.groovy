package com.app.generated.model

import com.app.generated.exception.*

trait ProductTrait implements ThingTrait {
  
  private static Set<ProductTrait> allInstances = [].toSet()
  
  private List<ProductTrait> similarToProducts = [this]
  private List<CategoryTrait> belongsToCategory = []
  private List<ProductTrait> cheaperThan = []
  private List<ProducerTrait> producedBy = []
  private BigDecimal price
  private String productName
  private List<BigDecimal> previousPrices = []
  
  static Product createInstance() {
    def classInstance = new Product()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(ProductTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
  }
  
  static boolean removeInstance(ProductTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(ProductTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<ProductTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  List<ProductTrait> getSimilarToProducts() {
    return this.similarToProducts
  }
  
  void setSimilarToProducts(List<ProductTrait> similarToProducts) {
    if (this.similarToProducts == similarToProducts) { return }
    if (!similarToProducts.contains(this)) {
      throw new ReflexiveAttributeException("Cannot set $similarToProducts as it does not contain 'this' instance - it would break reflexive attribute rule")
    }
    def oldSimilarToProducts = this.similarToProducts ?: []
    def newSimilarToProducts = similarToProducts ?: []
    def removedSimilarToProducts = oldSimilarToProducts - newSimilarToProducts
    def addedSimilarToProducts = newSimilarToProducts - oldSimilarToProducts
    removedSimilarToProducts.each { removeSimilarToProducts(it) }
    addedSimilarToProducts.each { addSimilarToProducts(it) }
  }
  
  void addSimilarToProducts(ProductTrait similarToProducts) {
    if (similarToProducts == null || this.similarToProducts.contains(similarToProducts)) { return }
    this.similarToProducts.add(similarToProducts)
    similarToProducts.addSimilarToProducts(this)
  }
  
  void removeSimilarToProducts(ProductTrait similarToProducts) {
    if (similarToProducts == null || !this.getSimilarToProducts().contains(similarToProducts)) { return }
    if (similarToProducts == this) {
      throw new ReflexiveAttributeException("Cannot remove value 'this' - it would break reflexivity of attribute")
    }
    this.similarToProducts.remove(similarToProducts)
    similarToProducts.removeSimilarToProducts(this)
  }
  
  List<CategoryTrait> getBelongsToCategory() {
    return this.belongsToCategory
  }
  
  void setBelongsToCategory(List<CategoryTrait> belongsToCategory) {
    if (this.belongsToCategory == belongsToCategory) { return }
    def oldBelongsToCategory = this.belongsToCategory ?: []
    def newBelongsToCategory = belongsToCategory ?: []
    def removedBelongsToCategory = oldBelongsToCategory - newBelongsToCategory
    def addedBelongsToCategory = newBelongsToCategory - oldBelongsToCategory
    removedBelongsToCategory.each {
      it.hasProducts.remove(this)
    }
    addedBelongsToCategory.each {
      it.setHasProductsDirectly(it.hasProducts ? it.hasProducts + this : [this])
    }
    this.belongsToCategory = belongsToCategory
  }
  
  void addBelongsToCategory(CategoryTrait belongsToCategory) {
    if (belongsToCategory == null || this.belongsToCategory.contains(belongsToCategory)) { return }
    this.belongsToCategory.add(belongsToCategory)
    belongsToCategory.addHasProducts(this)
  }
  
  void removeBelongsToCategory(CategoryTrait belongsToCategory) {
    if (belongsToCategory == null || !this.getBelongsToCategory().contains(belongsToCategory)) { return }
    this.belongsToCategory.remove(belongsToCategory)
    belongsToCategory.removeHasProducts(this)
  }
  
  void setBelongsToCategoryDirectly(List<CategoryTrait> belongsToCategory) {
    this.belongsToCategory = belongsToCategory
  }
  
  List<ProductTrait> getCheaperThan() {
    def result = []
    _fillTransitiveCheaperThan(result)
    return result
  }
  
  void setCheaperThan(List<ProductTrait> cheaperThan) {
    if (this.cheaperThan == cheaperThan) { return }
    this.cheaperThan = cheaperThan
  }
  
  void addCheaperThan(ProductTrait cheaperThan) {
    if (cheaperThan == null || this.cheaperThan.contains(cheaperThan)) { return }
    this.cheaperThan.add(cheaperThan)
  }
  
  void removeCheaperThan(ProductTrait cheaperThan) {
    if (cheaperThan == null || !this.getCheaperThan().contains(cheaperThan)) { return }
    if (!this.cheaperThan.contains(cheaperThan)) {
      throw new TransitiveAttributeException("Cannot remove $cheaperThan because it would break transitivity rule")
    }
    this.cheaperThan.remove(cheaperThan)
  }
  
  void _fillTransitiveCheaperThan(List<ProductTrait> result) {
    (this.cheaperThan - result).each {
      if (!result.contains(it)) {
        result.add(it)
        it._fillTransitiveCheaperThan(result)
      }
    }
  }
  
  List<ProducerTrait> getProducedBy() {
    return this.producedBy
  }
  
  void setProducedBy(List<ProducerTrait> producedBy) {
    if (this.producedBy == producedBy) { return }
    this.producedBy = producedBy
  }
  
  void addProducedBy(ProducerTrait producedBy) {
    if (producedBy == null || this.producedBy.contains(producedBy)) { return }
    this.producedBy.add(producedBy)
  }
  
  void removeProducedBy(ProducerTrait producedBy) {
    if (producedBy == null || !this.getProducedBy().contains(producedBy)) { return }
    this.producedBy.remove(producedBy)
  }
  
  BigDecimal getPrice() {
    return this.price
  }
  
  void setPrice(BigDecimal price) {
    if (this.price == price) { return }
    this.price = price
  }
  
  String getProductName() {
    return this.productName
  }
  
  void setProductName(String productName) {
    if (this.productName == productName) { return }
    this.productName = productName
  }
  
  List<BigDecimal> getPreviousPrices() {
    return this.previousPrices
  }
  
  void setPreviousPrices(List<BigDecimal> previousPrices) {
    if (this.previousPrices == previousPrices) { return }
    this.previousPrices = previousPrices
  }
  
  void addPreviousPrices(BigDecimal previousPrices) {
    if (previousPrices == null || this.previousPrices.contains(previousPrices)) { return }
    this.previousPrices.add(previousPrices)
  }
  
  void removePreviousPrices(BigDecimal previousPrices) {
    if (previousPrices == null || !this.getPreviousPrices().contains(previousPrices)) { return }
    this.previousPrices.remove(previousPrices)
  }
  
}
