package com.app.generated.model

import com.app.generated.exception.*

trait CategoryTrait implements ThingTrait {
  
  private static Set<CategorySectionEquivalentTrait> allInstances = [].toSet()
  
  private List<ProductTrait> hasProducts = []
  private String categoryName
  
  static Category createInstance() {
    def classInstance = new Category()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    Section.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(CategorySectionEquivalentTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    Section.addInstance(classInstance)
  }
  
  static boolean removeInstance(CategorySectionEquivalentTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    Section.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(CategorySectionEquivalentTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<CategorySectionEquivalentTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  List<ProductTrait> getHasProducts() {
    return this.hasProducts
  }
  
  void setHasProducts(List<ProductTrait> hasProducts) {
    if (this.hasProducts == hasProducts) { return }
    def oldHasProducts = this.hasProducts ?: []
    def newHasProducts = hasProducts ?: []
    def removedHasProducts = oldHasProducts - newHasProducts
    def addedHasProducts = newHasProducts - oldHasProducts
    removedHasProducts.each {
      it.belongsToCategory.remove(this)
    }
    addedHasProducts.each {
      it.setBelongsToCategoryDirectly(it.belongsToCategory ? it.belongsToCategory + this : [this])
    }
    this.hasProducts = hasProducts
  }
  
  void addHasProducts(ProductTrait hasProducts) {
    if (hasProducts == null || this.hasProducts.contains(hasProducts)) { return }
    this.hasProducts.add(hasProducts)
    hasProducts.addBelongsToCategory(this)
  }
  
  void removeHasProducts(ProductTrait hasProducts) {
    if (hasProducts == null || !this.getHasProducts().contains(hasProducts)) { return }
    this.hasProducts.remove(hasProducts)
    hasProducts.removeBelongsToCategory(this)
  }
  
  void setHasProductsDirectly(List<ProductTrait> hasProducts) {
    this.hasProducts = hasProducts
  }
  
  String getCategoryName() {
    return this.categoryName
  }
  
  void setCategoryName(String categoryName) {
    if (this.categoryName == categoryName) { return }
    this.categoryName = categoryName
  }
  
}
