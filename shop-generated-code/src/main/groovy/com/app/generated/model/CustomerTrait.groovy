package com.app.generated.model

import com.app.generated.exception.*
import com.app.generated.owlSimpleType.PositiveInteger

trait CustomerTrait implements ThingTrait {
  
  private static Set<CustomerTrait> allInstances = [].toSet()
  
  private CustomerTrait recommendedBy
  private List<OrderTrait> hasOrders = []
  private String lastName
  private String firstName
  private PositiveInteger customerId
  
  static Customer createInstance() {
    def classInstance = new Customer()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(CustomerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
  }
  
  static boolean removeInstance(CustomerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(CustomerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<CustomerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  CustomerTrait getRecommendedBy() {
    return this.recommendedBy
  }
  
  void setRecommendedBy(CustomerTrait recommendedBy) {
    if (this.recommendedBy == recommendedBy) { return }
    if (recommendedBy == this) {
      throw new IrreflexiveAttributeException("Cannot set 'this' instance - it would break irreflexive attribute rule")
    }
    if (recommendedBy.recommendedBy == this) {
      throw new AsymmetricAttributeException("Cannot set value '$recommendedBy' - it would break asymmetric attribute rule")
    }
    this.recommendedBy = recommendedBy
  }
  
  List<OrderTrait> getHasOrders() {
    return this.hasOrders
  }
  
  void setHasOrders(List<OrderTrait> hasOrders) {
    if (this.hasOrders == hasOrders) { return }
    def hasOrdersOfOtherInstances = (allInstances - this)*.hasOrders.flatten()
    def commonValues = hasOrders.intersect(hasOrdersOfOtherInstances)
    if (!commonValues.isEmpty()) {
      throw new InverseFunctionalAttributeException("Cannot set $commonValues because it would break inverse functional attribute rule - these values are already set in other class instances.")
    }
    def oldHasOrders = this.hasOrders ?: []
    def newHasOrders = hasOrders ?: []
    def removedHasOrders = oldHasOrders - newHasOrders
    def addedHasOrders = newHasOrders - oldHasOrders
    addedHasOrders.each {
      it.setOrderedByDirectly(this)
    }
    this.hasOrders = hasOrders
  }
  
  void addHasOrders(OrderTrait hasOrders) {
    if (hasOrders == null || this.hasOrders.contains(hasOrders)) { return }
    def hasOrdersOfOtherInstances = (allInstances - this)*.hasOrders.flatten()
    if (hasOrders in hasOrdersOfOtherInstances) {
      throw new InverseFunctionalAttributeException("Cannot add value '$hasOrders' - it would break inverse functional attribute rule")
    }
    this.hasOrders.add(hasOrders)
    hasOrders.setOrderedByDirectly(this)
  }
  
  void removeHasOrders(OrderTrait hasOrders) {
    if (hasOrders == null || !this.getHasOrders().contains(hasOrders)) { return }
    this.hasOrders.remove(hasOrders)
    hasOrders.setOrderedByDirectly(null)
  }
  
  void setHasOrdersDirectly(List<OrderTrait> hasOrders) {
    this.hasOrders = hasOrders
  }
  
  String getLastName() {
    return this.lastName
  }
  
  void setLastName(String lastName) {
    if (this.lastName == lastName) { return }
    this.lastName = lastName
  }
  
  String getFirstName() {
    return this.firstName
  }
  
  void setFirstName(String firstName) {
    if (this.firstName == firstName) { return }
    this.firstName = firstName
  }
  
  PositiveInteger getCustomerId() {
    return this.customerId
  }
  
  void setCustomerId(PositiveInteger customerId) {
    if (this.customerId == customerId) { return }
    this.customerId = customerId
  }
  
}
