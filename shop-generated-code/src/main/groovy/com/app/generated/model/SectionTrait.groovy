package com.app.generated.model

import com.app.generated.exception.*

trait SectionTrait implements ThingTrait {
  
  private static Set<CategorySectionEquivalentTrait> allInstances = [].toSet()
  
  static Section createInstance() {
    def classInstance = new Section()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    Category.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(CategorySectionEquivalentTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    Category.addInstance(classInstance)
  }
  
  static boolean removeInstance(CategorySectionEquivalentTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    Category.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(CategorySectionEquivalentTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<CategorySectionEquivalentTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
