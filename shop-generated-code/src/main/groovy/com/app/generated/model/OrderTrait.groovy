package com.app.generated.model

import com.app.generated.exception.*

trait OrderTrait implements ThingTrait {
  
  private static Set<OrderTrait> allInstances = [].toSet()
  
  private List<ProductTrait> orderedProducts = []
  private CustomerTrait orderedBy
  
  static Order createInstance() {
    def classInstance = new Order()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(OrderTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
  }
  
  static boolean removeInstance(OrderTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(OrderTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<OrderTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  List<ProductTrait> getOrderedProducts() {
    return this.orderedProducts
  }
  
  void setOrderedProducts(List<ProductTrait> orderedProducts) {
    if (this.orderedProducts == orderedProducts) { return }
    this.orderedProducts = orderedProducts
  }
  
  void addOrderedProducts(ProductTrait orderedProducts) {
    if (orderedProducts == null || this.orderedProducts.contains(orderedProducts)) { return }
    this.orderedProducts.add(orderedProducts)
  }
  
  void removeOrderedProducts(ProductTrait orderedProducts) {
    if (orderedProducts == null || !this.getOrderedProducts().contains(orderedProducts)) { return }
    this.orderedProducts.remove(orderedProducts)
  }
  
  CustomerTrait getOrderedBy() {
    return this.orderedBy
  }
  
  void setOrderedBy(CustomerTrait orderedBy) {
    if (this.orderedBy == orderedBy) { return }
    orderedBy.setHasOrdersDirectly([this])
    this.orderedBy = orderedBy
  }
  
  void setOrderedByDirectly(CustomerTrait orderedBy) {
    this.orderedBy = orderedBy
  }
  
}
