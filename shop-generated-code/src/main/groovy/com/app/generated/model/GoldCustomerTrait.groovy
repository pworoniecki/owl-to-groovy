package com.app.generated.model

import com.app.generated.exception.*

trait GoldCustomerTrait implements CustomerTrait {
  
  private static Set<GoldCustomerTrait> allInstances = [].toSet()
  
  static GoldCustomer createInstance() {
    def classInstance = new GoldCustomer()
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(GoldCustomerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
  }
  
  static boolean removeInstance(GoldCustomerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Customer.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(GoldCustomerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<GoldCustomerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
