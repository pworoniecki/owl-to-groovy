package com.app.generated.model

import com.app.generated.exception.*
import com.app.generated.validator.DisjointWith

@DisjointWith([AdultIndividualCustomerTrait, ChildIndividualCustomerTrait, CompanyCustomerTrait])
trait IndividualCustomerTrait implements CustomerTrait {
  
  static IndividualCustomer createInstance() {
    throw new UnsupportedOperationException('Method not available in union class')
  }
  
  static void addInstance(IndividualCustomerTrait classInstance) {
    throw new UnsupportedOperationException('Method not available in union class')
  }
  
  static boolean removeInstance(IndividualCustomerTrait classInstance) {
    boolean removalResult
    if(classInstance.class == AdultIndividualCustomer) {
      removalResult = AdultIndividualCustomer.removeInstance(classInstance as AdultIndividualCustomerTrait)
    }
    else if(classInstance.class == ChildIndividualCustomer) {
      removalResult = ChildIndividualCustomer.removeInstance(classInstance as ChildIndividualCustomerTrait)
    }
    else {
      throw new IllegalArgumentException('Unsupported instance type')
    }
    Customer.removeInstance(classInstance)
    return removalResult
  }
  
  static boolean containsInstance(IndividualCustomerTrait classInstance) {
    return AdultIndividualCustomer.containsInstance(classInstance as AdultIndividualCustomerTrait) || ChildIndividualCustomer.containsInstance(classInstance as ChildIndividualCustomerTrait)
  }
  
  static Set<IndividualCustomerTrait> getInstances() {
    return [] + AdultIndividualCustomer.getInstances() + ChildIndividualCustomer.getInstances()
  }
  
}
