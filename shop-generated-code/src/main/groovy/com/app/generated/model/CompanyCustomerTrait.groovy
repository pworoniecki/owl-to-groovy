package com.app.generated.model

import com.app.generated.exception.*
import com.app.generated.validator.DisjointWith

@DisjointWith([IndividualCustomerTrait])
trait CompanyCustomerTrait implements CompanyTrait, CustomerTrait {
  
  private static Set<CompanyCustomerTrait> allInstances = [].toSet()
  
  static CompanyCustomer createInstance() {
    def classInstance = new CompanyCustomer()
    allInstances.add(classInstance)
    Company.addInstance(classInstance)
    Customer.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(CompanyCustomerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Company.addInstance(classInstance)
    Customer.addInstance(classInstance)
  }
  
  static boolean removeInstance(CompanyCustomerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Company.removeInstance(classInstance)
    Customer.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(CompanyCustomerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<CompanyCustomerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
