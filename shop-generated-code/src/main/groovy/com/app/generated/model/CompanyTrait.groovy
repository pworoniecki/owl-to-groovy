package com.app.generated.model

import com.app.generated.exception.*

trait CompanyTrait implements ThingTrait {
  
  private static Set<CompanyTrait> allInstances = [].toSet()
  
  private String companyName
  
  static Company createInstance() {
    def classInstance = new Company()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(CompanyTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
  }
  
  static boolean removeInstance(CompanyTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(CompanyTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<CompanyTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  String getCompanyName() {
    return this.companyName
  }
  
  void setCompanyName(String companyName) {
    if (this.companyName == companyName) { return }
    this.companyName = companyName
  }
  
}
