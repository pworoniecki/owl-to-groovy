package com.app.generated.model

import com.app.generated.exception.*
import com.app.generated.validator.DisjointWith

@DisjointWith([IndividualCustomerTrait, ChildIndividualCustomerTrait])
trait AdultIndividualCustomerTrait implements CustomerTrait, IndividualCustomerTrait {
  
  private static Set<AdultIndividualCustomerTrait> allInstances = [].toSet()
  
  static AdultIndividualCustomer createInstance() {
    def classInstance = new AdultIndividualCustomer()
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(AdultIndividualCustomerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
  }
  
  static boolean removeInstance(AdultIndividualCustomerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Customer.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(AdultIndividualCustomerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<AdultIndividualCustomerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
