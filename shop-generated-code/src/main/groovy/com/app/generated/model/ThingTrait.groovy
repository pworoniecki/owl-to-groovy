package com.app.generated.model

import com.app.generated.exception.*

trait ThingTrait {
  
  private static Set<ThingTrait> allInstances = [].toSet()
  
  static Thing createInstance() {
    def classInstance = new Thing()
    allInstances.add(classInstance)
    return classInstance
  }
  
  static void addInstance(ThingTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
  }
  
  static boolean removeInstance(ThingTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    return true
  }
  
  static boolean containsInstance(ThingTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<ThingTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
