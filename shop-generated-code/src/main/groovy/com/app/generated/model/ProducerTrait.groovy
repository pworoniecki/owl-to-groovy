package com.app.generated.model

import com.app.generated.exception.*

trait ProducerTrait implements ThingTrait {
  
  private static Set<ProducerTrait> allInstances = [].toSet()
  
  private String producerName
  
  static Producer createInstance() {
    def classInstance = new Producer()
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(ProducerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Thing.addInstance(classInstance)
  }
  
  static boolean removeInstance(ProducerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Thing.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(ProducerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<ProducerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
  String getProducerName() {
    return this.producerName
  }
  
  void setProducerName(String producerName) {
    if (this.producerName == producerName) { return }
    this.producerName = producerName
  }
  
}
