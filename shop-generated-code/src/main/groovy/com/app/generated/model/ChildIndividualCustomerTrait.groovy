package com.app.generated.model

import com.app.generated.exception.*
import com.app.generated.validator.DisjointWith

@DisjointWith([AdultIndividualCustomerTrait, IndividualCustomerTrait])
trait ChildIndividualCustomerTrait implements CustomerTrait, IndividualCustomerTrait {
  
  private static Set<ChildIndividualCustomerTrait> allInstances = [].toSet()
  
  static ChildIndividualCustomer createInstance() {
    def classInstance = new ChildIndividualCustomer()
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
    return classInstance
  }
  
  static void addInstance(ChildIndividualCustomerTrait classInstance) {
    if(allInstances.contains(classInstance)) {
      return
    }
    allInstances.add(classInstance)
    Customer.addInstance(classInstance)
  }
  
  static boolean removeInstance(ChildIndividualCustomerTrait classInstance) {
    if(!allInstances.contains(classInstance)) {
      return false
    }
    allInstances.remove(classInstance)
    Customer.removeInstance(classInstance)
    return true
  }
  
  static boolean containsInstance(ChildIndividualCustomerTrait classInstance) {
    allInstances.contains(classInstance)
  }
  
  static Set<ChildIndividualCustomerTrait> getInstances() {
    return new HashSet<>(allInstances)
  }
  
}
